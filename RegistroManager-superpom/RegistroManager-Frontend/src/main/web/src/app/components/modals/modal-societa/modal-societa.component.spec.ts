import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSocietaComponent } from './modal-societa.component';

describe('ModalSocietaComponent', () => {
  let component: ModalSocietaComponent;
  let fixture: ComponentFixture<ModalSocietaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSocietaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSocietaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
