import { Component, OnInit, Inject } from '@angular/core';
import { UserWeb } from 'src/app/interfaces/user-web';
import { RegistroEntity } from 'src/app/interfaces/registro-entity';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { HttpServiceService } from 'src/app/services/http.service';
import { Router } from '@angular/router';
import { Global } from 'src/app/global';

@Component({
  selector: 'app-app-start',
  templateUrl: './app-start.component.html',
  styleUrls: ['./app-start.component.css']
})
export class AppStartComponent implements OnInit {

  user: UserWeb = null;
  registroEntity: RegistroEntity;

  respUser: boolean;
  indexArrayRole: number = 0;

  arrayRole: string[] = [];

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private http: HttpServiceService,
    private route: Router,
    private global: Global) { }

  ngOnInit() {
    this.http.doGetUser().subscribe((resp) => {
      this.registroEntity = resp;
      this.user = resp.body;

      if (this.user.ruoli !== null) {
        for (let i = 0; i < this.user.ruoli.length; i++) {
          if (this.user.ruoli[i] === this.global.RUOLO_GESTIONE ||
            this.user.ruoli[i] === this.global.RUOLO_CONFIG ||
            this.user.ruoli[i] === this.global.RUOLO_SUPERVISORE ||
            this.user.ruoli[i] === this.global.RUOLO_GESTIONE_WAS ||
            this.user.ruoli[i] === this.global.RUOLO_CONFIG_WAS ||
            this.user.ruoli[i] === this.global.RUOLO_SUPERVISORE_WAS) {

            this.arrayRole[this.indexArrayRole] = this.user.ruoli[i];
            this.indexArrayRole++;

          }
        }
      }

      if (this.registroEntity.status == 'OK' && this.user !== null) {
        this.respUser = true;
        this.storage.set('USER', this.user);

        console.log('RegistroEntity: ' + this.registroEntity.status);
        console.log('----------------------------------------');
        console.log('Utente connesso in sessione');
        console.log('USERNAME: ' + this.user.username);
        console.log('GRUPPO AUTENTICATO: ' + this.arrayRole);
        console.log('SESSION ID: ' + this.user.sessionID);
        console.log('COOKIE: ' + this.user.cookie);

        // in locale
        if (this.user.cookie != null) {
          this.storage.set('X-XSRF-TOKEN', this.user.cookie.split('=')[1]);
          console.log('X-XSRF-TOKEN : ' + this.user.cookie.split('=')[1]);
        }

        // sul WAS
        // if (this.user.cookie != null) {
        //   this.storage.set('X-XSRF-TOKEN', this.user.cookie.split('JSESSIONID=')[1]);
        //   console.log('X-XSRF-TOKEN : ' + this.user.cookie.split('JSESSIONID=')[1]);
        // }

        console.log('----------------------------------------');
      }
      else {
        this.respUser = false;

        console.log('RegistroEntity: ' + this.registroEntity.status);
        console.log('----------------------------------------');
        console.log('Utente che ha tentato l\'accesso in sessione');
        console.log('IV-USER: ' + this.user.username);
        console.log('IV-GROUPS: ' + this.user.ruolo);
        console.log('----------------------------------------');

        this.route.navigate(['/error', this.registroEntity.messageDescription]);
      }
    });
  }

}