import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Societa } from 'src/app/interfaces/societa';
import { RestSocietaService } from 'src/app/services/rest-societa.service';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { ModalSocietaComponent } from '../../modals/modal-societa/modal-societa.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserWeb } from 'src/app/interfaces/user-web';
import { Global } from 'src/app/global';
import { Router } from '@angular/router';

@Component({
  selector: 'app-societa',
  templateUrl: './societa.component.html',
  styleUrls: ['./societa.component.css']
})
export class SocietaComponent implements OnInit {

  societaInserita: string;
  listaSocieta: Societa[];
  listaPersone: SchedaPersona[];
  messaggioErrore: string;
  sceltaAmbito: string;
  pathExportExcel: string;

  user: UserWeb;

  respMessaggioSocietaVuota: boolean;
  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;
  respAggiungiSocieta: boolean;
  respRimuoviSocieta: boolean;

  displayedColumns1: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  dataSource1: MatTableDataSource<SchedaPersona> = null;
  @ViewChild(MatSort) sort: MatSort;

  // tslint:disable-next-line: max-line-length
  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restSocietaService: RestSocietaService,
    private restPersonaService: RestPersonaService,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private route: Router,
    private global: Global) {

    this.sceltaAmbito = this.storage.get('ambito');
    this.societaInserita = '';
    this.listaSocieta = [];
    this.listaPersone = [];
    this.messaggioErrore = 'Non sono presenti persone nel Database per questa società';

    this.respMessaggioSocietaVuota = false;
    this.respGif = false;
    this.respTabella = false;
    this.respMessaggioErrore = false;

    this.pathExportExcel = '';

    this.user = this.storage.get('USER');

    if (this.user.ruolo !== null || this.user.ruolo !== '' || this.user.ruolo !== undefined) {
      if (this.user.ruolo === this.global.RUOLO_SUPERVISORE || this.user.ruolo === this.global.RUOLO_CONFIG) {
        this.respAggiungiSocieta = false;
        this.respRimuoviSocieta = false;
      } else if (this.user.ruolo === this.global.RUOLO_GESTIONE) {
        this.respAggiungiSocieta = true;
        this.respRimuoviSocieta = true;
      }

    } else {
      this.route.navigate(['/error', 'UTENTE NON AUTORIZZATO']);
    }
  }

  ngOnInit() {
    this.loadTableSocieta();
  }

  loadTableSocieta() {
    this.restSocietaService.getListaSocieta().subscribe((resp) => {
      this.listaSocieta = resp;
    });
  }

  selectSocieta() {
    this.respMessaggioSocietaVuota = false;
    this.respMessaggioErrore = false;
    this.respTabella = false;
    this.respGif = true;
    if (!this.societaInserita || this.societaInserita === '') {
      this.respGif = false;
      window.alert('Inserire una società!');
    } else {
      this.restPersonaService.findBySocieta(this.societaInserita, this.sceltaAmbito).subscribe((resp) => {
        this.listaPersone = resp;
        if (this.listaPersone.length === 0) {
          this.respGif = false;
          this.respMessaggioErrore = true;
        } else {
          this.pathExportExcel = `${this.restPersonaService.contextPath}rest/getElencoPersoneSocieta/${this.societaInserita}/${this.sceltaAmbito}`;
          this.respGif = false;
          this.respTabella = true;
          this.dataSource1 = new MatTableDataSource<SchedaPersona>(this.listaPersone);
          this.dataSource1.sort = this.sort;
        }
      });
    }
  }

  annullaSocietaInserita() {
    this.respTabella = false;
    this.societaInserita = '';
    this.respMessaggioSocietaVuota = false;
    this.respMessaggioErrore = false;
  }

  open() {
    const ngbModalRef = this.modalService.open(ModalSocietaComponent, { size: 'lg' });
    ngbModalRef.componentInstance.respAddSocieta = true;
    ngbModalRef.componentInstance.respDeleteSocieta = false;
    ngbModalRef.result.then(
      result => {
        if (result !== 'Modal aggiungi chiusa su Annulla') {
          console.log(result);
          window.alert('La società è stata inserita correttamente!');
          this.loadTableSocieta();
        }
      }, //in close()
      reject => console.log(reject) //in dismiss()
    );
  }

  openModalDelete() {
    const ngbModalRef = this.modalService.open(ModalSocietaComponent, { size: 'lg' });
    ngbModalRef.componentInstance.respAddSocieta = false;
    ngbModalRef.componentInstance.respDeleteSocieta = true;
    ngbModalRef.result.then(
      result => {
        if (result !== 'Modal rimuovi chiusa su annulla') {
          console.log(result);
          window.alert('La società è stata rimossa correttamente!');
          this.loadTableSocieta();
        }
      }, //in close()
      reject => console.log(reject) //in dismiss()
    );
  }

}
