import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMotivoVariazioneComponent } from './modal-motivo-variazione.component';

describe('ModalMotivoVariazioneComponent', () => {
  let component: ModalMotivoVariazioneComponent;
  let fixture: ComponentFixture<ModalMotivoVariazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMotivoVariazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMotivoVariazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
