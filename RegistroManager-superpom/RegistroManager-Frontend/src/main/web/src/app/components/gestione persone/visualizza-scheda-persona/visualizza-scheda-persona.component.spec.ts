import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaSchedaPersonaComponent } from './visualizza-scheda-persona.component';

describe('VisualizzaSchedaPersonaComponent', () => {
  let component: VisualizzaSchedaPersonaComponent;
  let fixture: ComponentFixture<VisualizzaSchedaPersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaSchedaPersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaSchedaPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
