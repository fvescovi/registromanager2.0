import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDataCancellazioneComponent } from './modal-data-cancellazione.component';

describe('ModalDataCancellazioneComponent', () => {
  let component: ModalDataCancellazioneComponent;
  let fixture: ComponentFixture<ModalDataCancellazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDataCancellazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDataCancellazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
