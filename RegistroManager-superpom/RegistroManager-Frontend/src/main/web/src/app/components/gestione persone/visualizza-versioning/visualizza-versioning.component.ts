import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { Versioning } from 'src/app/interfaces/versioning';
import { MatTableDataSource } from '@angular/material';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-versioning',
  templateUrl: './visualizza-versioning.component.html',
  styleUrls: ['./visualizza-versioning.component.css']
})
export class VisualizzaVersioningComponent implements OnInit {

  schedaPersona: SchedaPersona;
  idSchedaPersona: string;
  sceltaAmbito: string;
  listaVersioning: Versioning[];

  respGif: boolean;
  respTabella: boolean;

  displayedColumns1: string[] = ['1', '2', '3'];
  dataSource1: MatTableDataSource<Versioning> = null;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private activateRoute: ActivatedRoute,
    private restPersonaService: RestPersonaService) {

    this.sceltaAmbito = this.storage.get('ambito');

    this.idSchedaPersona = '';
    this.respGif = true;
    this.idSchedaPersona = this.activateRoute.snapshot.paramMap.get('idSchedaPersona');

    this.restPersonaService.findById(this.idSchedaPersona).subscribe((resp1) => {
      this.schedaPersona = resp1;
      this.restPersonaService.findVersioningByPersona(this.schedaPersona.idSchedaPersona).subscribe((resp2) => {
        this.listaVersioning = resp2;
        this.respGif = false;
        this.respTabella = true;
        this.dataSource1 = new MatTableDataSource<Versioning>(this.listaVersioning);
      });
    });

  }

  ngOnInit() {

  }

}
