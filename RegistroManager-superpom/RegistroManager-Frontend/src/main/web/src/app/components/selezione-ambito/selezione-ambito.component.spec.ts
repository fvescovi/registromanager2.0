import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelezioneAmbitoComponent } from './selezione-ambito.component';

describe('SelezioneAmbitoComponent', () => {
  let component: SelezioneAmbitoComponent;
  let fixture: ComponentFixture<SelezioneAmbitoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelezioneAmbitoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelezioneAmbitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
