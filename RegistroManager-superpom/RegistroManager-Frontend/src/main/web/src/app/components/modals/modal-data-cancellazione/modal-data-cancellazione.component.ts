import { Component, OnInit, Input, Inject } from '@angular/core';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Global } from 'src/app/global';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-data-cancellazione',
  templateUrl: './modal-data-cancellazione.component.html',
  styleUrls: ['./modal-data-cancellazione.component.css']
})
export class ModalDataCancellazioneComponent implements OnInit {

  @Input() schedaPersona: SchedaPersona;

  user: UserWeb
  sceltaAmbito: string;
  motivoUltimaVariazione: string;
  dataCancellazione: moment.Moment;
  oraCancellazione: string;
  confirm: boolean;
  maxDate: Date = new Date();

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    public activeModal: NgbActiveModal,
    private restPersonaService: RestPersonaService,
    public global: Global,
    private router: Router) {

    this.motivoUltimaVariazione = null;
    this.dataCancellazione = moment();
    this.oraCancellazione = '';

    this.confirm = true;

    this.sceltaAmbito = this.storage.get('ambito');
    this.user = this.storage.get('USER');
  }

  ngOnInit() { }

  deleteSchedaPersona() {
    if (window.confirm('Desideri cancellare la scheda?')) {
      this.dataCancellazione.hours(Number(this.oraCancellazione.split(':')[0]));
      this.dataCancellazione.minutes(Number(this.oraCancellazione.split(':')[1]));
      this.schedaPersona.stato = this.global.STATO_CANCELLATO;
      this.schedaPersona.tipoAmbito = this.sceltaAmbito;
      this.schedaPersona.motivoUltimaVariazione = this.motivoUltimaVariazione;
      this.schedaPersona.dataCancellazione = this.dataCancellazione.toDate();
      this.restPersonaService.insertPersona(this.user.username, this.schedaPersona).subscribe(
        (resp) => {
          this.activeModal.close(resp);
          this.router.navigate(['/find-all']);
        });
    } else {
      this.confirm = true;
    }
  }

  annullaScelta() {
    this.activeModal.close('Modal chiusa su Annulla');
  }

}
