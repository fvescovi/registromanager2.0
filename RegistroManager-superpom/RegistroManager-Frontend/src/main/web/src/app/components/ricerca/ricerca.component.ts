import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { RicercaPersona } from 'src/app/classes/ricerca-persona';
import { HttpServiceService } from 'src/app/services/http.service';
import { RestSocietaService } from 'src/app/services/rest-societa.service';
import { Societa } from 'src/app/interfaces/societa';
import { Global } from 'src/app/global';

@Component({
  selector: 'app-ricerca',
  templateUrl: './ricerca.component.html',
  styleUrls: ['./ricerca.component.css']
})
export class RicercaComponent implements OnInit {

  societa: string;
  nome: string;
  cognome: string;
  motivoIscrizione: string;
  ricercaPersona: RicercaPersona;
  listaPersone: SchedaPersona[];
  listaSocieta: Societa[];
  sceltaAmbito: string;
  pathExportExcel: string;

  // resp visualizzazione html
  respModulo1: boolean;
  respGif: boolean;
  respMessaggioPersoneAssenti: boolean;
  respTabella: boolean;

  // variabili mat table material
  displayedColumns1: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  dataSource1: MatTableDataSource<SchedaPersona> = null;
  @ViewChild(MatSort) sort: MatSort;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restPersonaService: RestPersonaService,
    private http: HttpServiceService,
    private restSocietaService: RestSocietaService,
    public global: Global) {

    this.sceltaAmbito = this.storage.get('ambito');
    this.ricercaPersona = new RicercaPersona();
    this.societa = '';
    this.nome = '';
    this.cognome = '';
    this.motivoIscrizione = '';
    this.listaSocieta = [];
    this.listaPersone = [];
    this.respModulo1 = true;
    this.respGif = false;
    this.respMessaggioPersoneAssenti = false;
    this.respTabella = false;

    this.pathExportExcel = '';
  }

  ngOnInit() {
    this.restSocietaService.getListaSocieta().subscribe((resp) => {
      this.listaSocieta = resp;
    });
  }

  searchPersone() {
    if (this.sceltaAmbito === this.global.AMBITO_INSIDER_LIST) {
      if ((!this.societa || this.societa.trim() === '') && this.nome.trim() === '' && this.cognome.trim() === '' && this.motivoIscrizione.trim() === '') {
        window.alert('Inserire almeno un campo!');
        this.respMessaggioPersoneAssenti = false;
        this.respGif = false;
      } else {
        this.respGif = true;
        this.ricercaPersona.societa = this.societa;
        this.ricercaPersona.nome = this.nome;
        this.ricercaPersona.cognome = this.cognome;
        this.ricercaPersona.motivoIscrizione = this.motivoIscrizione;
        this.restPersonaService.findPersonaByCampi(this.sceltaAmbito, this.ricercaPersona).subscribe((resp) => {
          this.listaPersone = resp;
          if (this.listaPersone.length === 0) {
            this.respGif = false;
            this.respMessaggioPersoneAssenti = true;
            this.respTabella = false;
          } else {
            this.pathExportExcel = `${this.restPersonaService.contextPath}rest/getElencoPersoneRicerca/${this.sceltaAmbito}/${JSON.stringify(this.ricercaPersona)}`;
            this.respMessaggioPersoneAssenti = false;
            this.respGif = false;
            this.dataSource1 = new MatTableDataSource<SchedaPersona>(this.listaPersone);
            this.respTabella = true;
            this.dataSource1.sort = this.sort;
          }
        });
      }
    }

    else {
      
      if ((this.societa.trim() === '' || this.societa === null) && this.nome.trim() === '' && this.cognome.trim() === '') {
        window.alert('Inserire almeno un campo!');
        this.respMessaggioPersoneAssenti = false;
        this.respGif = false;
      } else {
        this.respGif = true;
        this.ricercaPersona.societa = this.societa;
        this.ricercaPersona.nome = this.nome;
        this.ricercaPersona.cognome = this.cognome;
        this.restPersonaService.findPersonaByCampi(this.sceltaAmbito, this.ricercaPersona).subscribe((resp) => {
          this.listaPersone = resp;
          if (this.listaPersone.length === 0) {
            this.respGif = false;
            this.respMessaggioPersoneAssenti = true;
            this.respTabella = false;
          } else {
            this.pathExportExcel = `${this.restPersonaService.contextPath}rest/getElencoPersoneRicerca/${this.sceltaAmbito}/${JSON.stringify(this.ricercaPersona)}`;
            this.respMessaggioPersoneAssenti = false;
            this.respGif = false;
            this.dataSource1 = new MatTableDataSource<SchedaPersona>(this.listaPersone);
            this.respTabella = true;
            this.dataSource1.sort = this.sort;
          }
        });
      }
    }
  }

  resetModulo() {
    this.societa = '';
    this.nome = '';
    this.cognome = '';
    this.motivoIscrizione = '';
    this.respTabella = false;
    this.respMessaggioPersoneAssenti = false;
  }

}
