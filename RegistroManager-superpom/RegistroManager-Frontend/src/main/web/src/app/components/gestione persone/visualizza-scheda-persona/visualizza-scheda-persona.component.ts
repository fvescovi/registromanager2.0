import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { Societa } from 'src/app/interfaces/societa';
import { Provincia } from 'src/app/classes/provincia';
import { ControlloCampi } from 'src/app/classes/utility/controllo-campi';
import { RestSocietaService } from 'src/app/services/rest-societa.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalMotivoVariazioneComponent } from '../../modals/modal-motivo-variazione/modal-motivo-variazione.component';
import { ModalMailComponent } from '../../modals/modal-mail/modal-mail.component';
import { UserWeb } from 'src/app/interfaces/user-web';
import { Global } from 'src/app/global';
import { ModalDataCancellazioneComponent } from '../../modals/modal-data-cancellazione/modal-data-cancellazione.component';


@Component({
  selector: 'app-visualizza-scheda-persona',
  templateUrl: './visualizza-scheda-persona.component.html',
  styleUrls: ['./visualizza-scheda-persona.component.css']
})
export class VisualizzaSchedaPersonaComponent implements OnInit {

  listaProvince: Provincia;
  schedaPersona: SchedaPersona;
  schedaPersona2: SchedaPersona;
  controlloCampi: ControlloCampi;

  idSchedaPersona: string;
  listaValori: any;
  listaSocieta: Societa[];
  sceltaAmbito: string;
  maxDate: Date = new Date();

  step: number[];
  currentStep: number;

  user: UserWeb;

  // Response per visualizzazione html
  respModulo1: boolean;
  respModulo2: boolean;
  respModulo3: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;
  respBottoneConferma: boolean;
  respGif: boolean;
  respScheda: boolean;
  respBottoneElimina: boolean;
  respBottoneModifica: boolean;
  respInformazioni: boolean;
  respBottoneEmail: boolean;
  respBottoneLettera: boolean;
  respBottoneVersioni: boolean;
  respBottoneStampaPersona: boolean;
  respBottoneBozza: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private activateRoute: ActivatedRoute,
    private restPersonaService: RestPersonaService,
    private restSocietaService: RestSocietaService,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private global: Global,
    private router: Router) {

    this.sceltaAmbito = this.storage.get('ambito');

    this.user = this.storage.get('USER');

    this.idSchedaPersona = '';

    this.listaProvince = new Provincia();

    this.idSchedaPersona = this.activateRoute.snapshot.paramMap.get('idSchedaPersona');

    this.controlloCampi = new ControlloCampi();
    this.listaValori = [];
    this.listaSocieta = [];

    this.step = [];
    this.currentStep = 1;

    this.respGif = true;
    this.respBottoneEmail = true;
    this.respBottoneStampaPersona = true;

    this.respMessaggioErrore = false;

    this.restPersonaService.findById(this.idSchedaPersona).subscribe((resp) => {
      this.schedaPersona = resp;
      this.schedaPersona2 = JSON.parse(JSON.stringify(this.schedaPersona));

      if (this.schedaPersona.stato === this.global.STATO_BOZZA) {
        this.respBottoneBozza = true;
        this.respBottoneVersioni = false;
        this.respBottoneEmail = false;
        this.respBottoneLettera = false;
      } else if (this.schedaPersona.email === null) {
        this.respBottoneVersioni = true;
        this.respBottoneLettera = true;
        this.respBottoneEmail = false;
      } else {
        this.respBottoneVersioni = true;
        this.respBottoneEmail = true;
        this.respBottoneLettera = true;
      }

      this.respScheda = true;
      this.respGif = false;
      this.respTabella = true;
      if (this.sceltaAmbito === this.global.AMBITO_REGISTRO_MANAGER || this.schedaPersona.stato === this.global.STATO_BOZZA) {
        // tslint:disable-next-line:no-unused-expression
        this.respInformazioni = false;
      } else {
        // tslint:disable-next-line:no-unused-expression
        this.respInformazioni = true;
      }
      if (this.schedaPersona.stato === this.global.STATO_DEFINITIVO) {
        this.respModulo1 = false;
        this.respModulo2 = false;
        this.respModulo3 = false;
        this.respBottoneConferma = false;
        this.respBottoneElimina = true;
        this.respBottoneModifica = true;
        this.respBottoneBozza = false;
      } else if (this.schedaPersona.stato === this.global.STATO_BOZZA) {
        this.respModulo1 = false;
        this.respModulo2 = false;
        this.respModulo3 = false;
        this.respBottoneConferma = true;
        this.respBottoneModifica = true;
        this.respBottoneBozza = false;
      } else {
        this.respModulo1 = false;
        this.respModulo2 = false;
        this.respModulo3 = false;
        this.respBottoneConferma = false;
        this.respBottoneElimina = false;
        this.respBottoneModifica = false;
      }
    });

  }

  ngOnInit() {
    this.loadStep(this.global.STEP_PG);
  }

  loadStep(steps: number) {
    this.step = [];
    for (let i = 1; i <= steps; i++) {
      this.step.push(i);
    }
  }

  onChange(tipologiaSoggetto: string) {
    if (tipologiaSoggetto === 'Persona Giuridica') {
      this.loadStep(this.global.STEP_PG);
    } else {
      this.loadStep(this.global.STEP_PF);
    }
  }

  setModulo1() {
    this.listaValori = [];
    this.listaValori.push(this.schedaPersona.tipologiaSoggetto);
    this.listaValori.push(this.schedaPersona.nome);
    this.listaValori.push(this.schedaPersona.cognome);
    this.listaValori.push(this.schedaPersona.dataNascita);

    if (this.sceltaAmbito === this.global.AMBITO_INSIDER_LIST) {
      this.listaValori.push(this.schedaPersona.nomeIndirizzoImpresa);
      this.listaValori.push(this.schedaPersona.dataIscrizione);
    }

    if(this.sceltaAmbito === this.global.AMBITO_REGISTRO_MANAGER) {
      this.listaValori.push(this.schedaPersona.dataInserimento);
    }

    this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);

    if (this.respMessaggioErrore === false) {
      if (this.schedaPersona.tipologiaSoggetto === 'Persona Giuridica') {
        this.respMessaggioErrore = false;
        this.respModulo1 = false;
        this.respModulo2 = true;
      } else if (this.schedaPersona.tipologiaSoggetto === 'Persona Fisica') {
        this.respMessaggioErrore = false;
        this.respModulo1 = false;
        this.respModulo3 = true;
      }
    } else {
      window.alert('Inserire tutti i campi obbligatori!');
      this.respModulo1 = true;
    }

    this.restSocietaService.getListaSocieta().subscribe((resp) => {
      this.listaSocieta = resp;
    });

    if (this.respMessaggioErrore === false)
      this.currentStep++;

  }

  setModulo2() {
    this.listaValori = [];
    this.listaValori.push(this.schedaPersona.partitaIva);
    this.listaValori.push(this.schedaPersona.ragioneSociale);
    this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);
    if (this.respMessaggioErrore === false) {
      this.respMessaggioErrore = false;
      this.respModulo2 = false;
      this.respModulo3 = true;
    }

    if (this.respMessaggioErrore === false)
      this.currentStep++;
  }

  setModulo3() {
    this.listaValori = [];

    if (this.sceltaAmbito === this.global.AMBITO_INSIDER_LIST) {
      this.listaValori.push(this.schedaPersona.societa);
      this.listaValori.push(this.schedaPersona.codiceFiscale);
      this.listaValori.push(this.schedaPersona.ruolo);
      this.listaValori.push(this.schedaPersona.motivoIscrizione);
      this.listaValori.push(this.schedaPersona.descrizioneInformazione);

      this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);

    } else {
      this.listaValori.push(this.schedaPersona.societa);
      this.listaValori.push(this.schedaPersona.codiceFiscale);
      this.listaValori.push(this.schedaPersona.ruolo);

      this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);
    }

    if (this.respMessaggioErrore === false) {
      if (this.schedaPersona.stato === this.global.STATO_DEFINITIVO) {
        this.respBottoneConferma = this.controlloCampi.equal(this.schedaPersona, this.schedaPersona2);
        this.respBottoneBozza = false;
        this.respMessaggioErrore = false;
        this.respModulo3 = false;
        this.respTabella = true;
      } else {
        this.respMessaggioErrore = false;
        this.respModulo3 = false;
        this.respTabella = true;
      }

      if (this.schedaPersona.stato === this.global.STATO_BOZZA) {
        this.respBottoneBozza = this.controlloCampi.equal(this.schedaPersona, this.schedaPersona2);
        this.respMessaggioErrore = false;
        this.respModulo3 = false;
        this.respTabella = true;
      }

      this.respBottoneStampaPersona = true;
    }

    if (this.respMessaggioErrore === false)
      this.currentStep++;
  }

  resetModulo1() {
    this.schedaPersona = new SchedaPersona();
    this.respMessaggioErrore = false;
    this.currentStep = 1;
  }

  resetModulo2() {
    this.respModulo2 = false;
    this.respModulo1 = true;
    this.respMessaggioErrore = false;
    this.currentStep--;
  }

  resetModulo3() {
    this.respMessaggioErrore = false;
    if (this.schedaPersona.tipologiaSoggetto === 'Persona Giuridica') {
      this.respModulo3 = false;
      this.respModulo2 = true;
    } else {
      this.respModulo3 = false;
      this.respModulo2 = false;
      this.respModulo1 = true;
    }
    this.currentStep--;
  }

  editSchedaPersona() {
    this.respTabella = false;
    this.respModulo1 = true;
    this.respBottoneStampaPersona = false;
    this.currentStep = 1;

    if (this.schedaPersona.tipologiaSoggetto === 'Persona Giuridica')
      this.loadStep(this.global.STEP_PG);

    else
      this.loadStep(this.global.STEP_PF);
  }

  saveSchedaPersona() {

    if (this.schedaPersona.stato === this.global.STATO_DEFINITIVO) {
      this.respTabella = true;
      if (this.idSchedaPersona !== null) {
        const ngbModalRef = this.modalService.open(ModalMotivoVariazioneComponent, { windowClass: 'mdClass' });
        ngbModalRef.componentInstance.schedaPersona = this.schedaPersona;
        ngbModalRef.result.then(
          result => {
            if (result === 'Persona modificata') {
              console.log(result);
              window.alert('La scheda persona è stata modificata con successo!');
              this.respTabella = true;
              this.respModulo1 = false;
              this.respBottoneConferma = false;
            }
          }, //in close()
          reject => console.log(reject) //in dismiss()
        );
      }

    }

    if (this.schedaPersona.stato === this.global.STATO_BOZZA) {
      if (window.confirm('Desideri confermare la scheda?')) {
        window.alert('La scheda è stata confermata correttamente!');
        this.schedaPersona.tipoAmbito = this.sceltaAmbito;
        this.schedaPersona.stato = this.global.STATO_DEFINITIVO;
        this.respTabella = false;
        this.respModulo1 = true;
        this.restPersonaService.insertPersona(this.user.username, this.schedaPersona).subscribe(
          (resp) => this.router.navigate(['/find-all'])
        );
      }
    }

  }

  deleteSchedaPersona() {
    if (this.schedaPersona.stato === this.global.STATO_DEFINITIVO) {
      this.respTabella = true;
      const ngbModalRef = this.modalService.open(ModalDataCancellazioneComponent, { windowClass: 'mdClass' });
      ngbModalRef.componentInstance.schedaPersona = this.schedaPersona;
      ngbModalRef.result.then(
        result => {
          console.log(result);
          if (result !== 'Modal chiusa su Annulla') {
            window.alert('La scheda è stata cancellata correttamente!');
            this.respTabella = false;
          }
        }, // in close()
        reject => console.log(reject) //in dismiss()
      );
    } else if (window.confirm('Desideri rimuovere la scheda persona?')) {
      window.alert('La scheda è stata rimossa definitivamente!');
      this.restPersonaService.deleteSchedaPersona(this.schedaPersona.idSchedaPersona.toString(), this.user.username).subscribe(
        (resp) => this.router.navigate(['/find-all'])
      );
    } else {
      this.respTabella = true;
    }
  }

  openModal(action: string) {
    const ngbModalRef = this.modalService.open(ModalMailComponent, { size: 'lg' });
    ngbModalRef.componentInstance.schedaPersona = this.schedaPersona;
    ngbModalRef.componentInstance.action = action;
    ngbModalRef.result.then(
      result => {
        if (result === 'Mail inviata correttamente')
          window.alert('La mail è stata inviata correttamente!');

        else if (result === 'Errore mail')
          window.alert('Errore in fase di invio della mail');

        else if (result === 'Lettera stampata correttamente')
          window.alert('La lettera è pronta per il download!');

        this.respTabella = true;

      }, // in close()
      reject => console.log(reject) //in dismiss()
    );
  }

  resetSchedaPersona() {
    this.respTabella = false;
    this.respModulo1 = true;
    this.schedaPersona = new SchedaPersona();
  }

  saveBozzaSchedaPersona() {
    this.respTabella = false;
    if (window.confirm('Desideri salvare le modifiche effettuate su questa bozza?')) {
      window.alert('La scheda persona è stata modificata correttamente!');
      this.schedaPersona.tipoAmbito = this.sceltaAmbito;
      this.schedaPersona.stato = this.global.STATO_BOZZA;
      this.restPersonaService.insertPersona(this.user.username, this.schedaPersona).subscribe((resp) => {
        this.router.navigate(['/find-all'])
      });
      this.resetSchedaPersona();
    } else {
      this.respTabella = true;
    }
  }

  stampaSchedaPersona() {
    window.alert('La scheda persona è pronta per il download!');
    if (this.schedaPersona.motivoUltimaVariazione === null) {
      this.schedaPersona.motivoUltimaVariazione = '';
    }
    this.restPersonaService.stampaPersona(this.schedaPersona).subscribe((resultFile: Blob) => {
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(resultFile);
      }
      else {
        var downloadURL = URL.createObjectURL(resultFile);
        window.open(downloadURL);
      }
    });
  }

  validateCodiceFiscale(codiceFiscale) {
    var re = /^(?:(?:[B-DF-HJ-NP-TV-Z]|[AEIOU])[AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i
    if (!re.test(String(codiceFiscale).toUpperCase())) {
      window.alert('Codice fiscale non valido!');
      this.schedaPersona.codiceFiscale = null;
      return null;
    }
  }

}
