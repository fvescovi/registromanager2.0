import { Component, OnInit, Input, Inject } from '@angular/core';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Global } from 'src/app/global';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-modifca-informazione',
  templateUrl: './modal-modifca-informazione.component.html',
  styleUrls: ['./modal-modifca-informazione.component.css']
})
export class ModalModifcaInformazioneComponent implements OnInit {

  @Input() schedaInformazione: SchedaInformazione;
  @Input() action: number;

  stato: string = '';
  user: UserWeb;

  maxDate: Date = new Date();

  informazionePrivilegiata: string;
  dataIdentificazione: Date;
  oraIdentificazione: string;
  dataCessazione: Date = null;
  oraCessazione: string = null;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private activeModal: NgbActiveModal,
    private restInformazioneService: RestInformazioneService,
    private global: Global,
    private router: Router) { }

  ngOnInit() {
    this.informazionePrivilegiata = this.schedaInformazione.informazionePrivilegiata;

    if (this.action === 0) {
      this.dataIdentificazione = this.schedaInformazione.dataIdentificazione;
      this.oraIdentificazione = this.schedaInformazione.oraIdentificazione;
    }

    this.user = this.storage.get('USER');
  }

  controlField() {

    // MODIFICA
    if (this.action === 0) {

      if ((this.schedaInformazione.informazionePrivilegiata !== this.informazionePrivilegiata ||
        this.schedaInformazione.dataIdentificazione !== this.dataIdentificazione ||
        this.schedaInformazione.oraIdentificazione !== this.oraIdentificazione) &&
        this.oraIdentificazione !== '' && this.informazionePrivilegiata !== '')
        return true;

      else
        return false;

      // CANCELLA
    } else if (this.action === 1) {

      if (this.dataCessazione !== null &&
        this.oraCessazione !== null &&
        this.oraCessazione !== '')
        return true;

      else
        return false;
    }
  }

  // action = 1
  deleteInformazione() {
    if (window.confirm('Desideri eliminare questa informazione privilegiata?')) {
      this.schedaInformazione.stato = this.global.STATO_INFO_CESSATA;
      this.schedaInformazione.dataCessazioneInformazione = this.dataCessazione;
      this.schedaInformazione.oraCessazioneInformazione = this.oraCessazione;
      this.schedaInformazione.autoreInformazione = this.user.username;
      this.restInformazioneService.insertInformazione(this.schedaInformazione).subscribe(
        () => {
          window.alert('Informazione privilegiata cessata con successo!');
          this.router.navigate(['/find-all-informazioni']);
          this.activeModal.close();
        }
      );
    }
  }

  // action = 0
  modificaInformazione() {
    if (window.confirm('Desideri modificare questa informazione privilegiata?')) {
      this.schedaInformazione.informazionePrivilegiata = this.informazionePrivilegiata;
      this.schedaInformazione.dataIdentificazione = this.dataIdentificazione;
      this.schedaInformazione.oraIdentificazione = this.oraIdentificazione;
      this.schedaInformazione.autoreInformazione = this.user.username;
      this.restInformazioneService.insertInformazione(this.schedaInformazione).subscribe(
        () => {
          window.alert('Informazione privilegiata modificata con successo!');
          this.router.navigate(['/find-all-informazioni']);
          this.activeModal.close();
        }
      );
    }
  }

  annullaScelta() {
    this.activeModal.close('Modal chiusa su Annulla');
  }

}
