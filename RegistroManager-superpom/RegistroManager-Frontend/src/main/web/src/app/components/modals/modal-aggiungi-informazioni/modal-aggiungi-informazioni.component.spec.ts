import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAggiungiInformazioniComponent } from './modal-aggiungi-informazioni.component';

describe('ModalAggiungiInformazioniComponent', () => {
  let component: ModalAggiungiInformazioniComponent;
  let fixture: ComponentFixture<ModalAggiungiInformazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAggiungiInformazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAggiungiInformazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
