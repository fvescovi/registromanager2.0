import { Component, OnInit, ViewChild, Inject, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';
import { ControlloCampi } from 'src/app/classes/utility/controllo-campi';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalModifcaInformazioneComponent } from '../../modals/modal-modifca-informazione/modal-modifca-informazione.component';
import { UserWeb } from 'src/app/interfaces/user-web';
import { Global } from 'src/app/global';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { PaginaSchedaPersona } from 'src/app/interfaces/paginaSchedaPersona';
import { AccessoInformazione } from 'src/app/classes/accesso-informazione';

@Component({
  selector: 'app-visualizza-scheda-informazioni',
  templateUrl: './visualizza-scheda-informazioni.component.html',
  styleUrls: ['./visualizza-scheda-informazioni.component.css']
})
export class VisualizzaSchedaInformazioniComponent implements OnInit {

  @Output() page: EventEmitter<PageEvent>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  listaValori: any = [];
  idSchedaInformazione: string = '';

  schedaInformazione: SchedaInformazione = new SchedaInformazione();
  controlloCampi: ControlloCampi = new ControlloCampi();

  user: UserWeb;

  paginaSchedaPersona: PaginaSchedaPersona;

  // variabili per paginator 
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;

  // Response per visualizzazione html
  respTabella: boolean;
  respBarra: boolean;
  respGif: boolean;
  respTabellaInformazioni: boolean;
  respBottoneModifica: boolean;
  respBottoneElimina: boolean;

  displayedColumns: string[] = ['Nome', 'Cognome', 'Società', 'Tipo di accesso'];

  accessList: AccessoInformazione[] = [];

  dataSource: MatTableDataSource<SchedaPersona> = null;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private activateRoute: ActivatedRoute,
    private restInformazioneService: RestInformazioneService,
    private restPersonaService: RestPersonaService,
    public modalService: NgbModal,
    private global: Global) {

    this.idSchedaInformazione = this.activateRoute.snapshot.paramMap.get('idSchedaInformazione');
    this.respGif = true;
    
    this.user = this.storage.get('USER');

    if (this.user.ruolo === this.global.RUOLO_SUPERVISORE) {
      this.respBottoneModifica = false;
      this.respBottoneElimina = false;
    } else if (this.user.ruolo === this.global.RUOLO_CONFIG) {
      this.respBottoneModifica = true;
      this.respBottoneElimina = false;
    } else if (this.user.ruolo === this.global.RUOLO_GESTIONE) {
      this.respBottoneModifica = true;
      this.respBottoneElimina = true;
    }

  }

  ngOnInit() {
    this.restInformazioneService.findById(this.idSchedaInformazione).subscribe((resp) => {
      this.schedaInformazione = resp;

      if (this.schedaInformazione.listaAccessoInformazione === undefined ||
        this.schedaInformazione.listaAccessoInformazione === null ||
        this.schedaInformazione.listaAccessoInformazione.length === 0) {
        this.schedaInformazione.listaAccessoInformazione = this.getAccessList();
      }

      this.respGif = false;
      this.respTabella = true;
      this.respBarra = false;
      this.respTabellaInformazioni = false;

      this.getPaginaSchedaPersona(null);
      this.accessList = this.getAccessList();
    });
  }

  getAccessList(): AccessoInformazione[] {
    this.restInformazioneService.findAccessListByIdInfo(this.idSchedaInformazione).subscribe((data) => {
      this.accessList = data;
    });
    return this.accessList;
  }

  getPaginaSchedaPersona(event?: PageEvent) {

    if (event === null) {
      this.firstTimeGet();
    } else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.getList();
    }

  }

  firstTimeGet() {
    this.pageIndex = 0;
    this.pageSize = 5;
    this.getList();
  }

  getList() {
    this.restPersonaService.findPersonaByIdInformazione(this.idSchedaInformazione, this.pageIndex, this.pageSize).subscribe((resp) => {
      this.paginaSchedaPersona = resp;

      if (this.paginaSchedaPersona.listaSchedaPersona.length === 0 && this.schedaInformazione.stato !== this.global.STATO_INFO_CESSATA)
        window.alert('Attualmente nessuna persona accede a questa informazione privilegiata!');

      else if (this.schedaInformazione.stato !== this.global.STATO_INFO_CESSATA)
        this.respTabellaInformazioni = true;

      this.setDataSuorce(this.paginaSchedaPersona);
    });
  }

  setDataSuorce(paginaSchedaPersona: PaginaSchedaPersona) {

    if (paginaSchedaPersona !== null) {

      this.pageIndex = paginaSchedaPersona.paginaCorrente;
      this.length = paginaSchedaPersona.totaleRecord;

      if (paginaSchedaPersona.totaleRecord !== 0) {

        if (this.dataSource === null) {
          this.dataSource = new MatTableDataSource<SchedaPersona>(this.paginaSchedaPersona.listaSchedaPersona);
          this.dataSource.paginator = this.paginator;
        } else {
          this.dataSource.data = paginaSchedaPersona.listaSchedaPersona;
          this.updatePaginator(this.paginator.length, this.pageIndex);
        }
      }
    }
  }

  updatePaginator(filteredDataLength: number, pageIndex: number) {
    Promise.resolve().then(() => {
      this.paginator.pageIndex = pageIndex;

      if (!this.paginator) { return; }
      this.paginator.length = filteredDataLength;

      if (this.paginator.pageIndex > 0) {
        const lastPageIndex = Math.ceil(this.paginator.length / this.paginator.pageSize) - 1 || 0;
        this.paginator.pageIndex = Math.min(this.paginator.pageIndex, lastPageIndex);
      }
    });
  }

  editSchedaInformazione() {
    this.respTabella = true;
    this.respBarra = false;
    const ngbModalRef = this.modalService.open(ModalModifcaInformazioneComponent, { size: 'lg' });
    ngbModalRef.componentInstance.schedaInformazione = this.schedaInformazione;
    ngbModalRef.componentInstance.action = 0;
    ngbModalRef.result.then(
      result => console.log(result),
      reject => console.log(reject)
    );
  }

  deleteSchedaInformazione() {
    this.respTabella = true;
    this.respBarra = false;
    const ngbModalRef = this.modalService.open(ModalModifcaInformazioneComponent, { size: 'lg' });
    ngbModalRef.componentInstance.schedaInformazione = this.schedaInformazione;
    ngbModalRef.componentInstance.action = 1;
    ngbModalRef.result.then(
      result => console.log(result),
      reject => console.log(reject)
    );
  }

}
