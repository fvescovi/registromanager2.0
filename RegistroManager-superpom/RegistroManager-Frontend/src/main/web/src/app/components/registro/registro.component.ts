import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { Riga } from 'src/app/classes/riga';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  listaPersoneInfoSingole: Riga[] = [];
  listaPersonaInfoPermanenti: Riga[] = [];
  pathExportExcelPerm: string;
  pathExportExcelLim: string;
  sceltaAmbito: string;

  // resp per visualizzazione html
  respGif: boolean;
  respTabella: boolean;
  respMessaggioErroreInfoPerm: boolean;
  respMessaggioErroreInfoLim: boolean;

  check: boolean[] = new Array();
  selectedAll: boolean = false;

  // variabili mat table material
  displayedColumnsPerm: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  displayedColumnsLim: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  dataSourcePerm: MatTableDataSource<Riga> = null;
  dataSourceLim: MatTableDataSource<Riga> = null;
  messaggioErrorePerm: string;
  messaggioErroreLim: string;

  @ViewChild(MatSort) sort: MatSort;

  // tslint:disable-next-line:max-line-length
  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restPersonaService: RestPersonaService,
    private router: Router) {

    this.sceltaAmbito = this.storage.get('ambito');

    this.messaggioErrorePerm = 'Non sono presenti persone con accesso permanente';
    this.messaggioErroreLim = 'Non sono presenti persone con accesso a informazioni privilegiate';

    this.pathExportExcelPerm = `${this.restPersonaService.contextPath}rest/getExportPersonaInfoPerm`;
    this.pathExportExcelLim = `${this.restPersonaService.contextPath}rest/getExportPersonaInfoLim?arrayInfo=`;

    // resp per html
    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErroreInfoPerm = false;
    this.respMessaggioErroreInfoLim = false;

    this.restPersonaService.findListaPersoneInfoPermanenti().subscribe((resp) => {
      this.listaPersonaInfoPermanenti = resp;
      if (this.listaPersonaInfoPermanenti.length === 0) {
        this.respGif = false;
        this.respMessaggioErroreInfoPerm = true;
      } else {
        this.respGif = false;
        this.dataSourcePerm = new MatTableDataSource<Riga>(this.listaPersonaInfoPermanenti);
        this.respTabella = true;
      }
    });

    this.restPersonaService.findListaPersoneInfoLimitate().subscribe((resp) => {
      this.listaPersoneInfoSingole = resp;
      if (this.listaPersoneInfoSingole.length === 0) {
        this.respGif = false;
        this.respMessaggioErroreInfoLim = true;
      } else {
        this.respGif = false;
        this.dataSourceLim = new MatTableDataSource<Riga>(this.listaPersoneInfoSingole);

        for (let i = 0; i < this.dataSourceLim.data.length; i++)
          if (this.listaPersoneInfoSingole[i].info)
            this.check.push(false);
          else
            this.check.push(undefined);

        this.respTabella = true;
      }
    });

  }

  ngOnInit() {
    this.controlloListe();
  }

  controlloListe() {
    if (this.listaPersonaInfoPermanenti.length === 0 && this.listaPersoneInfoSingole.length === 0) {
      this.respTabella = false;
    } else {
      this.respTabella = true;
    }
  }

  controlCheck() {
    let control = this.check.some((x) => x === true);
    return control;
  }

  controlField(info: string, idSchedaPersona: string) {
    if (info === '')
      this.router.navigate(['/visualizza-scheda-persona/', idSchedaPersona]);
  }

  selectAll() {
    for (let i = 0; i < this.check.length; i++) {
      if (this.check[i] !== undefined)
        this.check[i] = this.selectedAll;
    }
  }

  setUrl() {
    let arr = new Array();
    this.pathExportExcelLim = `${this.restPersonaService.contextPath}rest/getExportPersonaInfoLim?arrayInfo=`;
    
    for (let i = 0; i < this.check.length; i++)
      if (this.check[i])
        arr.push(this.listaPersoneInfoSingole[i].info);

    arr.forEach(
      (x, index) => {
        if (index > 0)
          this.pathExportExcelLim += ',';

        this.pathExportExcelLim += x;
      }
    );
  }

}