import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { AccessoInformazioniService } from 'src/app/services/accesso-informazioni.service';
import { ModalAggiungiInformazioniComponent } from '../../modals/modal-aggiungi-informazioni/modal-aggiungi-informazioni.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalCessaInformazioniComponent } from '../../modals/modal-cessa-informazioni/modal-cessa-informazioni.component';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { UserWeb } from 'src/app/interfaces/user-web';
import { Global } from 'src/app/global';
import { AccessoInformazione } from 'src/app/classes/accesso-informazione';

@Component({
  selector: 'app-visualizza-informazioni-collegate',
  templateUrl: './visualizza-informazioni-collegate.component.html',
  styleUrls: ['./visualizza-informazioni-collegate.component.css']
})
export class VisualizzaInformazioniCollegateComponent implements OnInit {

  idSchedaPersona: string;
  schedaPersona: SchedaPersona;
  listaInformazioniCollegabili: SchedaInformazione[];
  listaInformazioniCollegate: AccessoInformazione[];
  listaInformazioniPersona: AccessoInformazione[];
  informazione: SchedaInformazione;
  tipoAccesso: string;
  user: UserWeb;

  count: number;
  respCessaDisabled: boolean;

  // resp per visualizzazione html
  respGif: boolean;
  respTabella: boolean;
  respScheda: boolean;
  respBottoniInsert: boolean;
  respBottoniDelete: boolean;
  respBottoniTabella: boolean;
  respForm1: boolean;
  respForm2: boolean;
  respForm3: boolean;
  respMessaggioCampi: boolean;
  respMessaggioTipoAccesso: boolean;
  valueBold: boolean;
  valueColor: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restAccessoInformazioniService: AccessoInformazioniService,
    private modalService: NgbModal,
    private activateRoute: ActivatedRoute,
    private restInformazioniService: RestInformazioneService,
    private restPersonaService: RestPersonaService,
    private global: Global) {

    this.respGif = true;
    this.respTabella = false;
    this.respScheda = false;
    this.respBottoniInsert = false;
    this.respBottoniDelete = false;
    this.respForm1 = false;
    this.respForm2 = false;
    this.respForm3 = false;
    this.respBottoniTabella = true;
    this.respMessaggioCampi = false;
    this.respMessaggioTipoAccesso = false;

    this.listaInformazioniPersona = [];

    this.user = this.storage.get('USER');

    if (this.user.ruolo === this.global.RUOLO_SUPERVISORE || this.user.ruolo === this.global.RUOLO_CONFIG) {
      this.respBottoniTabella = false;
    }

    this.count = 0;

    this.informazione = new SchedaInformazione();
    this.schedaPersona = new SchedaPersona();

    this.idSchedaPersona = '';
    this.idSchedaPersona = this.activateRoute.snapshot.paramMap.get('idSchedaPersona');
    this.tipoAccesso = '';

    this.findPersonaById();

  }

  ngOnInit() {
    this.loadTable();
  }

  private findPersonaById() {
    this.restPersonaService.findById(this.idSchedaPersona).subscribe((resp1) => {
      this.schedaPersona = resp1;
    });
  }

  private loadTable() {
    this.restAccessoInformazioniService.findInformazioniByIdPersona(this.idSchedaPersona).subscribe((resp3) => {
      this.listaInformazioniCollegate = resp3;

      this.respGif = false;
      this.respTabella = true;
      this.respScheda = true;

      if (this.user.ruolo === this.global.RUOLO_SUPERVISORE) {
        this.respBottoniTabella = false;
      }

      this.count = 0;

      for (let i = 0; i < this.listaInformazioniCollegate.length; i++) {
        if (this.listaInformazioniCollegate[i].dataCessazioneInformazione != null &&
          this.listaInformazioniCollegate[i].oraCessazioneInformazione != null) {
          this.count++;
        }

      }

      if (this.count === this.listaInformazioniCollegate.length) {
        this.respCessaDisabled = true;
        window.alert('Questa persona non ha accesso ad alcuna informazione!');
      } else {
        this.respCessaDisabled = false;
      }

    });

    this.restInformazioniService.findInformazioniDisponibiliByIdPersona(this.idSchedaPersona).subscribe(
      (resp2) => {
        this.listaInformazioniCollegabili = resp2;
      });
  }

  collegaInfo() {

    const ngbModalRef = this.modalService.open(ModalAggiungiInformazioniComponent, { size: 'lg' });
    ngbModalRef.componentInstance.schedaPersona = this.schedaPersona;
    ngbModalRef.componentInstance.listaInformazioniCollegabili = this.listaInformazioniCollegabili;
    ngbModalRef.componentInstance.listaInformazioniCollegate = this.listaInformazioniCollegate;
    ngbModalRef.result.then(
      result => {
        console.log(result);
        if (result == 'Informazione aggiunta' || result == 'Accesso Permanente') {
          this.loadTable();
        }
      }, //in close()
      reject => console.log(reject) //in dismiss()
    );
  }

  cessaInfo() {
    this.respTabella = false;
    this.respBottoniTabella = false;
    this.respGif = true;
    this.restInformazioniService.findInformazioniByIdPersona(this.idSchedaPersona).subscribe((resp2) => {
      this.listaInformazioniPersona = resp2;

      this.respGif = false;
      this.respTabella = true;
      this.respBottoniTabella = true;

      if (this.listaInformazioniPersona.length === 0) {
        this.respBottoniInsert = false;
      }

      const ngbModalRef = this.modalService.open(ModalCessaInformazioniComponent, { size: 'lg' });
      ngbModalRef.componentInstance.listaInformazioniCollegatePersona = this.listaInformazioniPersona;
      ngbModalRef.componentInstance.schedaPersona = this.schedaPersona;
      ngbModalRef.result.then(
        result => {
          console.log(result);
          if (result == 'Informazione cessata') {
            this.loadTable();
            this.findPersonaById();
          }
        }, //in close()
        reject => console.log(reject) //in dismiss()
      );

    });
  }

}
