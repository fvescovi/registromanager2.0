import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TemplateService } from 'src/app/services/template.service';
import { Template } from 'src/app/interfaces/template';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Global } from 'src/app/global';

@Component({
  selector: 'app-visualizza-template',
  templateUrl: './visualizza-template.component.html',
  styleUrls: ['./visualizza-template.component.css']
})
export class VisualizzaTemplateComponent implements OnInit {

  idTemplate: string;
  testoDefault: string;
  nomeDefault: string;
  nomeTemplate: string = '';
  legenda: string[] = [];
  template: Template = new Template();

  user: UserWeb;

  respRuoloGestione: boolean;
  respRuoloConfig: boolean;
  respTemplate: boolean;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private activateRoute: ActivatedRoute,
    private restTemplateService: TemplateService,
    private router: Router,
    private global: Global) {

    this.idTemplate = this.activateRoute.snapshot.paramMap.get('id');
    this.respTemplate = true;
    this.user = this.storage.get('USER');

    this.respRuoloGestione = false;
    this.respRuoloConfig = false;

    this.legenda = [
      "${Societa}",
      "${MotivoIscrizione}",
      "${DataUltimoAggiornamento}",
      "${Cognome}",
      "${Nome}",
      "${DataNascita}",
      "${CodiceFiscale}",
      "${Ruolo}",
      "${DataIscrizione}"
    ];

  }

  ngOnInit() {
    this.user.ruoli.forEach(
      (x) => {
        if (x === this.global.RUOLO_GESTIONE)
          this.respRuoloGestione = true;

        if (x === this.global.RUOLO_CONFIG)
          this.respRuoloConfig = true;
      }
    );

    this.restTemplateService.findById(this.idTemplate).subscribe((resp) => {
      this.template = resp;
      this.testoDefault = this.template.testo;
      this.nomeDefault = this.template.nomeTemplate;
      this.nomeTemplate = this.template.nomeTemplate;
    });
  }

  saveTemplate() {
    if (window.confirm('Desideri salvare le modifiche effettuate a questo template?')) {
      this.template.nomeTemplate = this.nomeDefault;
      this.template.testo = this.testoDefault;
      this.restTemplateService.addTemplate(this.template).subscribe(resp => {
        this.template = resp;
      });
      window.alert('Il template è stato modificato correttamente!');
      this.respTemplate = false;
      this.router.navigate(['/amministrazione']);
    } else {
      this.respTemplate = true;
    }
  }

  annullaModifica() {
    this.testoDefault = this.template.testo;
    this.nomeDefault = this.template.nomeTemplate;
  }

  controlField() {
    if ((this.template.nomeTemplate !== this.nomeDefault ||
      this.template.testo !== this.testoDefault) &&
      (this.nomeDefault.trim() !== '' && this.testoDefault.trim() !== ''))
      return false;

    else
      return true;
  }

}
