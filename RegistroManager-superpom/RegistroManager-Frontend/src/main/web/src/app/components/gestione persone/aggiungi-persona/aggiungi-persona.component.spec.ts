import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AggiungiPersonaComponent } from './aggiungi-persona.component';

describe('AggiungiPersonaComponent', () => {
  let component: AggiungiPersonaComponent;
  let fixture: ComponentFixture<AggiungiPersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AggiungiPersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AggiungiPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
