import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaSchedaInformazioniComponent } from './visualizza-scheda-informazioni.component';

describe('VisualizzaSchedaInformazioniComponent', () => {
  let component: VisualizzaSchedaInformazioniComponent;
  let fixture: ComponentFixture<VisualizzaSchedaInformazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaSchedaInformazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaSchedaInformazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
