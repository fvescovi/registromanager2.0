import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModifcaInformazioneComponent } from './modal-modifca-informazione.component';

describe('ModalModifcaInformazioneComponent', () => {
  let component: ModalModifcaInformazioneComponent;
  let fixture: ComponentFixture<ModalModifcaInformazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalModifcaInformazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalModifcaInformazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
