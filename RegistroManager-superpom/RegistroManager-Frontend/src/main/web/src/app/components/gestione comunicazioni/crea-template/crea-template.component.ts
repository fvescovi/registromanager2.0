import { Component, OnInit } from '@angular/core';
import { Template } from 'src/app/interfaces/template';
import { Router } from '@angular/router';
import { TemplateService } from 'src/app/services/template.service';

@Component({
  selector: 'app-crea-template',
  templateUrl: './crea-template.component.html',
  styleUrls: ['./crea-template.component.css']
})
export class CreaTemplateComponent implements OnInit {

  template: Template = new Template();

  respTemplate: boolean;
  legenda: string[] = [];

  constructor(private restTemplateService: TemplateService,
    private router: Router) {
    this.respTemplate = true;
    this.template.testo = '';
    this.template.nomeTemplate = '';

    this.legenda = [
      "${Societa}",
      "${MotivoIscrizione}",
      "${DataUltimoAggiornamento}",
      "${Cognome}",
      "${Nome}",
      "${DataNascita}",
      "${CodiceFiscale}",
      "${Ruolo}",
      "${DataIscrizione}"
    ];
  }

  ngOnInit() {
  }

  saveTemplate() {
    if (window.confirm('Desideri salvare questo template?')) {
      this.restTemplateService.addTemplate(this.template).subscribe(resp => {
        this.template = resp;
      });
      window.alert('Il template è stato salvato corretamente!');
      this.respTemplate = false;
      this.router.navigate(['/amministrazione']);
    } else {
      this.respTemplate = true;
    }
  }

  annullaModifica() {
    this.template = null;
  }

  controlField() {
    if (!this.template.nomeTemplate ||
      !this.template.testo ||
      this.template.nomeTemplate.trim() === '' ||
      this.template.testo.trim() === '')
      return true;

    else
      return false;
  }

}
