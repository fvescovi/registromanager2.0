import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  error: string;
  constructor(private activedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.error = this.activedRoute.snapshot.paramMap.get('error');
  }

}
