import { Component, OnInit, ViewChild } from '@angular/core';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { RicercaAccesso } from 'src/app/classes/ricerca-accesso';
import { AccessoInformazione } from 'src/app/classes/accesso-informazione';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';

@Component({
  selector: 'app-ricerca-per-data',
  templateUrl: './ricerca-per-data.component.html',
  styleUrls: ['./ricerca-per-data.component.css']
})
export class RicercaPerDataComponent implements OnInit {

  informazionePrivilegiata: string = '';
  pathExportExcel: string = '';

  startDate: Date = null;
  endDate: Date = null;
  maxDate: Date = new Date();

  respTabella: boolean = false;
  respGif: boolean = false;
  respMessaggioAccessiAssenti: boolean = false;
  listaSchedaInformazione: SchedaInformazione[] = [];
  accessList: AccessoInformazione[] = [];

  ricercaAccesso: RicercaAccesso = new RicercaAccesso();

  displayedColumns: string[] = ['1', '2', '3', '4', '5', '6', '7'];
  dataSource: MatTableDataSource<AccessoInformazione> = null;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private restInformazioneService: RestInformazioneService,
    private restPersonaService: RestPersonaService) { }

  ngOnInit() {
    this.restInformazioneService.findAllInformazione().subscribe((resp) => {
      this.listaSchedaInformazione = resp;
    });
  }

  controlField() {
    if (!this.informazionePrivilegiata ||
      this.informazionePrivilegiata.trim() === '' ||
      !this.startDate || !this.endDate)
      return true;

    else
      return false;
  }

  search() {
    this.ricercaAccesso.informazionePrivilegiata = this.informazionePrivilegiata;
    this.ricercaAccesso.startDate = this.startDate;
    this.ricercaAccesso.endDate = this.endDate;
    this.respMessaggioAccessiAssenti = false;
    this.respGif = true;

    this.restInformazioneService.findAccessListByRicercaAccesso(JSON.stringify(this.ricercaAccesso)).subscribe((data) => {
      this.accessList = data;
      this.respGif = false;
      if (this.accessList.length === 0) {
        this.respMessaggioAccessiAssenti = true;
        this.respTabella = false;
      } else {
        let arrayInfo: string[] = new Array();
        arrayInfo.push(this.ricercaAccesso.informazionePrivilegiata);
        this.pathExportExcel = `${this.restPersonaService.contextPath}rest/getElencoPersonaBetweenDate/${JSON.stringify(this.ricercaAccesso)}/?arrayInfo=${arrayInfo[0]}`;
        this.dataSource = new MatTableDataSource<AccessoInformazione>(this.accessList);
        this.respTabella = true;
        this.dataSource.sort = this.sort;
      }
    });
  }

  reset() {
    this.informazionePrivilegiata = '';
    this.startDate = null;
    this.endDate = null;
    this.respGif = false;
    this.respMessaggioAccessiAssenti = false;
    this.respTabella = false;
  }

}
