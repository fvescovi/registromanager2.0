import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaTemplateComponent } from './visualizza-template.component';

describe('VisualizzaTemplateComponent', () => {
  let component: VisualizzaTemplateComponent;
  let fixture: ComponentFixture<VisualizzaTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
