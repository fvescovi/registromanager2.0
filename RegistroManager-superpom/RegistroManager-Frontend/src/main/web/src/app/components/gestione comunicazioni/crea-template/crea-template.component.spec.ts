import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreaTemplateComponent } from './crea-template.component';

describe('CreaTemplateComponent', () => {
  let component: CreaTemplateComponent;
  let fixture: ComponentFixture<CreaTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreaTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreaTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
