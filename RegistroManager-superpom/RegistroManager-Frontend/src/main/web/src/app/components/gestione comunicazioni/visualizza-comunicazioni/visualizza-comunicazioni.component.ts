import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { ComunicazioneService } from 'src/app/services/comunicazione.service';
import { ComunicazionePersona } from 'src/app/classes/comunicazione-persona';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-visualizza-comunicazioni',
  templateUrl: './visualizza-comunicazioni.component.html',
  styleUrls: ['./visualizza-comunicazioni.component.css']
})
export class VisualizzaComunicazioniComponent implements OnInit {

  listaEmail: ComunicazionePersona[];
  listaLettera: ComunicazionePersona[];
  comunicazionePersona: ComunicazionePersona;

  tipoEmail: string;
  tipoLettera: string;

  ambito: string;

  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  messaggioErrore: string;
  messaggioEmailVuote: string;
  messaggioLettereVuote: string;

  displayedColumns1: string[] = ['1', '2', '3', '4', '5','6'];
  displayedColumns2: string[] = ['1', '2', '3', '4', '5'];
  dataSource1: MatTableDataSource<ComunicazionePersona> = null;
  dataSource2: MatTableDataSource<ComunicazionePersona> = null;
  @ViewChild(MatSort) sort: MatSort;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
  private restComunicazioneService: ComunicazioneService) {

    this.ambito = this.storage.get('ambito');

    this.listaEmail = [];
    this.listaLettera = [];
    this.comunicazionePersona = new ComunicazionePersona();

    this.messaggioErrore = 'Non sono presenti comunicazioni nel Database';
    this.messaggioEmailVuote = 'Non sono presenti Email inviate nel Database';
    this.messaggioLettereVuote = 'Non sono presenti Lettere stampate nel Database';

    this.tipoEmail = 'Email';
    this.tipoLettera = 'Lettera';

    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErrore = false;
  }

  ngOnInit() {
    this.restComunicazioneService.caricaListaComunicazione(this.ambito).subscribe((resp) => {
      this.listaEmail = resp[0];
      this.listaLettera = resp[1];
      if (this.listaEmail.length === 0 && this.listaLettera.length === 0) {
        this.respGif = false;
        this.respMessaggioErrore = true;
      } else {
        this.respGif = false;
        this.dataSource1 = new MatTableDataSource<ComunicazionePersona>(this.listaEmail);
        this.dataSource2 = new MatTableDataSource<ComunicazionePersona>(this.listaLettera);
        this.respTabella = true;
        this.dataSource1.sort = this.sort;
      }
    });
  }

}
