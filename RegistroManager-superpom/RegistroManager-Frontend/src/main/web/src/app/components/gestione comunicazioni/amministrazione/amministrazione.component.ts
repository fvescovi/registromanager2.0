import { Component, OnInit, ViewChild } from '@angular/core';
import { Template } from 'src/app/interfaces/template';
import { MatTableDataSource, MatSort } from '@angular/material';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { TemplateService } from 'src/app/services/template.service';

@Component({
  selector: 'app-amministrazione',
  templateUrl: './amministrazione.component.html',
  styleUrls: ['./amministrazione.component.css']
})
export class AmministrazioneComponent implements OnInit {

  listaTemplate: Template[];
  template: Template;

  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  messaggioErrore: string;

  displayedColumns1: string[] = ['1', '2'];
  dataSource1: MatTableDataSource<Template> = null;
  @ViewChild(MatSort) sort: MatSort;
  

  constructor(private restPersonaService: RestPersonaService,
    private restTemplateService: TemplateService) {

    this.listaTemplate = [];
    this.template = new Template();

    this.messaggioErrore = 'Non sono presenti template nel Database';

    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErrore = false;

  }

  ngOnInit() {

    this.restTemplateService.caricaListaTemplate().subscribe((resp) => {
      this.listaTemplate = resp;
      if (this.listaTemplate.length === 0) {
        this.respGif = false;
        this.respMessaggioErrore = true;
      } else {
        this.respGif = false;
        this.dataSource1 = new MatTableDataSource<Template>(this.listaTemplate);
        this.respTabella = true;
        this.dataSource1.sort = this.sort;
      }
    });

  }

}
