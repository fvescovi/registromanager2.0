import { Component, OnInit, Input, Inject } from '@angular/core';
import { AccessoInformazioniService } from 'src/app/services/accesso-informazioni.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Global } from 'src/app/global';
import { AccessoInformazione } from 'src/app/classes/accesso-informazione';

@Component({
  selector: 'app-modal-cessa-informazioni',
  templateUrl: './modal-cessa-informazioni.component.html',
  styleUrls: ['./modal-cessa-informazioni.component.css']
})
export class ModalCessaInformazioniComponent implements OnInit {

  @Input() schedaPersona: SchedaPersona;
  @Input() listaInformazioniCollegatePersona: AccessoInformazione[];

  respBottoniDelete: boolean;
  respMessaggioConferma: boolean;

  maxDate: Date = new Date();
  minDate: Date = null;

  accessoInformazione: AccessoInformazione = new AccessoInformazione();

  oraInfo: string;
  dataInfo: Date;

  user: UserWeb;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restAccessoInformazioniService: AccessoInformazioniService,
    public activeModal: NgbActiveModal,
    public global: Global) {

    this.respBottoniDelete = true;
    this.oraInfo = '';

    this.user = this.storage.get('USER');
  }

  ngOnInit() {
    if (this.listaInformazioniCollegatePersona.length === 1)
      this.minDate = this.listaInformazioniCollegatePersona[0].dataAccessoInformazione;
  }

  onChangeDate() {
    this.minDate = this.accessoInformazione.dataAccessoInformazione;
  }

  confermaSceltaDelete() {
    if (window.confirm('Desideri cessare tale informazione privilegiata?')) {

      if (this.listaInformazioniCollegatePersona[0].schedaInformazione.informazionePrivilegiata === this.global.ACCESSO_PERMANENTE)
        this.accessoInformazione = this.listaInformazioniCollegatePersona[0];

      this.accessoInformazione.dataCessazioneInformazione = this.dataInfo;
      this.accessoInformazione.oraCessazioneInformazione = this.oraInfo;

      this.restAccessoInformazioniService.cessaAccessoInformazione(this.schedaPersona.idSchedaPersona, this.accessoInformazione).subscribe(
        () => {
          this.respBottoniDelete = false;
          this.respMessaggioConferma = true;
          this.activeModal.close('Informazione cessata');
        }
      );
    }
  }

  annullaScelta() {
    this.respBottoniDelete = false;
    this.activeModal.close('Modal chiusa su Annulla');
  }

}
