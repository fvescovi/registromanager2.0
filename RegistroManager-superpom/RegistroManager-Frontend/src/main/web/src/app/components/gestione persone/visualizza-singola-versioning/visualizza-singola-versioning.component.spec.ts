import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaSingolaVersioningComponent } from './visualizza-singola-versioning.component';

describe('VisualizzaSingolaVersioningComponent', () => {
  let component: VisualizzaSingolaVersioningComponent;
  let fixture: ComponentFixture<VisualizzaSingolaVersioningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaSingolaVersioningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaSingolaVersioningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
