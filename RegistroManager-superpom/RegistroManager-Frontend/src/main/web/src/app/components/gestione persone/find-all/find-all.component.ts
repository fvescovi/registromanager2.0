import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-find-all',
  templateUrl: './find-all.component.html',
  styleUrls: ['./find-all.component.css']
})
export class FindAllComponent implements OnInit {

  listaPersone: SchedaPersona[];
  messaggioErrore: string;
  pathExportExcel: string;
  sceltaAmbito: string;

  // resp per visualizzazione html
  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  // variabili mat table material
  displayedColumns1: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  displayedColumns2: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  dataSource1: MatTableDataSource<SchedaPersona> = null;
  @ViewChild(MatSort) sort: MatSort;

  // tslint:disable-next-line:max-line-length
  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService, private restPersonaService: RestPersonaService) {

    this.sceltaAmbito = this.storage.get('ambito');

    this.messaggioErrore = 'Non sono presenti persone nel Database';
    this.listaPersone = [];
    this.pathExportExcel = '';

    // resp per html
    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErrore = false;


  }

  ngOnInit() {
    this.restPersonaService.findAllPersona(this.sceltaAmbito).subscribe((resp) => {
      this.listaPersone = resp;
      if (this.listaPersone.length === 0) {
        this.respGif = false;
        this.respMessaggioErrore = true;
      } else {
        this.pathExportExcel = `${this.restPersonaService.contextPath}rest/getExportElencoSchedePersona/${this.sceltaAmbito}`;
        this.respGif = false;
        this.dataSource1 = new MatTableDataSource<SchedaPersona>(this.listaPersone);
        this.respTabella = true;
        this.dataSource1.sort = this.sort;
      }
    });
  }
  
}