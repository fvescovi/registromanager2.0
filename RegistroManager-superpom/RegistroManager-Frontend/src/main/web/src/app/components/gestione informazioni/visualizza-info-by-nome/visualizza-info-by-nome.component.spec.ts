import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaInfoByNomeComponent } from './visualizza-info-by-nome.component';

describe('VisualizzaInfoByNomeComponent', () => {
  let component: VisualizzaInfoByNomeComponent;
  let fixture: ComponentFixture<VisualizzaInfoByNomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaInfoByNomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaInfoByNomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
