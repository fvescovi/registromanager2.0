import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaComunicazioneComponent } from './visualizza-comunicazione.component';

describe('VisualizzaComunicazioneComponent', () => {
  let component: VisualizzaComunicazioneComponent;
  let fixture: ComponentFixture<VisualizzaComunicazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaComunicazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaComunicazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
