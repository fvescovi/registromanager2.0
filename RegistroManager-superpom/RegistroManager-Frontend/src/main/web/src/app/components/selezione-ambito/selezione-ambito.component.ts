import { Component, OnInit, Inject } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { ExportcsvService } from 'src/app/services/exportcsv.service';

@Component({
  selector: 'app-selezione-ambito',
  templateUrl: './selezione-ambito.component.html',
  styleUrls: ['./selezione-ambito.component.css']
})
export class SelezioneAmbitoComponent implements OnInit {

  respBottoni: boolean;
  respAppStart: boolean;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restExportCsvService: ExportcsvService) {

    this.respBottoni = true;
    this.respAppStart = false;
  
  }

  ngOnInit() { 

  }

  sceltaR(scelta: string) {
    this.storage.set('ambito', scelta);
    this.respBottoni = false;
    this.respAppStart = true;
  }

  sceltaI(scelta: string) {
    this.storage.set('ambito', scelta);
    this.respBottoni = false;
    this.respAppStart = true;
  }

// METODO PER IMPORT CSV

//  exportDatiCsv() {
//     this.restExportCsvService.exportSchedaPersonaCsv().subscribe();
//     this.restExportCsvService.exportAccessoInformazioneCsv().subscribe();
//     this.restExportCsvService.exportLetteraCsv().subscribe();
//     this.restExportCsvService.exportMailCsv().subscribe();
//  }

}
