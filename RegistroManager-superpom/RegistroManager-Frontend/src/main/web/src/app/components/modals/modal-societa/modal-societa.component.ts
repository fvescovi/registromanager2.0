import { Component, OnInit, Input } from '@angular/core';
import { RestSocietaService } from 'src/app/services/rest-societa.service';
import { Societa } from 'src/app/interfaces/societa';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Global } from 'src/app/global';

@Component({
  selector: 'app-modal-societa',
  templateUrl: 'modal-societa.component.html'
})
export class ModalSocietaComponent implements OnInit {

  societa: Societa;
  listaSocieta: Societa[];

  nome: string;
  iniziale: string;

  @Input() respAddSocieta: boolean;
  @Input() respDeleteSocieta: boolean;

  constructor(private restSocietaService: RestSocietaService,
    public activeModal: NgbActiveModal,
    public global: Global) {

      this.nome = '';
      this.iniziale = '';
      this.listaSocieta = [];
      this.societa = new Societa();

    }

  ngOnInit() {
    this.restSocietaService.getListaSocieta().subscribe((resp) => {
      this.listaSocieta = resp;
    });
  }

  saveSocieta() {
    if (window.confirm('Desideri aggiungere questa società?')) {
      this.societa.nome = this.nome;
      this.societa.iniziale = this.iniziale;
      this.societa.stato = this.global.STATO_DEFINITIVO;
      this.restSocietaService.insertSocieta(this.societa).subscribe(() => {
        this.restSocietaService.getListaSocieta().subscribe((res) => {
          this.activeModal.close(res);
        });
      });
    } else {
      this.respAddSocieta = true;
    }
  }

  deleteSocieta() {
    if (window.confirm('Desideri rimuovere questa società?')) {
      this.societa.stato = this.global.STATO_CANCELLATO;
      this.restSocietaService.insertSocieta(this.societa).subscribe(() => {
        this.restSocietaService.getListaSocieta().subscribe((res) => {
          this.activeModal.close(res);
        });
      });
    } else {
      this.respDeleteSocieta = true;
    }
  }

  annullaScelta() {
    if(this.respAddSocieta === true)
    this.activeModal.close('Modal aggiungi chiusa su Annulla');
    
    else if(this.respDeleteSocieta === true)
    this.activeModal.close('Modal rimuovi chiusa su annulla');
  }

}
