import { Component, OnInit } from '@angular/core';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { ActivatedRoute, Router } from '@angular/router';
import { WebStorageService, SESSION_STORAGE } from "angular-webstorage-service";
import { Inject } from "@angular/core";

@Component({
    selector: 'app-visualizza-singola-versioning',
    templateUrl: './visualizza-singola-versioning.component.html',
    styleUrls: ['./visualizza-singola-versioning.component.css']
})
export class VisualizzaSingolaVersioningComponent implements OnInit {

    idVersioning: string;
    sceltaAmbito: string;

    schedaPersona: SchedaPersona;

    // resp html
    respTabella: boolean;
    respScheda: boolean;
    respGif: boolean;

    constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
        private activateRoute: ActivatedRoute,
        private restPersonaService: RestPersonaService,
        private router: Router) {

        this.sceltaAmbito = this.storage.get('ambito');

        this.idVersioning = '';
        this.idVersioning = this.activateRoute.snapshot.paramMap.get('idVersioning');

        this.respTabella = false;
        this.respScheda = false;
        this.respGif = true;
    }

    ngOnInit() {
        this.restPersonaService.findVersioningById(this.idVersioning).subscribe((resp) => {
            this.schedaPersona = resp;
            this.respGif = false;
            this.respScheda = true;
            this.respTabella = true;
        });
    }

    goToVisualizzaVersioning() {
        this.router.navigate(['visualizza-versioning', this.schedaPersona.idSchedaPersona]);
    }

}
