import { Component, OnInit, Input, Inject } from '@angular/core';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Template } from 'src/app/interfaces/template';
import { TemplateService } from 'src/app/services/template.service';
import { ComunicazioneService } from 'src/app/services/comunicazione.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-modal-mail',
  templateUrl: './modal-mail.component.html',
  styleUrls: ['./modal-mail.component.css']
})
export class ModalMailComponent implements OnInit {

  @Input() schedaPersona: SchedaPersona;
  @Input() action: string;

  pathExportPdf: string;
  respLettera: boolean;
  respMail: boolean;

  defaultTemplate: string;
  personeCc: string;
  oggetto: string;

  ambito: string;

  listaTemplate: Template[];
  template: Template;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restComunicazioneService: ComunicazioneService,
    public activeModal: NgbActiveModal,
    private datePipe: DatePipe,
    private restTemplateService: TemplateService) {
    this.respLettera = false;
    this.respMail = false;
    this.pathExportPdf = '';
    this.defaultTemplate = '';
    this.personeCc = '';
    this.oggetto = '';
    this.template = new Template();

    this.ambito = this.storage.get('ambito');
  }

  ngOnInit() {
    this.restTemplateService.getListaTemplate(this.schedaPersona).subscribe((resp) => {
      this.listaTemplate = resp;
    });

    if (this.action === 'email') {
      this.respMail = true;
    }

    else {
      this.respLettera = true;
    }

  }

  inviaMail() {
    if (window.confirm('Desideri inviare questa e-mail?')) {
      this.template.personeCc = this.personeCc;
      this.template.testo = this.defaultTemplate;
      this.restComunicazioneService.inviaEmail(this.schedaPersona.idSchedaPersona, this.oggetto, this.ambito, this.template).subscribe((resp) => {
        this.respMail = false;
        
        if (resp != null)
          this.activeModal.close('Mail inviata correttamente');
        else
          this.activeModal.close('Errore mail');

      });

    }
  }

  stampaLettera() {
    if (window.confirm('Desideri scaricare questa lettera?')) {
      this.template.testo = this.defaultTemplate;
      this.restComunicazioneService.stampaLettera(this.schedaPersona.idSchedaPersona, this.ambito, this.template).subscribe((resultFile: Blob) => {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(resultFile);
        }
        else {
          var downloadURL = URL.createObjectURL(resultFile);
          window.open(downloadURL);
        }
      });
      this.respLettera = false;
      this.activeModal.close('Lettera stampata correttamente');
    }
  }

  annullaScelta() {
    this.activeModal.close('Modal chiusa su Annulla');
  }

  transformDate(date: string | Date) {
    return this.datePipe.transform(date, 'dd/MM/yyyy');
  }

  onChange(template: Template) {
    if (template !== null)
      this.defaultTemplate = template.testo;
  }

}
