import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { Societa } from 'src/app/interfaces/societa';
import { Provincia } from 'src/app/classes/provincia';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { ControlloCampi } from 'src/app/classes/utility/controllo-campi';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { RestSocietaService } from 'src/app/services/rest-societa.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { UserWeb } from 'src/app/interfaces/user-web';
import { Global } from 'src/app/global';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MY_FORMATS } from "src/app/app.module";

@Component({
  selector: 'app-aggiungi-persona',
  templateUrl: './aggiungi-persona.component.html',
  styleUrls: ['./aggiungi-persona.component.css'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})

export class AggiungiPersonaComponent implements OnInit {

  listaProvince: Provincia;
  schedaPersona: SchedaPersona;
  controlloCampi: ControlloCampi;
  listaValori: any;
  listaSocieta: Societa[];
  sceltaAmbito: string;

  step: number[];
  currentStep: number;

  user: UserWeb;
  maxDate = new Date;
  // Response per visualizzazione html
  respModulo1: boolean;
  respModulo2: boolean;
  respModulo3: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restPersonaService: RestPersonaService,
    private restSocietaService: RestSocietaService,
    private global: Global,
    private router: Router) {

    this.sceltaAmbito = this.storage.get('ambito');

    this.user = this.storage.get('USER');

    this.listaProvince = new Provincia();
    this.schedaPersona = new SchedaPersona();
    this.controlloCampi = new ControlloCampi();

    this.listaValori = [];
    this.listaSocieta = [];
    this.step = [];
    this.currentStep = 1;

    this.respModulo1 = true;
    this.respModulo2 = false;
    this.respModulo3 = false;
    this.respTabella = false;
    this.respMessaggioErrore = false;

    this.schedaPersona.nomeIndirizzoImpresa = 'Sara Assicurazioni - Via Po, 20 – 00198 – Roma';
  }

  ngOnInit() {
    this.loadStep(this.global.STEP_PG);
  }

  loadStep(steps: number) {
    this.step = [];
    for (let i = 1; i <= steps; i++) {
      this.step.push(i);
    }
  }

  onChange(tipologiaSoggetto: string) {
    if (tipologiaSoggetto === 'Persona Giuridica') {
      this.loadStep(this.global.STEP_PG);
    } else {
      this.loadStep(this.global.STEP_PF);
    }
  }

  setModulo1() {
    this.listaValori = [];
    this.listaValori.push(this.schedaPersona.tipologiaSoggetto);
    this.listaValori.push(this.schedaPersona.nome);
    this.listaValori.push(this.schedaPersona.cognome);
    this.listaValori.push(this.schedaPersona.dataNascita);

    if (this.sceltaAmbito === this.global.AMBITO_INSIDER_LIST) {
      this.listaValori.push(this.schedaPersona.nomeIndirizzoImpresa);
      this.listaValori.push(this.schedaPersona.dataIscrizione);
    }

    if(this.sceltaAmbito === this.global.AMBITO_REGISTRO_MANAGER) {
      this.listaValori.push(this.schedaPersona.dataInserimento);
    }

    this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);

    if (this.respMessaggioErrore === false) {
      this.currentStep++;
      this.respModulo1 = false;

      if (this.schedaPersona.tipologiaSoggetto === 'Persona Giuridica')
        this.respModulo2 = true;

      else
        this.respModulo3 = true;

      this.restSocietaService.getListaSocieta().subscribe((resp) => {
        this.listaSocieta = resp;
      });
    } else {
      window.alert('Inserire tutti i campi obbligatori!');
      this.respModulo1 = true;
    }
  }

  setModulo2() {
    this.listaValori = [];
    this.listaValori.push(this.schedaPersona.partitaIva);
    this.listaValori.push(this.schedaPersona.ragioneSociale);
    this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);
    if (this.respMessaggioErrore === false) {
      this.respModulo2 = false;
      this.respModulo3 = true;
      this.currentStep++;
    } else {
      window.alert('Inserire tutti i campi obbligatori!');
      this.respModulo2 = true;
    }
  }

  setModulo3() {
    this.listaValori = [];
    this.listaValori.push(this.schedaPersona.societa);
    this.listaValori.push(this.schedaPersona.codiceFiscale);
    this.listaValori.push(this.schedaPersona.ruolo);

    if (this.sceltaAmbito === this.global.AMBITO_INSIDER_LIST) {
      this.listaValori.push(this.schedaPersona.motivoIscrizione);
      this.listaValori.push(this.schedaPersona.descrizioneInformazione);
    }

    this.respMessaggioErrore = this.controlloCampi.controllo(this.listaValori);

    if (this.respMessaggioErrore === false) {
      this.currentStep++;
        this.respMessaggioErrore = false;
        this.respModulo3 = false;
        this.respTabella = true;
    } else {
      window.alert('Inserire tutti i campi obbligatori!');
      this.respModulo3 = true;
    }
  }

  resetModulo1() {
    this.schedaPersona = new SchedaPersona();
    this.respMessaggioErrore = false;
    this.currentStep = 1;
  }

  resetModulo2() {
    this.respModulo2 = false;
    this.respModulo1 = true;
    this.respMessaggioErrore = false;
    this.currentStep--;
  }

  resetModulo3() {
    this.respMessaggioErrore = false;
    if (this.schedaPersona.tipologiaSoggetto === 'Persona Giuridica') {
      this.respModulo3 = false;
      this.respModulo2 = true;
    } else {
      this.respModulo3 = false;
      this.respModulo2 = false;
      this.respModulo1 = true;
    }
    this.currentStep--;
  }

  saveSchedaPersona() {
    this.respTabella = false;
    if (window.confirm('Desideri salvare la scheda persona?')) {
      window.alert('La scheda persona è stata salvata correttamente!');
      this.schedaPersona.stato = this.global.STATO_DEFINITIVO;
      this.schedaPersona.tipoAmbito = this.sceltaAmbito;
      this.restPersonaService.insertPersona(this.user.username, this.schedaPersona).subscribe(
        (resp) => {
          this.router.navigate(['/find-all'])
        });
      this.resetSchedaPersona();
    } else {
      this.respTabella = true;
    }
  }

  saveBozzaSchedaPersona() {
    this.respTabella = false;
    if (window.confirm('Desideri salvare la scheda persona in bozze?')) {
      window.alert('La scheda persona è stata salvata in bozze correttamente!');
      this.schedaPersona.stato = this.global.STATO_BOZZA;
      this.schedaPersona.tipoAmbito = this.sceltaAmbito;
      this.restPersonaService.insertPersona(this.user.username, this.schedaPersona).subscribe(
        (resp) => {
          this.router.navigate(['/find-all'])
        });
      this.resetSchedaPersona();
    } else {
      this.respTabella = true;
    }
  }

  editSchedaPersona() {
    this.respTabella = false;
    this.respModulo1 = true;
    this.currentStep = 1;

    if (this.schedaPersona.tipologiaSoggetto === 'Persona Giuridica')
      this.loadStep(this.global.STEP_PG);

    else
      this.loadStep(this.global.STEP_PF);
  }

  resetSchedaPersona() {
    this.respTabella = false;
    this.respModulo1 = true;
    this.schedaPersona = new SchedaPersona();
    this.schedaPersona.nomeIndirizzoImpresa = 'Sara Assicurazioni - Via Po, 20 – 00198 – Roma';
    this.currentStep = 1;
  }

  validateCodiceFiscale(codiceFiscale) {
    var re = /^(?:(?:[B-DF-HJ-NP-TV-Z]|[AEIOU])[AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i
    if (!re.test(String(codiceFiscale).toUpperCase())) {
      window.alert('Codice fiscale non valido!');
      this.schedaPersona.codiceFiscale = null;
      return null;
    }
  }

}
