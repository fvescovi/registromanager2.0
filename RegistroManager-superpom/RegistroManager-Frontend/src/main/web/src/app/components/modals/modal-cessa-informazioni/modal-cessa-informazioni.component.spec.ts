import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCessaInformazioniComponent } from './modal-cessa-informazioni.component';

describe('ModalInformazioniComponent', () => {
  let component: ModalCessaInformazioniComponent;
  let fixture: ComponentFixture<ModalCessaInformazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCessaInformazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCessaInformazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
