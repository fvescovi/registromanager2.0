import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaVersioningComponent } from './visualizza-versioning.component';

describe('VisualizzaVersioningComponent', () => {
  let component: VisualizzaVersioningComponent;
  let fixture: ComponentFixture<VisualizzaVersioningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaVersioningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaVersioningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
