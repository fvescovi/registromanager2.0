import { Component, OnInit } from '@angular/core';
import { Comunicazione } from 'src/app/interfaces/comunicazione';
import { ActivatedRoute, Router } from '@angular/router';
import { ComunicazioneService } from 'src/app/services/comunicazione.service';
import { ComunicazionePersona } from 'src/app/classes/comunicazione-persona';

@Component({
  selector: 'app-visualizza-comunicazione',
  templateUrl: './visualizza-comunicazione.component.html',
  styleUrls: ['./visualizza-comunicazione.component.css']
})
export class VisualizzaComunicazioneComponent implements OnInit {

  comunicazionePersona: ComunicazionePersona;
  idComunicazionePersona: string;

  constructor(private activateRoute: ActivatedRoute,
    private restComunicazioneService: ComunicazioneService,
    private route: Router) {
    this.idComunicazionePersona = this.activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.restComunicazioneService.findById(this.idComunicazionePersona).subscribe((resp) => {
      this.comunicazionePersona = resp;
    });
  }

  navigateToFindAll() {
    this.route.navigate(['/visualizza-comunicazioni']);
  }

}
