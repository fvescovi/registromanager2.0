import { Component, OnInit, Inject } from '@angular/core';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Router } from '@angular/router';
import { Global } from 'src/app/global';

@Component({
  selector: 'app-creazione-informazione',
  templateUrl: './creazione-informazione.component.html',
  styleUrls: ['./creazione-informazione.component.css']
})
export class CreazioneInformazioneComponent implements OnInit {

  schedaInformazione: SchedaInformazione = new SchedaInformazione();

  user: UserWeb;
  oraIdentificazione: string = '';
  maxDate: Date = new Date();

  regex: RegExp = /^([^\s])+((?!\s{2,}).)*$/g;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restInformazioneService: RestInformazioneService,
    private router: Router,
    private global: Global) {

    this.user = this.storage.get('USER');

  }

  ngOnInit() { }

  saveSchedaInformazione() {
    this.schedaInformazione.oraIdentificazione = this.oraIdentificazione;
    this.schedaInformazione.autoreInformazione = this.user.username;
    this.schedaInformazione.stato = this.global.STATO_INFO_ATTIVA;
    this.restInformazioneService.insertInformazione(this.schedaInformazione).subscribe(
      (resp) => {
        window.alert('Informazione privilegiata inserita correttamente!');
        this.gotoFindAll();
      }
    );
    this.resetSchedaInformazione();
  }

  gotoFindAll() {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(
      () => this.router.navigate(['/find-all-informazioni']));
  }

  resetSchedaInformazione() {
    this.schedaInformazione = new SchedaInformazione();
    this.oraIdentificazione = null;
  }

  controlField() {
    if (!this.schedaInformazione.informazionePrivilegiata ||
      !this.schedaInformazione.dataIdentificazione ||
      !this.oraIdentificazione ||
      this.oraIdentificazione === '' ||
      this.schedaInformazione.informazionePrivilegiata.trim() === '' ||
      !this.schedaInformazione.informazionePrivilegiata.match(this.regex) ||
      this.schedaInformazione.informazionePrivilegiata === 'Accesso Permanente')
      return true;

    else
      return false;
  }

  controlInfo() {
    if (!this.schedaInformazione.informazionePrivilegiata ||
      this.schedaInformazione.informazionePrivilegiata.trim() === '' ||
      !this.schedaInformazione.informazionePrivilegiata.match(this.regex))
      return true;

    else
      return false;
  }

}
