import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RicercaPerDataComponent } from './ricerca-per-data.component';

describe('RicercaPerDataComponent', () => {
  let component: RicercaPerDataComponent;
  let fixture: ComponentFixture<RicercaPerDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RicercaPerDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RicercaPerDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
