import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaPerStatoComponent } from './visualizza-per-stato.component';

describe('VisualizzaPerStatoComponent', () => {
  let component: VisualizzaPerStatoComponent;
  let fixture: ComponentFixture<VisualizzaPerStatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaPerStatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaPerStatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
