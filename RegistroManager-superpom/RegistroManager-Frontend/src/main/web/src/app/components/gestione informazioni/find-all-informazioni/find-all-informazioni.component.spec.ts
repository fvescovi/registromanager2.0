import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindAllInformazioniComponent } from './find-all-informazioni.component';

describe('FindAllInformazioniComponent', () => {
  let component: FindAllInformazioniComponent;
  let fixture: ComponentFixture<FindAllInformazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindAllInformazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindAllInformazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
