import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreazioneInformazioneComponent } from './creazione-informazione.component';

describe('CreazioneInformazioneComponent', () => {
  let component: CreazioneInformazioneComponent;
  let fixture: ComponentFixture<CreazioneInformazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreazioneInformazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreazioneInformazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
