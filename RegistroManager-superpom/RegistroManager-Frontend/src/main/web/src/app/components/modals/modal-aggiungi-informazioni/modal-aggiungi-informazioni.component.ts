import { Component, OnInit, Input, Inject } from '@angular/core';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AccessoInformazioniService } from 'src/app/services/accesso-informazioni.service';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Global } from 'src/app/global';
import { AccessoInformazione } from 'src/app/classes/accesso-informazione';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';

@Component({
  selector: 'app-modal-aggiungi-informazioni',
  templateUrl: './modal-aggiungi-informazioni.component.html',
  styleUrls: ['./modal-aggiungi-informazioni.component.css']
})
export class ModalAggiungiInformazioniComponent implements OnInit {

  @Input() listaInformazioniCollegabili: SchedaInformazione[];
  @Input() listaInformazioniCollegate: AccessoInformazione[];
  @Input() schedaPersona: SchedaPersona;

  schedaInformazione: SchedaInformazione;

  tipoAccesso: string;
  nuovaMotivazioneAccesso: string;
  oraInfo: string;

  maxDate: Date = new Date();

  count: number;

  dataInfo: Date;

  respMessaggioCampi: boolean;
  respMessaggioConferma: boolean;
  respGif: boolean;
  respBottoniDelete: boolean;
  respAccessoPermanente: boolean;

  user: UserWeb;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    public activeModal: NgbActiveModal,
    private restAccessoInformazioniService: AccessoInformazioniService,
    private restInformazioneService: RestInformazioneService,
    public global: Global) {

    this.count = 0;
    this.nuovaMotivazioneAccesso = null;
    this.respAccessoPermanente = true;
    this.oraInfo = '';
    this.dataInfo = new Date();

    this.user = this.storage.get('USER');
  }

  ngOnInit() {

    this.restInformazioneService.findAccessoPermanente().subscribe((resp) => {

      if (resp !== null)
        this.schedaInformazione = resp;

      else {
        this.schedaInformazione = new SchedaInformazione();
        this.schedaInformazione.dataIdentificazione = new Date();
        this.schedaInformazione.oraIdentificazione = new Date().getHours().toString() + ':' + new Date().getMinutes().toString();
      }

    });

    this.listaInformazioniCollegate.forEach(
      (x) => {
        if (x.dataCessazioneInformazione != null)
          this.count++;
      }
    );

    if (this.listaInformazioniCollegate.length !== this.count)
      this.respAccessoPermanente = false;

  }

  confermaSceltaInsert() {
    if (this.nuovaMotivazioneAccesso || this.nuovaMotivazioneAccesso !== '') {

      this.schedaInformazione.dataAccessoInformazione = this.dataInfo;
      this.schedaInformazione.motivazioneAccesso = this.nuovaMotivazioneAccesso;
      this.schedaInformazione.autoreInformazione = this.user.username;
      this.schedaInformazione.dataCessazioneInformazione = null;
      this.schedaInformazione.oraCessazioneInformazione = null;

      if (this.tipoAccesso === this.global.STATO_PERMANENTE && window.confirm('Desideri delegare l\'accesso permanente a questa persona?')) {
        this.schedaPersona.tipoAccessoInformazione = this.global.STATO_PERMANENTE;
        this.schedaInformazione.stato = this.global.STATO_PERMANENTE;

        this.restAccessoInformazioniService.insertAccessoInformazione(this.schedaPersona.idSchedaPersona.toString(), this.oraInfo, this.schedaPersona.tipoAccessoInformazione, this.schedaInformazione).subscribe(
          () => this.activeModal.close('Accesso Permanente')
        );

      } else if (window.confirm('Desideri collegare l\'informazione privilegiata a questa persona?')) {
        this.schedaPersona.tipoAccessoInformazione = this.global.STATO_LIMITATO;

        this.restAccessoInformazioniService.insertAccessoInformazione(this.schedaPersona.idSchedaPersona.toString(), this.oraInfo, this.schedaPersona.tipoAccessoInformazione, this.schedaInformazione).subscribe(
          () => this.activeModal.close('Informazione aggiunta')
        );
      }
    }
  }

  annullaScelta() {
    this.activeModal.close('Modal chiusa su Annulla');
  }

  onChangeInfo(info: string) {
    this.schedaInformazione.informazionePrivilegiata = info;
  }

  controlField() {
    if ((!this.tipoAccesso ||
      !this.nuovaMotivazioneAccesso ||
      this.dataInfo === null ||
      this.oraInfo === '' ||
      !this.schedaInformazione ||
      this.schedaInformazione.informazionePrivilegiata === undefined ||
      this.schedaInformazione.informazionePrivilegiata === '') ||
      (this.tipoAccesso === this.global.SOLO_ALCUNE_INFORMAZIONI &&
        this.schedaInformazione.informazionePrivilegiata === this.global.ACCESSO_PERMANENTE))
      return true;

    else
      return false;
  }

  filterPermanente(tipo: string) {
    if (tipo === this.global.SOLO_ALCUNE_INFORMAZIONI) {
        let arr: SchedaInformazione[] = this.listaInformazioniCollegabili.filter(data => {
        return data.idSchedaInformazione !== 1;
      })
      this.listaInformazioniCollegabili = arr;
    }
  }

}
