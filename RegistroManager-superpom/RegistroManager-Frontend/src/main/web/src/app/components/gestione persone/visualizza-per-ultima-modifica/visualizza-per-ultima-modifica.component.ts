import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { MatSort, MatTableDataSource } from '@angular/material';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import {SESSION_STORAGE, WebStorageService} from 'angular-webstorage-service';
@Component({
  selector: 'app-visualizza-per-ultima-modifica',
  templateUrl: './visualizza-per-ultima-modifica.component.html',
  styleUrls: ['./visualizza-per-ultima-modifica.component.css']
})
export class VisualizzaPerUltimaModificaComponent implements OnInit {

  listaPersone: SchedaPersona[];
  messaggioErrore: string;
  sceltaAmbito: string;
  pathExportExcel: string;

  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  displayedColumns1: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
  dataSource1: MatTableDataSource<SchedaPersona> = null;
  @ViewChild(MatSort) sort1: MatSort;
  

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService, private restPersonaService: RestPersonaService) {
    this.sceltaAmbito = this.storage.get('ambito');
    this.messaggioErrore = 'Non sono presenti persone nel Database';
    this.listaPersone = [];

    // resp per html
    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErrore = false;
    this.pathExportExcel = '';
  }

  ngOnInit() {
    this.restPersonaService.findAllByModifica(this.sceltaAmbito).subscribe((resp) => {
      this.listaPersone = resp;
      // tslint:disable-next-line:max-line-length
      if (this.listaPersone.length === 0) {
            this.respGif = false;
            this.respMessaggioErrore = true;
      } else {
      this.pathExportExcel = `${this.restPersonaService.contextPath}rest/getElencoPersoneDataUltAgg/${this.sceltaAmbito}`;
      this.respGif = false;
      this.respTabella = true;
      this.dataSource1 = new MatTableDataSource<SchedaPersona>(this.listaPersone);
      this.dataSource1.sort = this.sort1;
    }
    });
  }

}
