import { Component, OnInit, Input, Inject } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { Global } from 'src/app/global';
import { UserWeb } from 'src/app/interfaces/user-web';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  sceltaAmbito: string;
  pathStart: string;
  version: string;

  user: UserWeb

  resp: boolean;
  respRM: boolean;
  respIL: boolean;
  respCreaInformazione: boolean;
  respAggiungiPersona: boolean;
  respAmministrazione: boolean;
  respRuoloGestione: boolean;
  respRuoloConfig: boolean;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restPersonaService: RestPersonaService,
    private global: Global) {

    this.sceltaAmbito = this.storage.get('ambito');
    this.pathStart = `${this.restPersonaService.contextPath}`;

    this.version = '3.8.0';

    this.user = this.storage.get('USER');

    if (this.user.ruolo === this.global.RUOLO_SUPERVISORE) {
      this.respCreaInformazione = false;
      this.respAggiungiPersona = false;
    } else if (this.user.ruolo === this.global.RUOLO_GESTIONE) {
      this.respCreaInformazione = true;
      this.respAggiungiPersona = true;
    }

    if (this.user.ruolo === this.global.RUOLO_GESTIONE || this.user.ruolo === this.global.RUOLO_CONFIG) {
      this.respAmministrazione = true;
    }

    this.user.ruoli.forEach(
      (x) => {
        if (x === this.global.RUOLO_GESTIONE)
          this.respRuoloGestione = true;

        if (x === this.global.RUOLO_CONFIG)
          this.respRuoloConfig = true;
      }
    );

  }

  ngOnInit() {
    if (this.sceltaAmbito === this.global.AMBITO_REGISTRO_MANAGER) {
      this.resp = false;
      this.respRM = true;
      this.respIL = false;
    } else {
      this.resp = true;
      this.respRM = false;
      this.respIL = true;
    }
  }

  setAmbito() {
    this.storage.set('ambito', null);
  }

}
