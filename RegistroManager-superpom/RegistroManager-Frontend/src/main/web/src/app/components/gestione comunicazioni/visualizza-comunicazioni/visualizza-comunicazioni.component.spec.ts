import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaComunicazioniComponent } from './visualizza-comunicazioni.component';

describe('VisualizzaComunicazioniComponent', () => {
  let component: VisualizzaComunicazioniComponent;
  let fixture: ComponentFixture<VisualizzaComunicazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaComunicazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaComunicazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
