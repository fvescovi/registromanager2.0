import { Component, OnInit, ViewChild } from '@angular/core';
import { SchedaInformazione } from 'src/app/classes/scheda-informazione';
import { RestInformazioneService } from 'src/app/services/rest-informazione.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ControlloCampi } from 'src/app/classes/utility/controllo-campi';

@Component({
  selector: 'app-find-all-informazioni',
  templateUrl: './find-all-informazioni.component.html',
  styleUrls: ['./find-all-informazioni.component.css']
})
export class FindAllInformazioniComponent implements OnInit {

  listaInformazioni: SchedaInformazione[];
  messaggioErrore: string;
  controllo: ControlloCampi;

  // resp per visualizzazione html
  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  // variabili mat table material
  displayedColumns1: string[] = ['1', '2', '3', '4', '5', '6', '7','8'];
  dataSource1: MatTableDataSource<SchedaInformazione> = null;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private restInformazioneService: RestInformazioneService) {
    this.messaggioErrore = 'Non sono presenti informazioni nel Database';
    this.listaInformazioni = [];
    this.controllo = new ControlloCampi();

    // resp per html
    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErrore = false;
  }

  ngOnInit() {
    this.restInformazioneService.findAllInformazione().subscribe((resp) => {
      this.listaInformazioni = resp;
      if (this.listaInformazioni.length === 0) {
        this.respGif = false;
        this.respMessaggioErrore = true;
      } else {
        this.respGif = false;
        this.respTabella = true;
        this.dataSource1 = new MatTableDataSource<SchedaInformazione>(this.listaInformazioni);
        this.dataSource1.sort = this.sort;
      }
    });
  }

}
