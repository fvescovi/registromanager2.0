import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-visualizza-per-stato',
  templateUrl: './visualizza-per-stato.component.html',
  styleUrls: ['./visualizza-per-stato.component.css']
})
export class VisualizzaPerStatoComponent implements OnInit {

  messaggioErrore: string;
  sceltaAmbito: string;

  pathExportExcelDefinitive: string;
  pathExportExcelBozze: string;
  pathExportExcelCancellate: string;

  respGif: boolean;
  respTabella: boolean;
  respMessaggioErrore: boolean;

  // variabili mat table material
  displayedColumns1: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  displayedColumns2: string[] = ['1', '2', '3', '4', '5', '6', '7', '8'];
  displayedColumns3: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
  dataSource1: MatTableDataSource<SchedaPersona> = null;
  dataSource2: MatTableDataSource<SchedaPersona> = null;
  dataSource3: MatTableDataSource<SchedaPersona> = null;
  @ViewChild(MatSort) sort1: MatSort;
  @ViewChild(MatSort) sort2: MatSort;
  @ViewChild(MatSort) sort3: MatSort;


  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService, private restPersonaService: RestPersonaService) {
    this.sceltaAmbito = this.storage.get('ambito');
    this.messaggioErrore = 'Non sono presenti persone nel Database';

    // resp per html
    this.respGif = true;
    this.respTabella = false;
    this.respMessaggioErrore = false;

    this.pathExportExcelDefinitive = '';
    this.pathExportExcelBozze = '';
    this.pathExportExcelCancellate = '';
  }

  ngOnInit() {

    this.restPersonaService.findAllByStato(this.sceltaAmbito).subscribe((resp) => {

      if (resp[0].length === 0 && resp[1].length === 0 && resp[2].length === 0) {
        this.respGif = false;
        this.respMessaggioErrore = true;

      } else {
        this.respGif = false;
        this.respTabella = true;

        this.dataSource1 = new MatTableDataSource<SchedaPersona>(resp[0]);
        this.dataSource2 = new MatTableDataSource<SchedaPersona>(resp[1]);
        this.dataSource3 = new MatTableDataSource<SchedaPersona>(resp[2]);

        this.dataSource1.sort ? this.sort1 : null;
        this.dataSource2.sort ? this.sort2 : null;
        this.dataSource3.sort ? this.sort3 : null;

        this.pathExportExcelDefinitive = `${this.restPersonaService.contextPath}rest/getElencoPersoneDefinitive/${this.sceltaAmbito}`;
        this.pathExportExcelBozze = `${this.restPersonaService.contextPath}rest/getElencoPersoneBozze/${this.sceltaAmbito}`;
        this.pathExportExcelCancellate = `${this.restPersonaService.contextPath}rest/getElencoPersoneCancellate/${this.sceltaAmbito}`;
      }
      
    });
  }
}
