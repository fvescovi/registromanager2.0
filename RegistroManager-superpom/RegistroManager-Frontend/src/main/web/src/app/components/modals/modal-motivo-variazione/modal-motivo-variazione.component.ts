import { Component, OnInit, Input, Inject } from '@angular/core';
import { SchedaPersona } from 'src/app/classes/scheda-persona';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserWeb } from 'src/app/interfaces/user-web';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-motivo-variazione',
  templateUrl: './modal-motivo-variazione.component.html',
  styleUrls: ['./modal-motivo-variazione.component.css']
})
export class ModalMotivoVariazioneComponent implements OnInit {

  @Input() schedaPersona: SchedaPersona;

  motivoUltimaVariazione: string;
  confirm: boolean;

  user: UserWeb;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
    private restPersonaService: RestPersonaService,
    private activeModal: NgbActiveModal,
    private router: Router) {

    this.confirm = true;
    this.motivoUltimaVariazione = '';

  }

  ngOnInit() {
    this.schedaPersona.motivoUltimaVariazione = '';
    this.user = this.storage.get('USER');
  }

  saveSchedaPersona() {
    if (window.confirm('Confermi la modifica della scheda persona?')) {
      this.schedaPersona.motivoUltimaVariazione = this.motivoUltimaVariazione;
      this.restPersonaService.insertPersona(this.user.username, this.schedaPersona).subscribe(
        (resp) => {
          resp.dataNascita = new Date(resp.dataNascita);
          this.activeModal.close('Persona modificata');
          this.router.navigate(['/find-all']);
        }
      );
    } else {
      this.confirm = true;
    }
  }

  annullaScelta() {
    this.schedaPersona.motivoUltimaVariazione = '';
    this.activeModal.close('Modal chiusa su Annulla');
  }

}
