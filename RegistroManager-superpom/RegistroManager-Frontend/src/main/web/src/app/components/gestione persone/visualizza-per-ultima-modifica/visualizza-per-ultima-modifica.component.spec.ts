import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaPerUltimaModificaComponent } from './visualizza-per-ultima-modifica.component';

describe('VisualizzaPerUltimaModificaComponent', () => {
  let component: VisualizzaPerUltimaModificaComponent;
  let fixture: ComponentFixture<VisualizzaPerUltimaModificaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaPerUltimaModificaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaPerUltimaModificaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
