import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaInformazioniCollegateComponent } from './visualizza-informazioni-collegate.component';

describe('VisualizzaInformazioniCollegateComponent', () => {
  let component: VisualizzaInformazioniCollegateComponent;
  let fixture: ComponentFixture<VisualizzaInformazioniCollegateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizzaInformazioniCollegateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaInformazioniCollegateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
