import { Component, Inject } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  ambito: string;

  constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService) {
    this.ambito = this.storage.get('ambito');
  }

  ngOnInit() {

  }

}