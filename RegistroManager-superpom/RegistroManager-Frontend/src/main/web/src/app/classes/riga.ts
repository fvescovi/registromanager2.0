export class Riga {
    idSchedaPersona: number;
    info: string;
    stato: string;
    nome: string;
    cognome: string;
    codiceScheda: string;
    tipologiaSoggetto: string;
    societa: string;
    ruolo: string;
    dataAccessoInformazione: string;
    dataCessazioneInformazione: string;
}