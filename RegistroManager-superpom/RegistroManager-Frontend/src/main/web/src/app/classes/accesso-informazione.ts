import { SchedaPersona } from './scheda-persona';
import { SchedaInformazione } from './scheda-informazione';

export class AccessoInformazione {
    id: number;
    dataAccessoInformazione: Date;
    oraAccessoInformazione: string;
    dataCessazioneInformazione: Date;
    oraCessazioneInformazione: string;
    motivazioneAccesso: string;
    storico: string;
    schedaPersona: SchedaPersona;
    schedaInformazione: SchedaInformazione;
}
