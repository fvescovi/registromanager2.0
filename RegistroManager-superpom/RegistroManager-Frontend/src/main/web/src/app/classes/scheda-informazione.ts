import { AccessoInformazione } from "src/app/classes/accesso-informazione";

export class SchedaInformazione {

    idSchedaInformazione: number;
    informazionePrivilegiata: string;
    dataIdentificazione: Date;
    oraIdentificazione: string;
    autoreInformazione: string;
    dataUltimoAggiornamentoInfo: Date;
    dataAccessoInformazione: Date;
    oraAccessoInformazione: string;
    motivazioneAccesso: string;
    dataCessazioneInformazione: Date;
    oraCessazioneInformazione: string;
    stato: string;
    dataSezione: Date;
    listaAccessoInformazione: AccessoInformazione[];

    constructor() {

    }

}
