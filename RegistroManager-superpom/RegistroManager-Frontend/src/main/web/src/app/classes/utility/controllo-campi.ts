import { SchedaPersona } from '../scheda-persona';

export class ControlloCampi {

  constructor() { }

  controllo(listaValori: any) {

    const regex: RegExp = /^([^\s])+((?!\s{2,}).)*$/g;

    for (const v of listaValori) {
      if (v === undefined || v === null || !v.toString().match(regex))
        return true;
    }

    return false;
  }

  public equal(oggettoo: SchedaPersona, oggetto: SchedaPersona): boolean {
    // tslint:disable-next-line:no-unused-expression
    if (oggettoo.idSchedaPersona === oggetto.idSchedaPersona
      && oggettoo.codiceFiscale === oggetto.codiceFiscale
      && oggettoo.nome === oggetto.nome
      && oggettoo.cognome === oggetto.cognome
      && oggettoo.cognomeNascita === oggetto.cognomeNascita
      && oggettoo.telefonoProfessionale === oggetto.telefonoProfessionale
      && oggettoo.telefonoPrivato === oggetto.telefonoPrivato
      && oggettoo.indirizzo === oggetto.indirizzo
      && oggettoo.cap === oggetto.cap
      && oggettoo.localita === oggetto.localita
      && oggettoo.provincia === oggetto.provincia
      && oggettoo.nazione === oggetto.nazione
      && oggettoo.nomeIndirizzoImpresa === oggetto.nomeIndirizzoImpresa
      && oggettoo.identificativoNazionale === oggetto.identificativoNazionale
      && oggettoo.dataNascita === oggetto.dataNascita
      && oggettoo.dataInserimento === oggetto.dataInserimento
      && oggettoo.email === oggetto.email
      && oggettoo.ruolo === oggetto.ruolo
      && oggettoo.ragioneSociale === oggetto.ragioneSociale
      && oggettoo.partitaIva === oggetto.partitaIva
      && oggettoo.tipoAccessoInformazione === oggetto.tipoAccessoInformazione
      && oggettoo.tipologiaSoggetto === oggetto.tipologiaSoggetto
      && oggettoo.managerRiferimento === oggetto.managerRiferimento
      && oggettoo.codiceScheda === oggetto.codiceScheda
      && oggettoo.stato === oggetto.stato
      && oggettoo.motivoUltimaVariazione === oggetto.motivoUltimaVariazione
      && oggettoo.motivoIscrizione === oggetto.motivoIscrizione
      && oggettoo.autore === oggetto.autore
      && oggettoo.dataIscrizione === oggetto.dataIscrizione
      && oggettoo.dataUltimoAggiornamento === oggetto.dataUltimoAggiornamento
      && oggettoo.dataCancellazione === oggetto.dataCancellazione
      && oggettoo.societa.nome === oggetto.societa.nome
      && oggettoo.societa.id === oggetto.societa.id
      && oggettoo.societa.iniziale === oggetto.societa.iniziale) {
      return false;
    } else {
      return true;
    }
  }

}
