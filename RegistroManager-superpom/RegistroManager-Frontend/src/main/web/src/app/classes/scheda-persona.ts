import { Societa } from '../interfaces/societa';
import { AccessoInformazione } from "src/app/classes/accesso-informazione";
import { Comunicazione } from '../interfaces/comunicazione';
import { Versioning } from '../interfaces/versioning';

export class SchedaPersona {

  idSchedaPersona: number;
  codiceFiscale: string;
  nome: string;
  cognome: string;
  cognomeNascita: string;
  telefonoProfessionale: string;
  telefonoPrivato: string;
  indirizzo: string;
  cap: string;
  localita: string;
  provincia: string;
  nazione: string;
  nomeIndirizzoImpresa: string;
  identificativoNazionale: string;
  dataNascita: Date;
  dataInserimento: Date;
  email: string;
  ruolo: string;
  ragioneSociale: string;
  partitaIva: string;
  tipoAccessoInformazione: string;
  tipologiaSoggetto: string;
  managerRiferimento: string;
  codiceScheda: string;
  stato: string;
  motivoUltimaVariazione: string;
  motivoIscrizione: string;
  autore: string;
  dataIscrizione: Date;
  dataUltimoAggiornamento: Date;
  dataCancellazione: Date;
  societa: Societa;
  tipoAmbito: string;
  descrizioneInformazione: string;
  listaAccessoInformazione: AccessoInformazione[];
  listaComunicazione: Comunicazione[];
  listaVersioning: Versioning[];

  constructor() {

  }

}
