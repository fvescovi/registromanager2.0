export class RicercaAccesso {
    informazionePrivilegiata: string;
    startDate: Date;
    endDate: Date;
}