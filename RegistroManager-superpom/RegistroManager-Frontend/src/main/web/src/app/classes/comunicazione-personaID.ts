import { Comunicazione } from '../interfaces/comunicazione';

import { SchedaPersona } from './scheda-persona';

export class ComunicazionePersonaID {
    comunicazione: Comunicazione;
    schedaPersona: SchedaPersona;
}