import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AggiungiPersonaComponent } from './components/gestione persone/aggiungi-persona/aggiungi-persona.component';
import { VisualizzaPerStatoComponent } from './components/gestione persone/visualizza-per-stato/visualizza-per-stato.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaPerUltimaModificaComponent } from './components/gestione persone/visualizza-per-ultima-modifica/visualizza-per-ultima-modifica.component';
import { SocietaComponent } from './components/gestione persone/societa/societa.component';
import { FindAllComponent } from './components/gestione persone/find-all/find-all.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaSchedaPersonaComponent } from './components/gestione persone/visualizza-scheda-persona/visualizza-scheda-persona.component';
import { ErrorComponent } from './components/error/error.component';
import { VisualizzaVersioningComponent } from './components/gestione persone/visualizza-versioning/visualizza-versioning.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaSingolaVersioningComponent } from './components/gestione persone/visualizza-singola-versioning/visualizza-singola-versioning.component';
import { CreazioneInformazioneComponent } from './components/gestione informazioni/creazione-informazione/creazione-informazione.component';
import { FindAllInformazioniComponent } from './components/gestione informazioni/find-all-informazioni/find-all-informazioni.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaSchedaInformazioniComponent } from './components/gestione informazioni/visualizza-scheda-informazioni/visualizza-scheda-informazioni.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaInformazioniCollegateComponent } from './components/gestione informazioni/visualizza-informazioni-collegate/visualizza-informazioni-collegate.component';
import { SelezioneAmbitoComponent } from './components/selezione-ambito/selezione-ambito.component';
import { RicercaComponent } from './components/ricerca/ricerca.component';
import { RegistroComponent } from './components/registro/registro.component';
import { AmministrazioneComponent } from './components/gestione comunicazioni/amministrazione/amministrazione.component';
import { CreaTemplateComponent } from './components/gestione comunicazioni/crea-template/crea-template.component';
import { VisualizzaComunicazioniComponent } from './components/gestione comunicazioni/visualizza-comunicazioni/visualizza-comunicazioni.component';
import { VisualizzaTemplateComponent } from './components/gestione comunicazioni/visualizza-template/visualizza-template.component';
import { VisualizzaComunicazioneComponent } from './components/gestione comunicazioni/visualizza-comunicazione/visualizza-comunicazione.component';
import { RicercaPerDataComponent } from './components/ricerca-per-data/ricerca-per-data.component';
import { VisualizzaInfoByNomeComponent } from './components/gestione informazioni/visualizza-info-by-nome/visualizza-info-by-nome.component';

const routes: Routes = [
  { path: '', redirectTo: '/find-all', pathMatch: 'full' },
  { path: 'aggiungi-persona', component: AggiungiPersonaComponent },
  { path: 'visualizza-per-stato', component: VisualizzaPerStatoComponent },
  { path: 'visualizza-per-ultima-modifica', component: VisualizzaPerUltimaModificaComponent },
  { path: 'societa', component: SocietaComponent },
  { path: 'find-all', component: FindAllComponent },
  { path: 'visualizza-scheda-persona/:idSchedaPersona', component: VisualizzaSchedaPersonaComponent },
  { path: 'error/:error', component: ErrorComponent },
  { path: 'visualizza-versioning/:idSchedaPersona', component: VisualizzaVersioningComponent },
  { path: 'visualizza-singola-versioning/:idVersioning', component: VisualizzaSingolaVersioningComponent },
  { path: 'creazione-informazione', component: CreazioneInformazioneComponent },
  { path: 'find-all-informazioni', component: FindAllInformazioniComponent },
  { path: 'visualizza-scheda-informazioni/:idSchedaInformazione', component: VisualizzaSchedaInformazioniComponent },
  { path: 'visualizza-informazioni-collegate/:idSchedaPersona', component: VisualizzaInformazioniCollegateComponent },
  { path: 'selezione-ambito', component: SelezioneAmbitoComponent },
  { path: 'ricerca', component: RicercaComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'amministrazione', component: AmministrazioneComponent },
  { path: 'crea-template', component: CreaTemplateComponent },
  { path: 'visualizza-comunicazioni', component: VisualizzaComunicazioniComponent },
  { path: 'visualizza-template/:id', component: VisualizzaTemplateComponent },
  { path: 'visualizza-comunicazione/:id', component: VisualizzaComunicazioneComponent },
  { path: 'ricerca-per-data', component: RicercaPerDataComponent },
  { path: 'visualizza-info-by-nome/:info', component: VisualizzaInfoByNomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
