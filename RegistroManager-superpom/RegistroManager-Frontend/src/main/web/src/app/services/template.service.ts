import { Injectable, Inject } from '@angular/core';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';
import { Template } from 'src/app/interfaces/template';
import { SchedaPersona } from '../classes/scheda-persona';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  contextPath: string;

  // tslint:disable-next-line: deprecation
  constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
    this.contextPath = http.contextPath;
  }

  findById(id: string) {
    return this.http.doGet(`rest/findTemplateById/${id}`);
  }

  addTemplate(t: Template) {
    return this.http.doPost(`rest/addTemplate`, t);
  }

  caricaListaTemplate() {
    return this.http.doGet(`rest/caricaListaTemplate`);
  }

  getListaTemplate(schedaPersona: SchedaPersona) {
    return this.http.doPost(`rest/getTemplates`, schedaPersona);
  }
  
}
