import { Injectable, Inject } from '@angular/core';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ExportcsvService {

  contextPath: string;

  // tslint:disable-next-line: deprecation
  constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
    this.contextPath = http.contextPath;
  }

  exportSchedaPersonaCsv() {
    return this.http.doPost(`rest/importSchedaPersona`, null);
  }

  exportMailCsv() {
    return this.http.doPost(`rest/importMail`, null);
  }

  exportLetteraCsv() {
    return this.http.doPost(`rest/importLettera`, null);
  }

  exportAccessoInformazioneCsv() {
    return this.http.doPost(`rest/importAccessoInformazione`, null);
  }

}