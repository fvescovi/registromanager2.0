import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';
import { SchedaPersona } from '../classes/scheda-persona';
import { RicercaPersona } from '../classes/ricerca-persona';
import { Template } from '../interfaces/template';
import { RicercaAccesso } from '../classes/ricerca-accesso';

@Injectable({
  providedIn: 'root'
})

export class RestPersonaService {

  contextPath: string;

  // tslint:disable-next-line: deprecation
  constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
    this.contextPath = http.contextPath;
  }

  insertPersona(username: string, schedaPersona: SchedaPersona) {
    return this.http.doPost(`rest/saveSchedaPersona/${username}`, schedaPersona);
  }

  findAllPersona(sceltaAmbito: string) {
    return this.http.doGet(`rest/caricaListaSchedaPersona/${sceltaAmbito}`);
  }

  findAllPersonaCognome(sceltaAmbito: string) {
    return this.http.doGet(`rest/caricaListaSchedaPersonaCognome/${sceltaAmbito}`);
  }

  findBySocieta(societa: string, sceltaAmbito: string) {
    return this.http.doGet(`rest/caricaListaSchedaPersonaSocieta/${societa}/${sceltaAmbito}`);
  }

  findAllByStato(sceltaAmbito: string) {
    return this.http.doGet(`rest/caricaListaSchedaPersonaStato/${sceltaAmbito}`);
  }

  findById(id: string) {
    return this.http.doGet(`rest/findSchedaPersonaById/${id}`);
  }

  findPersonaByCampi(sceltaAmbito: string, ricercaPersona: RicercaPersona) {
    return this.http.doGet(`rest/ricercaPersona/${sceltaAmbito}/${JSON.stringify(ricercaPersona)}`);
  }

  deleteSchedaPersona(id: string, autore: string) {
   return this.http.doDelete(`rest/deleteSchedaPersonaById/${id}/${autore}`);
  }

  findAllByModifica(sceltaAmbito: string) {
    return this.http.doGet(`rest/caricaListaSchedaPersonaDataUltimoAggiornamento/${sceltaAmbito}`);
  }

  findVersioningByPersona(id: number) {
    return this.http.doGet(`rest/findVersioniByIdSchedaPersona/${id}`);
  }

  findVersioningById(id: string) {
    return this.http.doGet(`rest/findVersioningById/${id}`);
  }

  findPersonaByIdInformazione(id: string, pageIndex: number, pageSize: number) {
    return this.http.doGet(`rest/getPaginator/${id}/${pageIndex}/${pageSize}`);
  }

  stampaPersona(schedaPersona: SchedaPersona): Observable<Blob> {
    return this.http.doPostFileDownload(`rest/stampaSchedaPersona/`, schedaPersona);
  }

  findListaPersoneInfoLimitate() {
    return this.http.doGet(`rest/getListPersonaInfoLim`);
  }

  findListaPersoneInfoPermanenti() {
    return this.http.doGet(`rest/getListPersonaInfoPerm`);
  }

}