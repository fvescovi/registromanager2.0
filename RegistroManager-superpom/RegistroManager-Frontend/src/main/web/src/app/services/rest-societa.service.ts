import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';
import { Societa } from '../interfaces/societa';

@Injectable({
  providedIn: 'root'
})
export class RestSocietaService {

  // tslint:disable-next-line: deprecation
  constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {

  }

  getListaSocieta() {
    return this.http.doGet(`rest/caricaListaSocieta`);
  }

  insertSocieta(societa: Societa) {
    return this.http.doPost(`rest/saveSocieta/`, societa);
  }

}
