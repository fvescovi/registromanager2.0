import { TestBed } from '@angular/core/testing';

import { AccessoInformazioniService } from './accesso-informazioni.service';

describe('AccessoInformazioniService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccessoInformazioniService = TestBed.get(AccessoInformazioniService);
    expect(service).toBeTruthy();
  });
});
