import { TestBed } from '@angular/core/testing';

import { RestInformazioneService } from './rest-informazione.service';

describe('RestInformazioneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestInformazioneService = TestBed.get(RestInformazioneService);
    expect(service).toBeTruthy();
  });
});
