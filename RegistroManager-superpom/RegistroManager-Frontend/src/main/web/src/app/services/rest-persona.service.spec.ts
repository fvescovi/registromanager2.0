import { TestBed } from '@angular/core/testing';

import { RestPersonaService } from './rest-persona.service';

describe('RestPersonaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestPersonaService = TestBed.get(RestPersonaService);
    expect(service).toBeTruthy();
  });
});
