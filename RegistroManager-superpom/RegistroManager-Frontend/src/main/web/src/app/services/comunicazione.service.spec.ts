import { TestBed } from '@angular/core/testing';

import { ComunicazioneService } from './comunicazione.service';

describe('ComunicazioneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComunicazioneService = TestBed.get(ComunicazioneService);
    expect(service).toBeTruthy();
  });
});
