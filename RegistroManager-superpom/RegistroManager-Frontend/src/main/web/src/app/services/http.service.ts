import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/platform-browser';
import { RegistroEntity } from "src/app/interfaces/registro-entity";

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  contextPath: string;

// tslint:disable-next-line: deprecation
  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
// tslint:disable-next-line:max-line-length
   this.contextPath = document.location.protocol + '//' + document.location.hostname + ':' + document.location.port + '/registromanager/';
//console.log("CONTEXT PATH: "+this.contextPath);
   // this.contextPath = 'http://10.20.36.45:8080/RegistroManager/';
  }

   doGet( url: string): Observable<any> {
    return this.http.get<any>(this.contextPath + url);
   }

   doPost(url: string, body: any): Observable <any> {
    const httpOptions = {
    headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
    'Access-Control-Allow-Headers': 'content-type',
    'Access-Control-Allow-Origin': '*',
    Allow: ' GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH',
    Cookie : '',
    'X-XSRF-TOKEN': ''
      })
    };

    return this.http.post<any>(this.contextPath + url, body, httpOptions);

  }
   
   doPostFileDownload(url: string, body: any): Observable<Blob> {
       const headers = new HttpHeaders({
         'Content-Type': 'application/json; charset=utf-8',
         'Access-Control-Allow-Headers': 'content-type',
         'Access-Control-Allow-Origin': '*',
         Allow: ' GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH',
         Cookie: '',
         'X-XSRF-TOKEN': ''
       });

       return this.http.post(this.contextPath + url, body, { headers: headers, responseType: 'blob' });
     } 
   
   doGetUser(): Observable<RegistroEntity> {
       return this.http.get<RegistroEntity>(this.contextPath + 'rest/utente/username');
      }

  doDelete(url: string): Observable <any> {
    const httpHoptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.delete<any>(this.contextPath + url, httpHoptions);
  }

}
