import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';
import { AccessoInformazione } from '../classes/accesso-informazione';
import { SchedaInformazione } from '../classes/scheda-informazione';
import { SchedaPersona } from '../classes/scheda-persona';

@Injectable({
  providedIn: 'root'
})
export class AccessoInformazioniService {

  contextPath: string;

  constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
    this.contextPath = http.contextPath;
  }

  findInformazioniByIdPersona(id: string) {
    return this.http.doGet(`rest/getAccessoInformazioneById/${id}`);
  }

  insertAccessoInformazione(id: string, ora:string,tipoAccessoInformazione:string, schedaInformazione: SchedaInformazione) {
    return this.http.doPost(`rest/associaInformazionePersona/${id}/${ora}/${tipoAccessoInformazione}`, schedaInformazione);
  }

  cessaAccessoInformazione(id: number, accessoInformazione: AccessoInformazione) {
    return this.http.doPost(`rest/cessaInformazionePersona/${id}`, accessoInformazione);
  }

}
