import { Injectable, Inject } from '@angular/core';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';
import { Template } from '../interfaces/template';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComunicazioneService {

  contextPath: string;

  // tslint:disable-next-line: deprecation
  constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
    this.contextPath = http.contextPath;
  }

  caricaListaComunicazione(ambito: string) {
    return this.http.doGet(`rest/caricaListaComunicazione/${ambito}`);
  }

  findById(id: string) {
    return this.http.doGet(`rest/findComunicazioneById/${id}`)
  }

  inviaEmail(id: number, oggetto: string, ambito: string, t: Template) {
    return this.http.doPost(`rest/inviaEmail/${id}/${oggetto}/${ambito}`, t);
  }

  stampaLettera(id: number, ambito: string, t: Template): Observable<Blob> {
    return this.http.doPostFileDownload(`rest/stampaLettera/${id}/${ambito}`, t);
  }

}
