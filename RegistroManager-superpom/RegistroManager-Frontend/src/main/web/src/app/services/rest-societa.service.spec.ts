import { TestBed } from '@angular/core/testing';

import { RestSocietaService } from './rest-societa.service';

describe('RestSocietaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestSocietaService = TestBed.get(RestSocietaService);
    expect(service).toBeTruthy();
  });
});
