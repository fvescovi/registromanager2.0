import { Injectable, Inject  } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpServiceService } from './http.service';
import { DOCUMENT } from '@angular/platform-browser';
import { SchedaInformazione } from '../classes/scheda-informazione';
import { UserWeb } from '../interfaces/user-web';



@Injectable({
    providedIn: 'root'
})

export class RestInformazioneService {

  contextPath: string;

// tslint:disable-next-line: deprecation
    constructor(private http: HttpServiceService, @Inject(DOCUMENT) private document) {
    this.contextPath = http.contextPath;
    }

    insertInformazione(schedaInformazione: SchedaInformazione) {
      return this.http.doPost(`rest/saveSchedaInformazione/`, schedaInformazione);
    }

    findAccessListByIdInfo(id: string) {
      return this.http.doGet(`rest/findAccessListByIdInfo/${id}`);
    }

    findAccessListByRicercaAccesso(ricercaAccesso: string) {
      return this.http.doGet(`rest/findAccessListByRicercaAccesso/${ricercaAccesso}`);
    }

    findAllInformazione() {
      return this.http.doGet(`rest/caricaListaInformazioni`);
    }

    findAllByStato() {
      return this.http.doGet(`rest/caricaListaInformazioniStato`);
    }

    findById(id: string) {
      return this.http.doGet(`rest/findSchedaInformazioneById/${id}`);
    }

    findInfoByNome(info: string) {
      return this.http.doGet(`rest/findInfoByNome/${info}`);
    }

    deleteSchedaInformazione(id: string, username: string) {
      this.http.doDelete(`rest/deleteSchedaInformazioneById/${id}/${username}`).subscribe(resp => console.log(), error => console.log()
      );
    }

    findInformazioniDisponibiliByIdPersona(id: string) {
      return this.http.doGet(`rest/getInformazioniDisponibiliByIdPersona/${id}`);
    }

    findInformazioniByIdPersona(id: string) {
      return this.http.doGet(`rest/getInformazioniAssociateByIdPersona/${id}`);
    }

    findAccessoPermanente() {
      return this.http.doGet(`rest/getAccessoPermanente`);
    }

}

