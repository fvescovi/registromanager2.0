export interface Versioning {

 idVersioning: number;
 versione: number;
 datiModificati: string;
 dataCreazione: Date;
 schedaPersona: number;
 schedaJson: string;

}
