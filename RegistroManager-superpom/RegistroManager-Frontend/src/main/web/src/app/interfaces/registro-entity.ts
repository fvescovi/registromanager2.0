export interface RegistroEntity {
  body: any;
  headers: any;
  messageType: string;
  messageDescription: string;
  status: string;
}
