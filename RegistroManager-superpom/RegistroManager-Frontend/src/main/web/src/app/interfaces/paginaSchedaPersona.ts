import { SchedaPersona } from '../classes/scheda-persona';

export interface PaginaSchedaPersona {
    listaSchedaPersona: SchedaPersona[];
    totalePagine: number;
    paginaCorrente: number;
    totaleRecord: number;
    recordPerPagina: number;
}