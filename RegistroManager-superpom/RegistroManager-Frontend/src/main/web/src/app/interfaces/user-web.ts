export interface UserWeb {
    username: string;
    ruolo: string;
    ruoli: string[];
    sessionID: string;
    lastAccessedTime: string;
    cookie: string;
}
