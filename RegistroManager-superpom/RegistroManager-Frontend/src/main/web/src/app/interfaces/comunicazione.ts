import { SchedaPersona } from '../classes/scheda-persona';
import { Template } from './template';

export class Comunicazione {
    id: number;
    dataInvio: Date;
    tipo: string;
    testo: string;
    oggetto: string;
    mittente: string;
    tipoAmbito: string;
    schedaPersona: SchedaPersona;
    template: Template;
}
