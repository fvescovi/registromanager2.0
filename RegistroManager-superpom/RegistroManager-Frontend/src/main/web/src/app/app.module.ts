import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule } from 'angular-webstorage-service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AggiungiPersonaComponent } from './components/gestione persone/aggiungi-persona/aggiungi-persona.component';
import { VisualizzaPerStatoComponent } from './components/gestione persone/visualizza-per-stato/visualizza-per-stato.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaPerUltimaModificaComponent } from './components/gestione persone/visualizza-per-ultima-modifica/visualizza-per-ultima-modifica.component';
import { RestPersonaService } from 'src/app/services/rest-persona.service';
import { RestSocietaService } from 'src/app/services/rest-societa.service';
import { SocietaComponent } from './components/gestione persone/societa/societa.component';
import { FindAllComponent } from './components/gestione persone/find-all/find-all.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
// tslint:disable-next-line:max-line-length
import { VisualizzaSchedaPersonaComponent } from './components/gestione persone/visualizza-scheda-persona/visualizza-scheda-persona.component';
import { ErrorComponent } from './components/error/error.component';
import { HttpErrorInterceptor } from './interceptor/http-error-interceptor';
import { VisualizzaVersioningComponent } from './components/gestione persone/visualizza-versioning/visualizza-versioning.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaSingolaVersioningComponent } from './components/gestione persone/visualizza-singola-versioning/visualizza-singola-versioning.component';
import { CreazioneInformazioneComponent } from './components/gestione informazioni/creazione-informazione/creazione-informazione.component';
import { FindAllInformazioniComponent } from './components/gestione informazioni/find-all-informazioni/find-all-informazioni.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaSchedaInformazioniComponent } from './components/gestione informazioni/visualizza-scheda-informazioni/visualizza-scheda-informazioni.component';
// tslint:disable-next-line:max-line-length
import { VisualizzaInformazioniCollegateComponent } from './components/gestione informazioni/visualizza-informazioni-collegate/visualizza-informazioni-collegate.component';
import { SelezioneAmbitoComponent } from './components/selezione-ambito/selezione-ambito.component';
import { AppStartComponent } from './components/app-start/app-start.component';
import { RicercaComponent } from './components/ricerca/ricerca.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ModalSocietaComponent } from './components/modals/modal-societa/modal-societa.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalAggiungiInformazioniComponent } from './components/modals/modal-aggiungi-informazioni/modal-aggiungi-informazioni.component';
import { ModalCessaInformazioniComponent } from './components/modals/modal-cessa-informazioni/modal-cessa-informazioni.component';
import { ModalMotivoVariazioneComponent } from './components/modals/modal-motivo-variazione/modal-motivo-variazione.component';
import { ModalModifcaInformazioneComponent } from './components/modals/modal-modifca-informazione/modal-modifca-informazione.component';
import { ModalMailComponent } from './components/modals/modal-mail/modal-mail.component';
import { DatePipe } from '@angular/common';
import { Global } from './global';
import { AmministrazioneComponent } from './components/gestione comunicazioni/amministrazione/amministrazione.component';
import { CreaTemplateComponent } from './components/gestione comunicazioni/crea-template/crea-template.component';
import { VisualizzaComunicazioniComponent } from './components/gestione comunicazioni/visualizza-comunicazioni/visualizza-comunicazioni.component';
import { VisualizzaTemplateComponent } from './components/gestione comunicazioni/visualizza-template/visualizza-template.component';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatDatepickerModule, MatCardModule, MatNativeDateModule } from '@angular/material';
import { MomentDateAdapter, MatMomentDateModule } from '@angular/material-moment-adapter';
import { ReactiveFormsModule } from '@angular/forms';
import * as _moment from 'moment';
import { ModalDataCancellazioneComponent } from './components/modals/modal-data-cancellazione/modal-data-cancellazione.component';
import { VisualizzaComunicazioneComponent } from './components/gestione comunicazioni/visualizza-comunicazione/visualizza-comunicazione.component';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';
import { RicercaPerDataComponent } from './components/ricerca-per-data/ricerca-per-data.component';
import { VisualizzaInfoByNomeComponent } from './components/gestione informazioni/visualizza-info-by-nome/visualizza-info-by-nome.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AggiungiPersonaComponent,
    VisualizzaPerStatoComponent,
    VisualizzaPerUltimaModificaComponent,
    SocietaComponent,
    FindAllComponent,
    VisualizzaSchedaPersonaComponent,
    ErrorComponent,
    VisualizzaVersioningComponent,
    VisualizzaSingolaVersioningComponent,
    CreazioneInformazioneComponent,
    FindAllInformazioniComponent,
    VisualizzaSchedaInformazioniComponent,
    VisualizzaInformazioniCollegateComponent,
    SelezioneAmbitoComponent,
    AppStartComponent,
    RicercaComponent,
    RegistroComponent,
    ModalSocietaComponent,
    ModalCessaInformazioniComponent,
    ModalAggiungiInformazioniComponent,
    ModalMotivoVariazioneComponent,
    ModalModifcaInformazioneComponent,
    ModalMailComponent,
    AmministrazioneComponent,
    CreaTemplateComponent,
    VisualizzaComunicazioniComponent,
    VisualizzaTemplateComponent,
    ModalDataCancellazioneComponent,
    VisualizzaComunicazioneComponent,
    RicercaPerDataComponent,
    VisualizzaInfoByNomeComponent
  ],
  imports: [
    BrowserModule,
    StorageServiceModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    NgbModule,
    MatDatepickerModule,
    MatCardModule,
    MatNativeDateModule,
    MatMomentDateModule,
    ReactiveFormsModule,
    AngularStickyThingsModule
  ],
  providers: [
    RestPersonaService,
    NgbActiveModal,
    DatePipe,
    Global,
    RestSocietaService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'it' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalSocietaComponent, ModalCessaInformazioniComponent, ModalAggiungiInformazioniComponent, ModalMotivoVariazioneComponent, ModalModifcaInformazioneComponent, ModalMailComponent, ModalDataCancellazioneComponent],
  exports: [ModalSocietaComponent, ModalCessaInformazioniComponent, ModalAggiungiInformazioniComponent, ModalMotivoVariazioneComponent, ModalModifcaInformazioneComponent, ModalMailComponent, ModalDataCancellazioneComponent]
})
export class AppModule { }
