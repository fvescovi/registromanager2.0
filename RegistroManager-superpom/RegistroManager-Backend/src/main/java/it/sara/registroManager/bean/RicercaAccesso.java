package it.sara.registroManager.bean;

import java.util.Date;

public class RicercaAccesso {

	private String informazionePrivilegiata;
	private Date startDate;
	private Date endDate;

	public String getInformazionePrivilegiata() {
		return informazionePrivilegiata;
	}

	public void setInformazionePrivilegiata(String informazionePrivilegiata) {
		this.informazionePrivilegiata = informazionePrivilegiata;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
