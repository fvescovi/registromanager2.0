package it.sara.registroManager.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class ComunicazionePersonaID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4272301266756501476L;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_COMUNICAZIONE")
	private Comunicazione comunicazione;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SCHEDA_PERSONA")
	private SchedaPersona schedaPersona;

	public Comunicazione getComunicazione() {
		return comunicazione;
	}

	public void setComunicazione(Comunicazione comunicazione) {
		this.comunicazione = comunicazione;
	}

	public SchedaPersona getSchedaPersona() {
		return schedaPersona;
	}

	public void setSchedaPersona(SchedaPersona schedaPersona) {
		this.schedaPersona = schedaPersona;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comunicazione == null) ? 0 : comunicazione.hashCode());
		result = prime * result + ((schedaPersona == null) ? 0 : schedaPersona.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ComunicazionePersonaID))
			return false;
		ComunicazionePersonaID other = (ComunicazionePersonaID) obj;
		if (comunicazione == null) {
			if (other.comunicazione != null)
				return false;
		} else if (!comunicazione.equals(other.comunicazione))
			return false;
		if (schedaPersona == null) {
			if (other.schedaPersona != null)
				return false;
		} else if (!schedaPersona.equals(other.schedaPersona))
			return false;
		return true;
	}

}
