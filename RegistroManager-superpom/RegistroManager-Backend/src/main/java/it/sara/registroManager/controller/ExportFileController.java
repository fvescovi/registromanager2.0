package it.sara.registroManager.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.registroManager.bean.GenericMapper;
import it.sara.registroManager.bean.RicercaAccesso;
import it.sara.registroManager.bean.RicercaPersona;
import it.sara.registroManager.bean.Riga;
import it.sara.registroManager.dao.AccessoInformazioneDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.export.ExportInfoLimXls;
import it.sara.registroManager.export.ExportInfoPermXls;
import it.sara.registroManager.export.ExportSchedePersonaCsv;
import it.sara.registroManager.utility.RicercaManager;
import it.sara.registroManager.utility.SchedaPersonaCrudManager;
import it.sara.registroManager.utility.Utility;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class ExportFileController {

	@SuppressWarnings("rawtypes")
	@Autowired
	GenericMapper genericMapper;

	@Autowired
	RicercaManager ricercaManager;

	@Autowired
	EntityManager entityManager;

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	ExportInfoLimXls exportInfoLimXls;

	@Autowired
	ExportInfoPermXls exportInfoPermXls;

	@Autowired
	AccessoInformazioneDao accessoInformazioneDao;

	@Autowired
	ExportSchedePersonaCsv exportSchedePersonaCsv;

	@Autowired
	SchedaPersonaCrudManager schedaPersonaCrudManager;

	@GetMapping(value = "/getExportElencoSchedePersona/{ambito}")
	public void getExportElencoSchedePersona(@PathVariable(name = "ambito") String ambito, HttpServletRequest request,
			HttpServletResponse response) throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		if (ambito.equals(Utility.AMBITO_INSIDER_LIST))
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaIL();

		else
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaRM();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"ElencoPersone.xlsx\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@GetMapping(value = "/getElencoPersoneDefinitive/{ambito}")
	public void getElencoPersoneDefinitive(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(name = "ambito") String ambito)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		if (ambito.equals(Utility.AMBITO_INSIDER_LIST))
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaDefinitivaIL();

		else
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaDefinitivaRM();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"PersoneDefinitive.xls\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@GetMapping(value = "/getElencoPersoneBozze/{ambito}")
	public void getElencoPersoneBozze(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(name = "ambito") String ambito)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		if (ambito.equals(Utility.AMBITO_INSIDER_LIST))
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaBozzaIL();

		else
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaBozzaRM();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"PersoneBozze.xls\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@GetMapping(value = "/getElencoPersoneCancellate/{ambito}")
	public void getElencoPersoneCancellate(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(name = "ambito") String ambito)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		if (ambito.equals(Utility.AMBITO_INSIDER_LIST))
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaCancellataIL();

		else
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaCancellataRM();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"PersoneCancellate.xls\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@GetMapping(value = "/getElencoPersoneDataUltAgg/{ambito}")
	public void getElencoPersoneDataUltAgg(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(name = "ambito") String ambito)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		if (ambito.equals(Utility.AMBITO_INSIDER_LIST))
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaByDataUltAggIL();

		else
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaByDataUltAggRM();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"PersonePerDataUltimaModifca.xls\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@GetMapping(value = "/getElencoPersoneSocieta/{nomeSocieta}/{ambito}")
	public void getElencoPersoneSocieta(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(name = "ambito") String ambito, @PathVariable(name = "nomeSocieta") String nomeSocieta)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findSchedaPersonaBySocieta(nomeSocieta, ambito);

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + nomeSocieta + ".xls\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/getElencoPersoneRicerca/{ambito}/{ricercaPersona}")
	public void getElencoPersoneRicerca(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(name = "ambito") String ambito, @PathVariable(name = "ricercaPersona") String ricercaPersona)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		RicercaPersona rp = new RicercaPersona();

		rp = (RicercaPersona) genericMapper.jsonToJava(ricercaPersona, rp);

		String sql = ricercaManager.getQueryRicerca(ambito, rp);

		Query query = entityManager.createQuery(sql);

		List<SchedaPersona> listaSchedaPersona = query.getResultList();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"RicercaPersona.xls\"");

		exportSchedePersonaCsv.creazioneFoglioCsv(listaSchedaPersona, response.getOutputStream(), ambito);

	}

	@GetMapping(value = "/getExportPersonaInfoLim")
	public void getExportPersonaInfoLim(HttpServletResponse response,
			@RequestParam(value = "arrayInfo") String[] arrayInfo)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		Set<SchedaPersona> listaSchedaPersona = new HashSet<SchedaPersona>();
		Set<AccessoInformazione> personSet = new HashSet<AccessoInformazione>();

		for (String s : arrayInfo) {

			personSet = accessoInformazioneDao.findAccessoInfoByNomeInfoLim(s);

			if (personSet.size() > 0) {

				for (AccessoInformazione a : personSet) {
					listaSchedaPersona.add(a.getSchedaPersona());
				}

				personSet = new HashSet<AccessoInformazione>();
			}

		}

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"RegistroAccessiSingoli.xls\"");

		exportInfoLimXls.creazioneFoglioXls(listaSchedaPersona, arrayInfo, response.getOutputStream());

	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@GetMapping(value = "/getElencoPersonaBetweenDate/{ricercaAccesso}")
	public void getElencoPersonaBetweenDate(HttpServletResponse response,
			@PathVariable(value = "ricercaAccesso") String ricercaAccesso,
			@RequestParam(value = "arrayInfo") String[] arrayInfo) {

		RicercaAccesso ra = new RicercaAccesso();
		List<SchedaPersona> listaSchedaPersona = new LinkedList<SchedaPersona>();
		Set<AccessoInformazione> accessList = new HashSet<AccessoInformazione>();
		Set<SchedaPersona> accessSet = new HashSet<SchedaPersona>();

		ra = (RicercaAccesso) genericMapper.jsonToJava(ricercaAccesso, ra);

		ra.getStartDate().setHours(0);
		ra.getStartDate().setMinutes(0);

		ra.getEndDate().setHours(23);
		ra.getEndDate().setMinutes(59);

		try {

			String startDate = Utility.formatDateRicercaAccesso(ra.getStartDate());
			String endDate = Utility.formatDateRicercaAccesso(ra.getEndDate());

			accessList = accessoInformazioneDao.getAccessBetweenDate(ra.getInformazionePrivilegiata(), startDate,
					endDate);

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=\"AccessiPerData.xls\"");

			if (ra.getInformazionePrivilegiata().equals(Utility.ACCESSO_PERMANENTE)) {

				for (AccessoInformazione a : accessList)
					listaSchedaPersona.add(a.getSchedaPersona());

				exportInfoPermXls.creazioneFoglioXlsPerm(listaSchedaPersona, response.getOutputStream());

			}

			else {

				for (AccessoInformazione a : accessList)
					accessSet.add(a.getSchedaPersona());

				exportInfoLimXls.creazioneFoglioXls(accessSet, arrayInfo, response.getOutputStream());

			}

		} catch (SQLException | ParseException | NullPointerException | IOException e) {
			e.printStackTrace();
		}

	}

	@GetMapping(value = "/getExportPersonaInfoPerm")
	public void getExportPersonaInfoPerm(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<SchedaPersona> listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaAccInfPerm();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"RegistroAccessiPermanenti.xls\"");

		exportInfoPermXls.creazioneFoglioXlsPerm(listaSchedaPersona, response.getOutputStream());

	}

	@GetMapping(value = "/getListPersonaInfoLim")
	public List<Riga> getListPersonaInfoLim() throws SQLException, ParseException, InvalidFormatException, IOException {

		List<Riga> righe = schedaPersonaCrudManager.getRigheRegistroLim();

		return righe;

	}

	@GetMapping(value = "/getListPersonaInfoPerm")
	public List<Riga> getListPersonaInfoPerm()
			throws SQLException, ParseException, InvalidFormatException, IOException {

		List<Riga> righe = schedaPersonaCrudManager.getRigheRegistroPerm();

		return righe;

	}

}
