package it.sara.registroManager.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.registroManager.entity.SchedaInformazione;

public interface SchedaInformazioneDao extends CrudRepository<SchedaInformazione, Integer> {

	@Query("select si from SchedaInformazione si where si.informazionePrivilegiata=:nome")
	SchedaInformazione findInfoByNome(@Param(value = "nome") String nome) throws SQLException, NullPointerException;

	@Query("select max(si.idSchedaInformazione) from SchedaInformazione si")
	Integer findMaxIdSchedaInfo() throws SQLException, NullPointerException;

	@Query("select si from SchedaInformazione si where si.informazionePrivilegiata='Accesso Permanente'")
	SchedaInformazione findAccessoPermanente() throws SQLException, NullPointerException;

	@Query("select distinct(ai.schedaInformazione) from AccessoInformazione ai")
	List<SchedaInformazione> findListaSchedaInformazione() throws SQLException, NullPointerException;
	
	@Query("select distinct(ai.schedaInformazione) from AccessoInformazione ai where ai.schedaInformazione.idSchedaInformazione > 1")
	List<SchedaInformazione> findInfoListLim() throws SQLException, NullPointerException;

	@Query("select si from SchedaInformazione si where si.stato<>'Cessata'")
	List<SchedaInformazione> loadEmptyAccessList() throws SQLException, NullPointerException;

	@Query("select si from SchedaInformazione si where si.stato<>'Cessata' and si.idSchedaInformazione not in (select ai.schedaInformazione from AccessoInformazione ai where ai.dataAccessoInformazione is not null and ai.dataCessazioneInformazione is null and ai.storico='N' and ai.schedaPersona.idSchedaPersona=:id)")
	List<SchedaInformazione> findInfoCollegabili(@Param(value = "id") int id) throws SQLException, NullPointerException;
}
