package it.sara.registroManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTATORE_SCHEDA")
public class ContatoreScheda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3771645399677139076L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_SCHEDA_PERSONA")
	@SequenceGenerator(name = "SEQUENCE_SCHEDA_PERSONA", sequenceName = "SEQ_CONTATORE_SCHEDA", allocationSize = 1)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "CONT_I")
	private Integer contI;

	@Column(name = "CONT_R")
	private Integer contR;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getContI() {
		return contI;
	}

	public void setContI(Integer contI) {
		this.contI = contI;
	}

	public Integer getContR() {
		return contR;
	}

	public void setContR(Integer contR) {
		this.contR = contR;
	}

}
