/**
 * 
 */
package it.sara.registroManager;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author f.vescovi
 *
 */

@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
public class DataBaseConfig {
	
	
	
	@Value("${urlJndi}")
	String ulrJndi;
	
	

	@Bean(name = "dataSource")
	public DataSource dataSource() throws NamingException {
		
		
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		ds.setUrl("jdbc:oracle:thin:@//192.168.56.101:1521/orcl.oracle.com");
		ds.setUsername("registro");
		ds.setPassword("registro");
		
        return ds;
		
	 //   return (DataSource) new JndiTemplate().lookup(ulrJndi);
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean  entityManagerFactory() throws NamingException {
	
	    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	    //vendorAdapter.setGenerateDdl(true);
	    vendorAdapter.setDatabasePlatform("org.hibernate.dialect.Oracle10gDialect");
	    
	    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
	    factory.setJpaVendorAdapter(vendorAdapter);
	    factory.setPackagesToScan("it.sara.registroManager.entity");
	    factory.setDataSource(dataSource());
	    factory.afterPropertiesSet();
	
	    
	    return factory;
	}

	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager()  throws NamingException{

	    JpaTransactionManager txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory().getObject());
	    return txManager;
	}


	
	

}
