package it.sara.registroManager.utility;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.TemplateException;
import it.sara.registroManager.entity.Comunicazione;
import it.sara.registroManager.entity.ComunicazionePersona;
import it.sara.registroManager.entity.ComunicazionePersonaID;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.entity.Template;
import it.sara.registroManager.mail.MailPojo;

@Component("ComunicazioneManager")
public class ComunicazioneManager {

	public List<ComunicazionePersona> loadList(List<Comunicazione> listaComunicazione) {

		List<ComunicazionePersona> listaComunicazionePersona = new ArrayList<ComunicazionePersona>();

		for (Comunicazione c : listaComunicazione) {
			for (SchedaPersona s : c.getListaSchedaPersona()) {

				ComunicazionePersonaID pk = new ComunicazionePersonaID();
				pk.setComunicazione(c);
				pk.setSchedaPersona(s);

				ComunicazionePersona cp = new ComunicazionePersona();
				cp.setPk(pk);

				listaComunicazionePersona.add(cp);
			}
		}

		return listaComunicazionePersona;
	}

	public void setFieldLettera(Template templateDb, Template t, SchedaPersona s, String ambito) {

		Comunicazione c = new Comunicazione();

		c.setTipo(Utility.TIPO_COMUNICAZIONE_LETTERA);
		c.setDataInvio(new Date());
		c.setTemplate(templateDb);
		c.setTesto(t.getTesto());
		c.setTipoAmbito(ambito);

		s.getListaComunicazione().add(c);
		c.getListaSchedaPersona().add(s);

	}

	@SuppressWarnings("deprecation")
	public void setTemplate(Template t, SchedaPersona s) {

		Reader reader = new StringReader(t.getTesto());

		byte[] byteArray;

		try {

			freemarker.template.Template tt = new freemarker.template.Template(t.getTesto(), reader);

			if (t.getId() == 1 && t.getNomeTemplate().equals("Inserimento al registro"))
				byteArray = FreeMarkerTemplateUtils.processTemplateIntoString(tt, this.loadMap(s, "Inserimento")).getBytes();

			else
				byteArray = FreeMarkerTemplateUtils.processTemplateIntoString(tt, this.loadMap(s, "")).getBytes();

			String text = new String(byteArray, StandardCharsets.UTF_8);
			t.setTesto(text);

		} catch (IOException | TemplateException | ParseException e) {
			e.printStackTrace();
		}
	}

	public MailPojo setMailPojo(Template t, SchedaPersona s, String oggetto) {

		MailPojo m = new MailPojo();

		m.setMailFrom(Utility.MITTENTE_SARA); // MITTENTE
		m.setMailTo(s.getEmail()); // DESTINATARIO
		m.setMailSubject(oggetto);

		if (t.getPersoneCc() != null || !("").equals(t.getPersoneCc())) {
			m.setMailCc(t.getPersoneCc());
		}

		return m;

	}

	public void setFieldMail(Template templateDb, Template t, String oggetto, SchedaPersona s, String ambito) {

		Comunicazione c = new Comunicazione();

		c.setTipo(Utility.TIPO_COMUNICAZIONE_MAIL);
		c.setOggetto(oggetto);
		c.setMittente(Utility.MITTENTE_SARA);
		c.setDataInvio(new Date());
		c.setTemplate(templateDb);
		c.setTesto(t.getTesto());
		c.setTipoAmbito(ambito);

		s.getListaComunicazione().add(c);
		c.getListaSchedaPersona().add(s);

	}

	public Map<String, String> loadMap(SchedaPersona schedaPersona, String action) throws ParseException {

		Map<String, String> model = new HashMap<String, String>();

		model.put("Email", schedaPersona.getEmail() != null ? schedaPersona.getEmail() : "");
		model.put("Societa", schedaPersona.getSocieta().getNome());

		if (action.equals("Inserimento"))
			model.put("MotivoIscrizione",
					schedaPersona.getMotivoIscrizione() != null ? schedaPersona.getMotivoIscrizione() : "");

		else
			model.put("MotivoVariazione",
					schedaPersona.getMotivoUltimaVariazione() != null ? schedaPersona.getMotivoUltimaVariazione() : "");

		model.put("DataUltimoAggiornamento", Utility.formatDate(schedaPersona.getDataUltimoAggiornamento()));
		model.put("Cognome", schedaPersona.getCognome());
		model.put("Nome", schedaPersona.getNome());
		model.put("DataNascita", "" + Utility.formatDate(schedaPersona.getDataNascita()));
		model.put("CodiceFiscale", schedaPersona.getCodiceFiscale().toUpperCase());
		model.put("Ruolo", schedaPersona.getRuolo());

		if (schedaPersona.getDataIscrizione() != null)
			model.put("DataIscrizione", Utility.formatDate(schedaPersona.getDataIscrizione()));
		else
			model.put("DataIscrizione", "");

		return model;
	}

}
