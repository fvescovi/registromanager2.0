package it.sara.registroManager.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.bean.UtenteWeb;
import it.sara.registroManager.business.GestioneUtente;
import it.sara.registroManager.utility.RegistroEntity;
import it.sara.registroManager.utility.Utility;

@RestController
@RequestMapping("/rest/utente")
public class UtenteController {

	private Logger logger = LoggerFactory.getLogger(UtenteController.class);

	@Autowired
	GestioneUtente gestioneUtente;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@CrossOrigin(origins = "*")
	@RequestMapping(value = { "/username" }, method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public RegistroEntity getUtente(HttpServletResponse response, HttpServletRequest request) {

		logger.info("Username: " + SecurityContextHolder.getContext().getAuthentication().getName());

		UtenteWeb utenteWeb = new UtenteWeb();
		RegistroEntity registroEntity = null;
		HttpHeaders responseHeaders = new HttpHeaders();

		String username = request.getHeader(Utility.HEADER_USER);
		String groups = request.getHeader(Utility.HEADER_GROUPS);
		String ruolo = "";

		ArrayList<String> ruoli = new ArrayList<String>();

		logger.info("@@@@@@ UTENTE HEADER IV-USER: " + username + " GROUPS HEADER IV-GROUPS: " + groups);

		if (username != null)
			utenteWeb.setUsername(username);

		if (groups != null)
			utenteWeb.setRuolo(groups);

		if (username == null && groups == null) {
			logger.error("utente e gruppo inesistenti");
			return new RegistroEntity(utenteWeb, null, Utility.GENERIC_ERROR,
					Utility.UTENTE_NOTFOUND_ERROR + " E " + Utility.GRUPPO_NON_ASSOCIATO, HttpStatus.NOT_FOUND);
		}

		if (("").equals(groups) || groups == null) {
			logger.error("gruppo inesistente");
			return new RegistroEntity(utenteWeb, null, Utility.GENERIC_ERROR, Utility.GRUPPO_NON_ASSOCIATO,
					HttpStatus.NOT_FOUND);
		}

		if (("").equals(username) || username == null) {
			logger.error("utente inesistente");

			return new RegistroEntity(utenteWeb, null, Utility.GENERIC_ERROR, Utility.UTENTE_NOTFOUND_ERROR,
					HttpStatus.NOT_FOUND);
		} else {

			StringTokenizer str = new StringTokenizer(groups, ",");

			while (str.hasMoreElements()) {
				ruoli.add(str.nextToken());
			}

			ruolo = getRuolo(ruoli);

			if (("").equals(ruolo) || ruolo == null) {

				logger.error("gruppo inesistente");

				return new RegistroEntity(utenteWeb, null, Utility.GENERIC_ERROR, Utility.GRUPPO_NOTFOUND_ERROR,
						HttpStatus.NOT_FOUND);
			}

			utenteWeb.setUsername(username);
			utenteWeb.setRuolo(ruolo);
			utenteWeb.setRuoli(ruoli);

			if (ruoli.isEmpty())
				ruoli.add(ruolo);

			utenteWeb.setRuoli(ruoli);

			if ((("").equals(utenteWeb.getCookie()) || utenteWeb.getCookie() == null)
					|| !utenteWeb.getUsername().equals(username)) {

				String cookie = request.getHeader("Cookie");

				logger.debug("##### COOKIE: " + cookie);

				if (cookie != null || !("").equals(cookie)) {
					utenteWeb.setCookie(cookie);
					utenteWeb.setSessionID(request.getSession().getId());
					responseHeaders.set("cookie", request.getHeader("Cookie"));
				}
			}

			responseHeaders.set("INFO", "UTENTE GIÀ IN SESSIONE");
			registroEntity = new RegistroEntity(utenteWeb, responseHeaders, Utility.UTENTE_OK, null, HttpStatus.OK);

			return registroEntity;
		}
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = { "/roleUtente" }, method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public List<String> ruoliUtente() {
		List<String> risultato = gestioneUtente
				.getRuoliFromGruppi(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
		logger.debug("Ruoli utente: " + risultato);
		return risultato;
	}

	public String getRuolo(List<String> listRuoli) {

		String ruolo = "";

		int[] arrayKey = new int[listRuoli.size()];

		if (listRuoli.size() == 1) {

			String r = listRuoli.get(0);

			for (Entry<Integer, String> map : Utility.mapRuoli.entrySet()) {
				
				if (r.contains("\"")) {
					StringTokenizer st = new StringTokenizer(r, "\"");
					r = st.nextToken();
				}
				
				if (r.equals(map.getValue())) {
					ruolo = map.getValue();
					break;
				}
				
			}

		} else {

			int i = 0;
			int max = 0;

			for (String s : listRuoli) {
				
				for (Entry<Integer, String> map : Utility.mapRuoli.entrySet()) {
					
					if (s.contains("\"")) {
						StringTokenizer st = new StringTokenizer(s, "\"");
						s = st.nextToken();
					}

					if (s.equals(map.getValue())) {
						int key = map.getKey();
						arrayKey[i] = key;
						i++;
					}

				}
			}

			for (int z = 0; z < arrayKey.length; z++) {
				if (arrayKey[z] > max) {
					max = arrayKey[z];
				}
			}

			ruolo = Utility.mapRuoli.get(max);

		}

		return ruolo;
	}

}
