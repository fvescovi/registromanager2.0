package it.sara.registroManager.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.dao.SocietaDao;
import it.sara.registroManager.entity.Societa;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class SocietaController {

	private Logger logger = LoggerFactory.getLogger(SocietaController.class);

	@Autowired
	SocietaDao societaDao;

	@GetMapping(value = "/caricaListaSocieta", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Societa> caricaListaSocieta() throws SQLException, NullPointerException {

		List<Societa> listaSocieta = (List<Societa>) societaDao.findSocietaByStato();

		return listaSocieta;

	}

	@GetMapping(value = "/findSocietaById/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Societa findSocietaById(@PathVariable(name = "id") int id)
			throws SQLException, ParseException, NullPointerException {

		Optional<Societa> societa = societaDao.findById(id);

		return societa.get();

	}

	@PostMapping(value = "/saveSocieta")
	public Societa saveSocieta(@RequestBody Societa societa) throws SQLException, NullPointerException, ParseException {

		if (societa.getId() != null)
			if (societa.getId() == 0)
				societa.setId(null);

		societaDao.save(societa);

		return societa;
	}

}
