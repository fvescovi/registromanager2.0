package it.sara.registroManager.utility;

import java.sql.SQLException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import it.sara.registroManager.bean.PaginaSchedaPersona;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.entity.SchedaPersona;

@Component("PaginatorManager")
public class PaginatorManager {

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@SuppressWarnings("deprecation")
	public PaginaSchedaPersona getPaginaSchedaPersona(int idSchedaInformazione, int numeroPagina, int righePagina) {

		Pageable p = new PageRequest(numeroPagina, righePagina);

		Page<SchedaPersona> l = null;

		try {

			l = schedaPersonaDao.getPageSchedaPersona(idSchedaInformazione, p);

		} catch (NullPointerException | SQLException | ParseException e) {

			e.printStackTrace();

		}

		if (l != null) {

			PaginaSchedaPersona risultato = new PaginaSchedaPersona();

			risultato.setListaSchedaPersona(l.getContent());
			risultato.setPaginaCorrente(l.getNumber());
			risultato.setRecordPerPagina(l.getNumberOfElements());
			risultato.setTotalePagina(l.getTotalPages());
			risultato.setTotaleRecord(l.getTotalElements());

			return risultato;

		} else
			return null;

	}

}
