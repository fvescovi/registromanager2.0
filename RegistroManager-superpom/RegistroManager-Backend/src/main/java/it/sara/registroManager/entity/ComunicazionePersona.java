package it.sara.registroManager.entity;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Embeddable
@Table(name = "COMUNICAZIONE_PERSONA")
@AssociationOverrides({
		@AssociationOverride(name = "pk.comunicazione", joinColumns = @JoinColumn(name = "ID_COMUNICAZIONE")),
		@AssociationOverride(name = "pk.schedaPersona", joinColumns = @JoinColumn(name = "ID_SCHEDA_PERSONA")) })
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class ComunicazionePersona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4272301266756501476L;

	@EmbeddedId
	ComunicazionePersonaID pk = new ComunicazionePersonaID();

	public ComunicazionePersonaID getPk() {
		return pk;
	}

	public void setPk(ComunicazionePersonaID pk) {
		this.pk = pk;
	}

}
