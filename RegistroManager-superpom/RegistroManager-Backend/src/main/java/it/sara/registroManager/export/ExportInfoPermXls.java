package it.sara.registroManager.export;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.utility.Utility;

@Component("ExportInfoPermXls")
public class ExportInfoPermXls extends AbstractExportSchedaPersona {

	public static String[] columns = { "DATA E ORA CREAZIONE: ", "DATA E ORA ULTIMO AGGIORNAMENTO",
			"NOME DEL TITOLARE DELL'ACCESSO", "COGNOME DEL TITOLARE DELL'ACCESSO",
			"COGNOME DI NASCITA DEL TITOLARE DELL'ACCESSO (SE DIVERSO)",
			"NUMERI DI TELEFONO PROFESSIONALI (LINEA TELEFONICA PROFESSIONALE DIRETTA FISSA E MOBILE)",
			"NOME E INDIRIZZO DELL'IMPRESA", "FUNZIONE E MOTIVO DELL'ACCESSO A INFORMAZIONI PRIVILEGIATE",
			"OTTENUTO (DATA E ORA IN CUI IL TITOLARE È STATO INSERITO NELLA SEZIONE DEGLI ACCESSI PERMANENTI)",
			"CESSATO (DATA E ORA IN CUI IL TITOLARE È STATO RIMOSSO DALLA SEZIONE DEGLI ACCESSI PERMANENTI)",
			"DATA DI NASCITA", "NUMERI DI IDENTIFICAZIONE NAZIONALE (SE APPLICABILE)",
			"NUMERI DI TELEFONO PRIVATI (CASA E CELLULARE PERSONALE)",
			"INDIRIZZO PRIVATO COMPLETO (VIA,NUMERO CIVICO, LOCALITÁ, CAP, STATO)" };

	@Override
	public void creazioneFoglioXlsPerm(List<SchedaPersona> listaSchedaPersona, ServletOutputStream fileOut)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Registro_Accessi_Permanenti");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row rowSezione = sheet.createRow(0);

		Cell sezioneCell = rowSezione.createCell(0);
		sezioneCell.setCellValue(
				"Sezione degli accessi permanenti dell'elenco delle persone aventi accesso a informazioni privilegiate");
		sezioneCell.setCellStyle(headerCellStyle);

		Row headerRow = sheet.createRow(2);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 3;

		Row rowInfo = sheet.createRow(rowNum++);

		for (SchedaPersona s : listaSchedaPersona) {

			for (AccessoInformazione ai : s.getListaAccessoInformazione()) {

				rowInfo.createCell(0)
						.setCellValue(Utility.formatDateRegistro(ai.getSchedaInformazione().getDataIdentificazione(),
								ai.getSchedaInformazione().getOraIdentificazione()));
				rowInfo.createCell(1).setCellValue(
						new SimpleDateFormat("yyyy-MM-dd HH:mm").format(ai.getSchedaInformazione().getDataSezione()));

				if (ai.getSchedaInformazione().getInformazionePrivilegiata().equals(Utility.ACCESSO_PERMANENTE)) {

					Row row = sheet.createRow(rowNum++);

					row.createCell(0).setCellValue("");
					row.createCell(1).setCellValue("");
					row.createCell(2).setCellValue(s.getNome());
					row.createCell(3).setCellValue(s.getCognome());
					row.createCell(4)
							.setCellValue(s.getCognomeNascita() != s.getCognome() ? s.getCognomeNascita() : "");
					row.createCell(5).setCellValue(s.getTelefonoProfessionale());
					row.createCell(6).setCellValue(s.getNomeIndirizzoImpresa());
					row.createCell(7).setCellValue(ai.getMotivazioneAccesso());
					row.createCell(8).setCellValue(Utility.formatDateRegistro(ai.getDataAccessoInformazione(),
							ai.getOraAccessoInformazione()));

					if (ai.getDataCessazioneInformazione() != null)
						row.createCell(9).setCellValue(Utility.formatDateRegistro(ai.getDataCessazioneInformazione(),
								ai.getOraCessazioneInformazione()));

					else
						row.createCell(9).setCellValue("");

					row.createCell(10).setCellValue(Utility.formatDataNascita(s.getDataNascita().toString()));
					row.createCell(11).setCellValue(s.getIdentificativoNazionale());
					row.createCell(12).setCellValue(s.getTelefonoPrivato());

					if (s.getIndirizzo() != null && s.getLocalita() != null && s.getCap() != null
							&& s.getNazione() != null) {

						row.createCell(13).setCellValue(
								s.getIndirizzo() + ", " + s.getLocalita() + ", " + s.getCap() + ", " + s.getNazione());

					} else {
						row.createCell(13).setCellValue("");
					}

				}

			}

		}

		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		workbook.write(fileOut);
		workbook.close();

	}

}