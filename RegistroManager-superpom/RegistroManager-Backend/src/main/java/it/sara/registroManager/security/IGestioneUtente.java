package it.sara.registroManager.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public interface IGestioneUtente {

	public String getAdministrator();
    public boolean hasRole(String nomeRuolo, Collection<? extends GrantedAuthority> elencoGruppi) ;

}
