package it.sara.registroManager.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "VERSIONING")
public class Versioning implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3934096215199515866L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_VERSIONING")
	@SequenceGenerator(name = "SEQUENCE_VERSIONING", sequenceName = "SEQ_VERSIONING", allocationSize = 1)
	@Column(name = "ID_VERSIONING")
	private Integer idVersioning;

	@Column(name = "VERSIONE")
	private Integer versione;

	@Column(name = "DATI_MODIFICATI")
	private String datiModificati;

	@Column(name = "DATA_CREAZIONE")
	private Date dataCreazione;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SCHEDA_PERSONA")
	private SchedaPersona schedaPersona;

	@Column(name = "SCHEDA_JSON")
	private String schedaJson;

	public Integer getIdVersioning() {
		return idVersioning;
	}

	public void setIdVersioning(Integer idVersioning) {
		this.idVersioning = idVersioning;
	}

	public Integer getVersione() {
		return versione;
	}

	public void setVersione(Integer versione) {
		this.versione = versione;
	}

	public String getDatiModificati() {
		return datiModificati;
	}

	public void setDatiModificati(String datiModificati) {
		this.datiModificati = datiModificati;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public SchedaPersona getSchedaPersona() {
		return schedaPersona;
	}

	public void setSchedaPersona(SchedaPersona schedaPersona) {
		this.schedaPersona = schedaPersona;
	}

	public String getSchedaJson() {
		return schedaJson;
	}

	public void setSchedaJson(String schedaJson) {
		this.schedaJson = schedaJson;
	}

}
