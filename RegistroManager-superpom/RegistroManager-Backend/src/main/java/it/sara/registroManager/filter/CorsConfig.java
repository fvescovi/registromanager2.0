package it.sara.registroManager.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public abstract class CorsConfig implements WebMvcConfigurer {

	private Logger logger = LoggerFactory.getLogger(CorsConfig.class);

	@Override
	public void addCorsMappings(CorsRegistry registry) {

		logger.debug(" @@@@@@ addCorsMappings @@@@@@@");

		registry.addMapping("/**")
				.allowedOrigins("http://mondosvil.sara.it", "https://mondosvil.sara.it",
						"http://was01svil.aws.sara.it:9083", "http://was01svil.aws.sara.it:9083")
				.allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS").allowCredentials(true)// .allowedHeaders("iv-user","iv-groups","x-xsrf-token","Access-Control-Allow-Headers","Content-Type,
																											// Accept,
																											// X-Requested-With,
																											// remember-me")

		;
	}

}