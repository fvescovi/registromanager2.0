package it.sara.registroManager.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import freemarker.template.TemplateException;
import it.sara.registroManager.dao.ComunicazioneDao;
import it.sara.registroManager.dao.ComunicazionePersonaDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.dao.TemplateDao;
import it.sara.registroManager.entity.Comunicazione;
import it.sara.registroManager.entity.ComunicazionePersona;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.entity.Template;
import it.sara.registroManager.export.ExportPdf;
import it.sara.registroManager.mail.MailPojo;
import it.sara.registroManager.mail.MailService;
import it.sara.registroManager.utility.ComunicazioneManager;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class ComunicazioneController {

	private Logger logger = LoggerFactory.getLogger(ComunicazioneController.class);

	@Autowired
	ExportPdf exportPdf;

	@Autowired
	MailService mailService;

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	TemplateDao templateDao;

	@Autowired
	ComunicazioneDao comunicazioneDao;

	@Autowired
	ComunicazionePersonaDao comunicazionePersonaDao;

	@Autowired
	ComunicazioneManager comunicazioneManager;

	@GetMapping(value = "/caricaListaComunicazione/{ambito}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<List<ComunicazionePersona>> caricaListaTemplate(@PathVariable(name = "ambito") String ambito)
			throws SQLException, NullPointerException, ParseException {

		List<Comunicazione> listaEmail = comunicazioneDao.findListaEmail(ambito);
		List<Comunicazione> listaLettera = comunicazioneDao.findListaLettera(ambito);

		List<ComunicazionePersona> listaEmailPersona = new ArrayList<ComunicazionePersona>();
		List<ComunicazionePersona> listaLetteraPersona = new ArrayList<ComunicazionePersona>();
		List<List<ComunicazionePersona>> listaCompleta = new ArrayList<List<ComunicazionePersona>>();

		if (listaEmail.size() != 0)
			listaEmailPersona = comunicazioneManager.loadList(listaEmail);

		if (listaLettera.size() != 0)
			listaLetteraPersona = comunicazioneManager.loadList(listaLettera);

		listaCompleta.add(listaEmailPersona);
		listaCompleta.add(listaLetteraPersona);

		return listaCompleta;

	}

	@GetMapping(value = "/findComunicazioneById/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ComunicazionePersona findComunicazioneById(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {

		int idComunicazione = Integer.parseInt(id);

		ComunicazionePersona comunicazionePersona = comunicazionePersonaDao
				.findComunicazionePersonaById(idComunicazione);

		return comunicazionePersona;

	}

	@PostMapping(value = "/getTemplates", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Template> getTemplates(@RequestBody SchedaPersona schedaPersona)
			throws SQLException, NullPointerException, IOException, TemplateException, ParseException {

		List<Template> listaTemplate = (List<Template>) templateDao.findAll();

		for (Template t : listaTemplate) {

			comunicazioneManager.setTemplate(t, schedaPersona);

		}

		return listaTemplate;

	}

	@PostMapping(value = "/inviaEmail/{id}/{oggetto}/{ambito}")
	public Object inviaEmail(@PathVariable(name = "id") Integer id, @PathVariable(name = "oggetto") String oggetto,
			@PathVariable(name = "ambito") String ambito, @RequestBody Template t) throws Exception {

		Optional<SchedaPersona> schedaPersonaDb = schedaPersonaDao.findById(id);

		Optional<Template> tt = templateDao.findById(t.getId());

		SchedaPersona s = schedaPersonaDb.get();

		MailPojo m = comunicazioneManager.setMailPojo(t, s, oggetto);

		Object object = mailService.sendEmail(m, t);

		if (object != null) {

			comunicazioneManager.setFieldMail(tt.get(), t, oggetto, s, ambito);

			schedaPersonaDao.save(s);

			return object;

		} else
			return null;

	}

	@PostMapping(value = "/stampaLettera/{id}/{ambito}")
	public void stampaLettera(@PathVariable(name = "id") Integer id, @PathVariable(name = "ambito") String ambito,
			@RequestBody Template t, HttpServletResponse response)
			throws ParseException, InvalidFormatException, IOException, NullPointerException, SQLException {

		Optional<SchedaPersona> schedaPersonaDb = schedaPersonaDao.findById(id);

		Optional<Template> tt = templateDao.findById(t.getId());

		SchedaPersona s = schedaPersonaDb.get();

		comunicazioneManager.setFieldLettera(tt.get(), t, s, ambito);

		schedaPersonaDao.save(s);

		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=\"Registro_Persone.pdf\"");

		exportPdf.creazioneLettera(t.getTesto(), response.getOutputStream());

	}

	@PostMapping(value = "/stampaSchedaPersona")
	public void stampaSchedaPersona(@RequestBody SchedaPersona schedaPersona, HttpServletResponse response)
			throws ParseException, InvalidFormatException, IOException {

		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + schedaPersona.getNome() + " " + schedaPersona.getCognome() + ".pdf\"");

		exportPdf.stampaPersona(schedaPersona, response.getOutputStream());

	}

}
