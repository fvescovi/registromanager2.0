package it.sara.registroManager.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.stereotype.Component;

@Component("Utility")
public class Utility {

	public static String HEADER_USER = "iv-user";
	public static String HEADER_GROUPS = "iv-groups";
	public static String STATO_BOZZA = "Bozza";
	public static String STATO_DEFINITIVO = "Definitivo";
	public static String STATO_CANCELLATO = "Cancellato";
	public static String TIPO_COMUNICAZIONE_MAIL = "M";
	public static String TIPO_COMUNICAZIONE_LETTERA = "L";
	public static String TIPO_ACCESSO_PERMANENTE = "Permanente";
	public static String TIPO_ACCESSO_LIMITATO = "Limitato";
	public static String STATO_INFO_PERMANENTE = "Permanente";
	public static String STATO_CSV_VERSIONING = "Versioning Definitivo";
	public static String STATO_INFO_ATTIVA = "Attiva";
	public static String STATO_INFO_CESSATA = "Cessata";
	public static String ACCESSO_PERMANENTE = "Accesso Permanente";
	public static String MITTENTE_SARA = "legale.societario@sara.it";
	public static String AMBITO_INSIDER_LIST = "I";
	public static String AMBITO_REGISTRO_MANAGER = "R";
	public static String STORICO_SI = "S";
	public static String STORICO_NO = "N";
	public static Map<Integer, String> mapRuoli = new HashMap<Integer, String>();

	public Utility() {
		loadMapRuoli();
	}

	/**
	 * MESSAGGI
	 */

	public static final String GENERIC_ERROR = "ERRORE GENERICO";

	public static final String UTENTE_NOTFOUND_ERROR = "UTENTE NON AUTORIZZATO";

	public static final String GRUPPO_NON_ASSOCIATO = "GRUPPO NON ASSOCIATO";

	public static final String GRUPPO_NOTFOUND_ERROR = "GRUPPO NON AUTORIZZATO";

	public static final String UTENTE_OK = "UTENTE IN ANAGRAFICA";

	public static void loadMapRuoli() {
		mapRuoli.put(1, "REGISTRO_MANAGER_SUPERVISORE");
		mapRuoli.put(2, "REGISTRO_MANAGER_CONFIG");
		mapRuoli.put(3, "REGISTRO_MANAGER_GESTIONE");
	}

	public static String formatDate(Date date) throws ParseException {
		SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ssss ZZZ yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
		String input = inputFormat.format(date);
		Date data = inputFormat.parse(input);
		String formattedDate = outputFormat.format(data);

		return formattedDate;
	}
	
	public static String formatDateRicercaAccesso(Date date) throws ParseException {
		SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ssss ZZZ yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String input = inputFormat.format(date);
		Date data = inputFormat.parse(input);
		String formattedDate = outputFormat.format(data);

		return formattedDate;
	}

	public static String formatDateCalendar(String dataNascita) throws ParseException {
		if (dataNascita.contains("T")) {
			final String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
			final SimpleDateFormat inputFormat = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT);
			SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date data = inputFormat.parse(dataNascita);
			String formattedDate = outputFormat.format(data);

			return formattedDate;
		} else {
			return dataNascita;
		}
	}

	public static String formatDataNascita(String dataNascita) throws ParseException {
		
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date data = inputFormat.parse(dataNascita);
		String formattedDate = outputFormat.format(data);

		return formattedDate;
	}

	public static String formatDateRegistro(Date data, String ora) {

		Calendar cal = Calendar.getInstance();

		StringTokenizer str = new StringTokenizer(ora, ":");

		if (str.countTokens() == 2) {

			while (str.hasMoreElements()) {

				cal.setTime(data);
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str.nextToken()));
				cal.set(Calendar.MINUTE, Integer.parseInt(str.nextToken()));
				cal.set(Calendar.SECOND, 0);

			}

			// IMPORT CSV
		} else if (("00:00:00").equals(ora)) {

			cal.setTime(data);
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str.nextToken()));
			cal.set(Calendar.MINUTE, Integer.parseInt(str.nextToken()));
			cal.set(Calendar.SECOND, Integer.parseInt(str.nextToken()));

		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		String s = sdf.format(cal.getTime());

		return s;

	}

	public static String formatHour(Date date) throws ParseException {
		SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ssss ZZZ yyyy");
		SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm");
		String input = inputFormat.format(date);
		Date data = inputFormat.parse(input);
		String formattedDate = outputFormat.format(data);

		return formattedDate;
	}

	public static String getInizialeSocieta(String nomeSocieta) {

		String value = null;

		value = "" + nomeSocieta.charAt(0);

		return value;
	}

	public static String getCodiceSchedaPersona(String inizialeSocieta, Integer contSchedaPersona, int idVersioning) {

		String codice = inizialeSocieta + "-" + String.format("%06d", contSchedaPersona) + "-V" + idVersioning;

		return codice;

	}

}
