package it.sara.registroManager.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ACCESSO_INFORMAZIONE")
public class AccessoInformazione implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_ACCESSO_INFORMAZIONE")
	@SequenceGenerator(name = "SEQUENCE_ACCESSO_INFORMAZIONE", sequenceName = "SEQ_ACCESSO_INFORMAZIONE", allocationSize = 1)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "DATA_ACCESSO_INFORMAZIONE")
	private Date dataAccessoInformazione;

	@Column(name = "ORA_ACCESSO_INFORMAZIONE")
	private String oraAccessoInformazione;

	@Column(name = "DATA_CESSAZIONE_INFORMAZIONE")
	private Date dataCessazioneInformazione;

	@Column(name = "ORA_CESSAZIONE_INFORMAZIONE")
	private String oraCessazioneInformazione;

	@Column(name = "MOTIVAZIONE_ACCESSO")
	private String motivazioneAccesso;

	@Column(name = "STORICO")
	private String storico;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, targetEntity = SchedaPersona.class)
	@JoinColumn(name = "ID_SCHEDA_PERSONA")
	private SchedaPersona schedaPersona;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, targetEntity = SchedaInformazione.class)
	@JoinColumn(name = "ID_SCHEDA_INFORMAZIONE")
	private SchedaInformazione schedaInformazione;

	public String getStorico() {
		return storico;
	}

	public void setStorico(String storico) {
		this.storico = storico;
	}

	public SchedaPersona getSchedaPersona() {
		return schedaPersona;
	}

	public void setSchedaPersona(SchedaPersona schedaPersona) {
		this.schedaPersona = schedaPersona;
	}

	public SchedaInformazione getSchedaInformazione() {
		return schedaInformazione;
	}

	public void setSchedaInformazione(SchedaInformazione schedaInformazione) {
		this.schedaInformazione = schedaInformazione;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataAccessoInformazione() {
		return dataAccessoInformazione;
	}

	public void setDataAccessoInformazione(Date dataAccessoInformazione) {
		this.dataAccessoInformazione = dataAccessoInformazione;
	}

	public String getOraAccessoInformazione() {
		return oraAccessoInformazione;
	}

	public void setOraAccessoInformazione(String oraAccessoInformazione) {
		this.oraAccessoInformazione = oraAccessoInformazione;
	}

	public Date getDataCessazioneInformazione() {
		return dataCessazioneInformazione;
	}

	public void setDataCessazioneInformazione(Date dataCessazioneInformazione) {
		this.dataCessazioneInformazione = dataCessazioneInformazione;
	}

	public String getOraCessazioneInformazione() {
		return oraCessazioneInformazione;
	}

	public void setOraCessazioneInformazione(String oraCessazioneInformazione) {
		this.oraCessazioneInformazione = oraCessazioneInformazione;
	}

	public String getMotivazioneAccesso() {
		return motivazioneAccesso;
	}

	public void setMotivazioneAccesso(String motivazioneAccesso) {
		this.motivazioneAccesso = motivazioneAccesso;
	}

}
