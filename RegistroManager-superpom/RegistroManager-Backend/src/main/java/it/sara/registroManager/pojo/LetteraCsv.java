package it.sara.registroManager.pojo;

import com.opencsv.bean.CsvBindByName;

public class LetteraCsv {

	@CsvBindByName(column = "Creata il")
	private String dataInvio;

	@CsvBindByName(column = "destinatario")
	private String destinatario;

	@CsvBindByName(column = "oggetto")
	private String oggetto;

	@CsvBindByName(column = "testo")
	private String testo;

	public String getOggetto() {
		return oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public String getDataInvio() {
		return dataInvio;
	}

	public void setDataInvio(String dataInvio) {
		this.dataInvio = dataInvio;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

}
