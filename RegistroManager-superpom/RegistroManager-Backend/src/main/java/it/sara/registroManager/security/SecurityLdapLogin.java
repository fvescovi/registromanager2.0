package it.sara.registroManager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import it.sara.security.CsrfHeaderFilter;

@EnableAutoConfiguration
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
@Profile("ldapLogin")
public class SecurityLdapLogin  extends WebSecurityConfigurerAdapter{

	@Value("${managerDn}")
	String managerDn; 
	
	@Value("${managerPassword}")
	String managerPassword;
	
	@Value("${urlLdap}")
	String urlLdap;
	
	@Value("${groupBase}")
	String groupBase;
	
	@Value("${userBase}")
	String userBase;
	
	@Value("${groupFilter}")
	String groupFilter;
	
	@Value("${userFilter}")
	String userFilter;

    @Override
    public void configure(WebSecurity security){
        security.ignoring().antMatchers("/css/**","/js/**","/error/**","/webjars/**","/xsl/**","/dist/**","/vendor/**");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	http
            .authorizeRequests()
                .antMatchers("/html/**").permitAll()
                .antMatchers("/","/index.html","/rest/**").hasAnyRole("ISPETTORE","RESPONSABILE AUDIT")
                .antMatchers("/test/**").hasRole("ISPETTORE")
                .anyRequest().authenticated()
                .and()
            .formLogin()
            	.loginProcessingUrl("/login")
            	.loginPage("/login.html")
                .successHandler(createSimpleUrlAuthenticationSuccessHandler())
                .permitAll()
                .and()
            .logout()
                .permitAll()
                .and()
            .csrf().csrfTokenRepository(csrfTokenRepository()).and()
            .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
            ;
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.ldapAuthentication().contextSource()
        .managerDn(managerDn)
        .managerPassword(managerPassword)
        .url(urlLdap)
        .and()
        .groupSearchBase(groupBase)
        .userSearchBase(userBase)
        .groupSearchFilter(groupFilter)
        .userSearchFilter(userFilter)
        ;
    }

    private CsrfTokenRepository csrfTokenRepository() {
    	HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
    	repository.setHeaderName("X-XSRF-TOKEN");
    	return repository;
    } 

    private SimpleUrlAuthenticationSuccessHandler createSimpleUrlAuthenticationSuccessHandler() {
    	SimpleUrlAuthenticationSuccessHandler risultato = new SimpleUrlAuthenticationSuccessHandler();
    	risultato.setAlwaysUseDefaultTargetUrl(true);
    	risultato.setDefaultTargetUrl("/");
    	return risultato;
    }
    

}
