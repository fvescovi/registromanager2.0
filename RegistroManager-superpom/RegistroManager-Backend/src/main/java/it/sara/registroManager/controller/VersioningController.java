package it.sara.registroManager.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.bean.GenericMapper;
import it.sara.registroManager.dao.VersioningDao;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.entity.Versioning;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class VersioningController {

	private Logger logger = LoggerFactory.getLogger(VersioningController.class);

	@SuppressWarnings("rawtypes")
	@Autowired
	GenericMapper genericMapper;

	@Autowired
	VersioningDao versioningDao;

	@GetMapping(value = "/findVersioniByIdSchedaPersona/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Set<Versioning> findVersioniByIdSchedaPersona(@PathVariable(name = "id") int id)
			throws SQLException, ParseException, NullPointerException {

		Set<Versioning> listaVersioniSchedaPersona = versioningDao.getVersioniSchedaPersonaById(id);

		return listaVersioniSchedaPersona;

	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/findVersioningById/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public SchedaPersona findVersioningById(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {
		
		SchedaPersona schedaPersona = new SchedaPersona();

		int idVersioning = Integer.parseInt(id);

		Optional<Versioning> versioning = versioningDao.findById(idVersioning);

		schedaPersona = (SchedaPersona) genericMapper.jsonToJava(versioning.get().getSchedaJson(), schedaPersona);

		return schedaPersona;

	}

}
