package it.sara.registroManager.mail;

import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.sara.registroManager.entity.Template;

@Service
public class MailService {

	@Value("${mittenteMail}")
	String mittenteMail;

	public Object sendEmail(MailPojo mail, Template t) {
		
		// INVIO EMAIL TRAMITE SERVER SARA

		Properties prop = new Properties();
		prop.put("mail.smtp.host", "mailgw.aws.sara.it");
		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.auth", "false");
		prop.put("mail.smtp.starttls.enable", "true"); // TLS

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(null, null);
			}
		});
		
		// INVIO EMAIL TRAMITE UTENTE REALE
		
//		final String username = "pellegrante@gmail.com";
//		final String password = "Pellegrante2019";
//
//		Properties prop = new Properties();
//		prop.put("mail.smtp.host", "smtp.gmail.com");
//		prop.put("mail.smtp.port", "587");
//		prop.put("mail.smtp.auth", "true");
//		prop.put("mail.smtp.starttls.enable", "true"); // TLS
//
//		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
//			protected PasswordAuthentication getPasswordAuthentication() {
//				return new PasswordAuthentication(username, password);
//			}
//		});


		try {

			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(mittenteMail));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(mail.getMailTo()));

			message.setSubject(mail.getMailSubject());

			message.setText(t.getTesto());

			if (t.getPersoneCc() != null || !t.getPersoneCc().equals("")) {

				StringTokenizer str = new StringTokenizer(mail.getMailCc(), ";");

				InternetAddress[] myCcList = new InternetAddress[str.countTokens()];

				int i = 0;

				while (str.hasMoreElements()) {

					InternetAddress ia = new InternetAddress(str.nextToken());
					myCcList[i] = ia;
					i++;
				}

				message.setRecipients(Message.RecipientType.CC, myCcList);

			}

			message.setContent(t.getTesto(), "text/html; charset=\"utf-8\"");

			Transport.send(message);

			System.out.println("Mail inviata correttamente...");

		} catch (MessagingException m) {

			m.printStackTrace();
			
			return null;
		}
		
		return new Object();

	}

}
