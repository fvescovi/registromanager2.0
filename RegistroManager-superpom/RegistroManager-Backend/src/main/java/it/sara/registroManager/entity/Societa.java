package it.sara.registroManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "SOCIETA")
@JsonIgnoreProperties({ "hibernateLazyInitializer" })
public class Societa implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -845484193771871271L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_SOCIETA")
	@SequenceGenerator(name = "SEQUENCE_SOCIETA", sequenceName = "SEQ_SOCIETA", allocationSize = 1)
	@Column(name = "ID_SOCIETA")
	private Integer id;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "INIZIALE")
	private String iniziale;

	@Column(name = "STATO")
	private String stato;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIniziale() {
		return iniziale;
	}

	public void setIniziale(String iniziale) {
		this.iniziale = iniziale;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

}
