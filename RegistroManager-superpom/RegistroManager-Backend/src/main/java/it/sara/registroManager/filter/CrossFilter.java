package it.sara.registroManager.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;

@Configuration
public class CrossFilter extends OncePerRequestFilter {

	private Logger logger = LoggerFactory.getLogger(CrossFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		logger.debug("####### doFilterInternal ######");

		if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
			// CORS "pre-flight" request
			logger.debug(
					"@@@@@@@@@@ doFilterInternal Access-Control-Request-Method valorizzato e request.getMethod()==OPTIONS @@@@@@");
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.addHeader("Access-Control-Allow-Headers", "Content-Type");
			response.addHeader("Access-Control-Max-Age", "1800");// 30 min
			response.addHeader("Access-Control-Allow-Credentials", "true");
		}
		filterChain.doFilter(request, response);
	}

}