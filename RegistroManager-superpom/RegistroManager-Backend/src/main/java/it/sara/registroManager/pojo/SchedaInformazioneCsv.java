package it.sara.registroManager.pojo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.opencsv.bean.CsvBindByName;

import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.SchedaPersona;

/**
 * @author a.magnante
 *
 */
public class SchedaInformazioneCsv {

	@CsvBindByName(column = "nome")
	private String informazionePrivilegiata;

	@CsvBindByName(column = "data creazione sezione")
	private Date dataCreazioneSezione;

	@CsvBindByName(column = "ora creazione sezione")
	private String oraCreazioneSezione;

	@CsvBindByName(column = "data identificazione informazione")
	private Date dataIdentificazione;

	@CsvBindByName(column = "ora identificazione informazione")
	private String oraIdentificazione;

	// @CsvBindByName(column = "")
	private String autoreInformazione;

	// @CsvBindByName(column = "")
	private Date dataUltimoAggiornamentoInfo;

	// @CsvBindByName(column = "")
	private Date dataAccessoInformazione;

	// @CsvBindByName(column = "")
	private String oraAccessoInformazione;

	// @CsvBindByName(column = "")
	private String motivazioneAccesso;

	// @CsvBindByName(column = "")
	private Date dataCessazioneInformazione;

	// @CsvBindByName(column = "")
	private String oraCessazioneInformazione;

	// @CsvBindByName(column = "")
	private String stato;

	private Set<AccessoInformazione> listaAccessoInformazione = new HashSet<AccessoInformazione>();

	private Set<SchedaPersona> listaSchedaPersona = new HashSet<SchedaPersona>();

	public String getInformazionePrivilegiata() {
		return informazionePrivilegiata;
	}

	public void setInformazionePrivilegiata(String informazionePrivilegiata) {
		this.informazionePrivilegiata = informazionePrivilegiata;
	}

	public Date getDataCreazioneSezione() {
		return dataCreazioneSezione;
	}

	public void setDataCreazioneSezione(Date dataCreazioneSezione) {
		this.dataCreazioneSezione = dataCreazioneSezione;
	}

	public String getOraCreazioneSezione() {
		return oraCreazioneSezione;
	}

	public void setOraCreazioneSezione(String oraCreazioneSezione) {
		this.oraCreazioneSezione = oraCreazioneSezione;
	}

	public Date getDataIdentificazione() {
		return dataIdentificazione;
	}

	public void setDataIdentificazione(Date dataIdentificazione) {
		this.dataIdentificazione = dataIdentificazione;
	}

	public String getOraIdentificazione() {
		return oraIdentificazione;
	}

	public void setOraIdentificazione(String oraIdentificazione) {
		this.oraIdentificazione = oraIdentificazione;
	}

	public String getAutoreInformazione() {
		return autoreInformazione;
	}

	public void setAutoreInformazione(String autoreInformazione) {
		this.autoreInformazione = autoreInformazione;
	}

	public Date getDataUltimoAggiornamentoInfo() {
		return dataUltimoAggiornamentoInfo;
	}

	public void setDataUltimoAggiornamentoInfo(Date dataUltimoAggiornamentoInfo) {
		this.dataUltimoAggiornamentoInfo = dataUltimoAggiornamentoInfo;
	}

	public Date getDataAccessoInformazione() {
		return dataAccessoInformazione;
	}

	public void setDataAccessoInformazione(Date dataAccessoInformazione) {
		this.dataAccessoInformazione = dataAccessoInformazione;
	}

	public String getOraAccessoInformazione() {
		return oraAccessoInformazione;
	}

	public void setOraAccessoInformazione(String oraAccessoInformazione) {
		this.oraAccessoInformazione = oraAccessoInformazione;
	}

	public String getMotivazioneAccesso() {
		return motivazioneAccesso;
	}

	public void setMotivazioneAccesso(String motivazioneAccesso) {
		this.motivazioneAccesso = motivazioneAccesso;
	}

	public Date getDataCessazioneInformazione() {
		return dataCessazioneInformazione;
	}

	public void setDataCessazioneInformazione(Date dataCessazioneInformazione) {
		this.dataCessazioneInformazione = dataCessazioneInformazione;
	}

	public String getOraCessazioneInformazione() {
		return oraCessazioneInformazione;
	}

	public void setOraCessazioneInformazione(String oraCessazioneInformazione) {
		this.oraCessazioneInformazione = oraCessazioneInformazione;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public Set<AccessoInformazione> getListaAccessoInformazione() {
		return listaAccessoInformazione;
	}

	public void setListaAccessoInformazione(Set<AccessoInformazione> listaAccessoInformazione) {
		this.listaAccessoInformazione = listaAccessoInformazione;
	}

	public Set<SchedaPersona> getListaSchedaPersona() {
		return listaSchedaPersona;
	}

	public void setListaSchedaPersona(Set<SchedaPersona> listaSchedaPersona) {
		this.listaSchedaPersona = listaSchedaPersona;
	}

}
