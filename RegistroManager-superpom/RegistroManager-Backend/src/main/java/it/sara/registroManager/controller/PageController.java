package it.sara.registroManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.bean.PaginaSchedaPersona;
import it.sara.registroManager.utility.PaginatorManager;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class PageController {

	@Autowired
	PaginatorManager paginatorManager;

	@GetMapping(value = "/getPaginator/{id}/{pageNumber}/{righePerPagina}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public PaginaSchedaPersona getPaginator(@PathVariable(name = "id") String id,
			@PathVariable(name = "pageNumber") Integer pageNumber,
			@PathVariable(name = "righePerPagina") Integer righePerPagina) {

		int idSchedaInformazione = Integer.parseInt(id);

		int numeroPagina = ((pageNumber == null) ? 0 : pageNumber);
		int righePagina = ((righePerPagina == null) ? 5 : righePerPagina);

		PaginaSchedaPersona p = paginatorManager.getPaginaSchedaPersona(idSchedaInformazione, numeroPagina, righePagina);

		return p;
	}

}
