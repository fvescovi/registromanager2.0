package it.sara.registroManager.pojo;

import com.opencsv.bean.CsvBindByName;

public class MailCsv {

	@CsvBindByName(column = "Creata il")
	private String dataInvio;

	@CsvBindByName(column = "testo")
	private String testo;

	@CsvBindByName(column = "Oggetto")
	private String oggetto;

	@CsvBindByName(column = "inviato da")
	private String mittente;

	@CsvBindByName(column = "destinatario")
	private String destinatario;

	public String getDataInvio() {
		return dataInvio;
	}

	public void setDataInvio(String dataInvio) {
		this.dataInvio = dataInvio;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public String getOggetto() {
		return oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public String getMittente() {
		return mittente;
	}

	public void setMittente(String mittente) {
		this.mittente = mittente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

}
