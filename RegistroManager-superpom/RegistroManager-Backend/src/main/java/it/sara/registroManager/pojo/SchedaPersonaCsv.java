package it.sara.registroManager.pojo;

import com.opencsv.bean.CsvBindByName;

public class SchedaPersonaCsv {

	public SchedaPersonaCsv() {

	}

	@CsvBindByName(column = "Codice fiscale")
	private String codiceFiscale;

	@CsvBindByName(column = "Nome")
	private String nome;

	@CsvBindByName(column = "Cognome")
	private String cognome;

	@CsvBindByName(column = "Cognome_Nascita")
	private String cognomeNascita;

	@CsvBindByName(column = "telefoni professionali")
	private String telefonoProfessionale;

	@CsvBindByName(column = "telefoni privati")
	private String telefonoPrivato;

	@CsvBindByName(column = "Indirizzo")
	private String indirizzo;

	@CsvBindByName(column = "CAP")
	private String cap;

	@CsvBindByName(column = "Localita")
	private String localita;

	@CsvBindByName(column = "Provincia")
	private String provincia;

	@CsvBindByName(column = "Nazione")
	private String nazione;

	@CsvBindByName(column = "Id_Nazionale")
	private String identificativoNazionale;

	@CsvBindByName(column = "data nascita")
	private String dataNascita;

	@CsvBindByName(column = "email")
	private String email;

	@CsvBindByName(column = "Ruolo")
	private String ruolo;

	@CsvBindByName(column = "Ragione_Sociale")
	private String ragioneSociale;

	@CsvBindByName(column = "Partita IVA")
	private String partitaIva;

	@CsvBindByName(column = "Tipo accesso")
	private String tipoAccessoInformazione;

	@CsvBindByName(column = "Tipo_soggetto")
	private String tipologiaSoggetto;

	@CsvBindByName(column = "codice")
	private String codiceScheda;

	@CsvBindByName(column = "Stato")
	private String stato;

	@CsvBindByName(column = "Motivo ultima variazione")
	private String motivoUltimaVariazione;

	@CsvBindByName(column = "Motivo Iscrizione")
	private String motivoIscrizione;

	@CsvBindByName(column = "autore ultimo aggiornamento")
	private String autore;

	@CsvBindByName(column = "Data iscrizione")
	private String dataIscrizione;

	// Ultimo aggiornamento = Insider List
	// Utimo aggiornamento = Registro Manager
	@CsvBindByName(column = "Ultimo aggiornamento")
	private String dataUltimoAggiornamento;

	@CsvBindByName(column = "Data cancellazione")
	private String dataCancellazione;

	@CsvBindByName(column = "dati modificati")
	private String datiModificati;

	@CsvBindByName(column = "Descrizione Informazione")
	private String descrizioneInformazione;

	@CsvBindByName(column = "created")
	private String dataCreazioneVersioning;

	@CsvBindByName(column = "Manager")
	private String managerRiferimento;

	@CsvBindByName(column = "Societa")
	private String societa;

	public String getSocieta() {
		return societa;
	}

	public void setSocieta(String societa) {
		this.societa = societa;
	}

	public String getDataCreazioneVersioning() {
		return dataCreazioneVersioning;
	}

	public void setDataCreazioneVersioning(String dataCreazioneVersioning) {
		this.dataCreazioneVersioning = dataCreazioneVersioning;
	}

	public String getCodiceScheda() {
		return codiceScheda;
	}

	public void setCodiceScheda(String codiceScheda) {
		this.codiceScheda = codiceScheda;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getMotivoUltimaVariazione() {
		return motivoUltimaVariazione;
	}

	public void setMotivoUltimaVariazione(String motivoUltimaVariazione) {
		this.motivoUltimaVariazione = motivoUltimaVariazione;
	}

	public String getMotivoIscrizione() {
		return motivoIscrizione;
	}

	public void setMotivoIscrizione(String motivoIscrizione) {
		this.motivoIscrizione = motivoIscrizione;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public String getDataIscrizione() {
		return dataIscrizione;
	}

	public void setDataIscrizione(String dataIscrizione) {
		this.dataIscrizione = dataIscrizione;
	}

	public String getDataUltimoAggiornamento() {
		return dataUltimoAggiornamento;
	}

	public void setDataUltimoAggiornamento(String dataUltimoAggiornamento) {
		this.dataUltimoAggiornamento = dataUltimoAggiornamento;
	}

	public String getDataCancellazione() {
		return dataCancellazione;
	}

	public void setDataCancellazione(String dataCancellazione) {
		this.dataCancellazione = dataCancellazione;
	}

	public String getDatiModificati() {
		return datiModificati;
	}

	public void setDatiModificati(String datiModificati) {
		this.datiModificati = datiModificati;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCognomeNascita() {
		return cognomeNascita;
	}

	public void setCognomeNascita(String cognomeNascita) {
		this.cognomeNascita = cognomeNascita;
	}

	public String getTelefonoProfessionale() {
		return telefonoProfessionale;
	}

	public void setTelefonoProfessionale(String telefonoProfessionale) {
		this.telefonoProfessionale = telefonoProfessionale;
	}

	public String getTelefonoPrivato() {
		return telefonoPrivato;
	}

	public void setTelefonoPrivato(String telefonoPrivato) {
		this.telefonoPrivato = telefonoPrivato;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getLocalita() {
		return localita;
	}

	public void setLocalita(String localita) {
		this.localita = localita;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getNazione() {
		return nazione;
	}

	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

	public String getIdentificativoNazionale() {
		return identificativoNazionale;
	}

	public void setIdentificativoNazionale(String identificativoNazionale) {
		this.identificativoNazionale = identificativoNazionale;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}

	public String getTipoAccessoInformazione() {
		return tipoAccessoInformazione;
	}

	public void setTipoAccessoInformazione(String tipoAccessoInformazione) {
		this.tipoAccessoInformazione = tipoAccessoInformazione;
	}

	public String getTipologiaSoggetto() {
		return tipologiaSoggetto;
	}

	public void setTipologiaSoggetto(String tipologiaSoggetto) {
		this.tipologiaSoggetto = tipologiaSoggetto;
	}

	public String getDescrizioneInformazione() {
		return descrizioneInformazione;
	}

	public void setDescrizioneInformazione(String descrizioneInformazione) {
		this.descrizioneInformazione = descrizioneInformazione;
	}

	public String getManagerRiferimento() {
		return managerRiferimento;
	}

	public void setManagerRiferimento(String managerRiferimento) {
		this.managerRiferimento = managerRiferimento;
	}

}
