package it.sara.registroManager.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.dao.TemplateDao;
import it.sara.registroManager.entity.Template;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class TemplateController {

	private Logger logger = LoggerFactory.getLogger(TemplateController.class);

	@Autowired
	TemplateDao templateDao;

	@GetMapping(value = "/caricaListaTemplate", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Template> caricaListaTemplate() throws SQLException, NullPointerException, ParseException {

		List<Template> listaTemplate = (List<Template>) templateDao.findAll();

		return listaTemplate;

	}

	@PostMapping(value = "/addTemplate", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Template addTemplate(@RequestBody Template template)
			throws SQLException, NullPointerException, ParseException {

		template = templateDao.save(template);

		return template;

	}

	@GetMapping(value = "/findTemplateById/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Template findTemplateById(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {
		
		int idTemplate = Integer.parseInt(id);

		Optional<Template> templateDb = templateDao.findById(idTemplate);

		Template template = templateDb.get();

		return template;

	}
}
