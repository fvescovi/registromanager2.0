package it.sara.registroManager.dao;

import java.sql.SQLException;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import it.sara.registroManager.entity.ContatoreScheda;

public interface ContatoreSchedaDao extends CrudRepository<ContatoreScheda, Integer> {
	
	@Query("select max(c.contI) from ContatoreScheda c")
	Integer findMaxValueI() throws SQLException, NullPointerException;
	
	@Query("select max(c.contR) from ContatoreScheda c")
	Integer findMaxValueR() throws SQLException, NullPointerException;

}
