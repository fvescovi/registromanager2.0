package it.sara.registroManager.export;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import javax.servlet.ServletOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.registroManager.dao.AccessoInformazioneDao;
import it.sara.registroManager.dao.SchedaInformazioneDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.utility.SchedaPersonaCrudManager;
import it.sara.registroManager.utility.Utility;

@Component("ExportInfoLimXls")
public class ExportInfoLimXls extends AbstractExportSchedaPersona {

	@Autowired
	SchedaInformazioneDao schedaInformazioneDao;

	@Autowired
	AccessoInformazioneDao accessoInformazioneDao;

	@Autowired
	SchedaPersonaCrudManager schedaPersonaCrudManager;

	public static String[] columns = { "SEZIONE SU: ", "DATA E ORA CREAZIONE: ", "DATA E ORA ULTIMO AGGIORNAMENTO ",
			"NOME DEL TITOLARE DELL'ACCESSO", "COGNOME DEL TITOLARE DELL'ACCESSO",
			"COGNOME DI NASCITA DEL TITOLARE DELL'ACCESSO (SE DIVERSO)",
			"NUMERI DI TELEFONO PROFESSIONALI (LINEA TELEFONICA PROFESSIONALE DIRETTA FISSA E MOBILE)",
			"NOME E INDIRIZZO DELL'IMPRESA", "FUNZIONE E MOTIVO DELL'ACCESSO A INFORMAZIONI PRIVILEGIATE",
			"OTTENUTO (DATA E ORA IN CUI IL TITOLARE HA OTTENUTO L'ACCESSO A INFORMAZIONI PRIVILEGIATE)",
			"CESSATO (DATA E ORA IN CUI IL TITOLARE HA CESSATO DI AVERE ACCESSO A INFORMAZIONI PRIVILEGIATE)",
			"DATA DI NASCITA", "NUMERO DI IDENTIFICAZIONE NAZIONALE (SE APPLICABILE)",
			"NUMERI DI TELEFONO PRIVATI (CASA E CELLULARE PERSONALE)",
			"INDIRIZZO PRIVATO COMPLETO (VIA,NUMERO CIVICO, LOCALITÁ, CAP, STATO)" };

	@Override
	public void creazioneFoglioXls(Set<SchedaPersona> listaSchedaPersona, String[] arrayInfo, ServletOutputStream fileOut)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Registro_Accessi_Singoli");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row rowSezione = sheet.createRow(0);

		Cell sezioneCell = rowSezione.createCell(0);
		sezioneCell.setCellValue("Elenco delle persone aventi accesso a informazioni privilegiate");
		sezioneCell.setCellStyle(headerCellStyle);

		Row headerRow = sheet.createRow(2);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;
		int rowNumInfo = 3;

		HashMap<String, LinkedList<SchedaPersona>> accessMap = schedaPersonaCrudManager.loadAccessMap(listaSchedaPersona, arrayInfo);

		for (String key : accessMap.keySet()) {

			Row rowInfo = sheet.createRow(rowNumInfo++);

			if (key != null) {

				for (SchedaPersona s : accessMap.get(key)) {

					for (AccessoInformazione ai : s.getListaAccessoInformazione()) {

						if (ai.getSchedaInformazione().getInformazionePrivilegiata().equals(key)) {

							rowInfo.createCell(0).setCellValue(key);
							rowInfo.createCell(1)
									.setCellValue(Utility.formatDateRegistro(
											ai.getSchedaInformazione().getDataIdentificazione(),
											ai.getSchedaInformazione().getOraIdentificazione()));
							rowInfo.createCell(2).setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm")
									.format(ai.getSchedaInformazione().getDataSezione()));

							for (int i = 3; i <= 14; i++) {
								rowInfo.createCell(i).setCellValue("");
							}

							rowNumInfo--;

							Row row = sheet.createRow(rowNumInfo + (rowNum++));

							row.createCell(0).setCellValue("");
							row.createCell(1).setCellValue("");
							row.createCell(2).setCellValue("");
							row.createCell(3).setCellValue(s.getNome());
							row.createCell(4).setCellValue(s.getCognome());

							if (s.getCognomeNascita() != null)
								row.createCell(5).setCellValue(s.getCognomeNascita());

							row.createCell(6).setCellValue(s.getTelefonoProfessionale());
							row.createCell(7).setCellValue(s.getNomeIndirizzoImpresa());
							row.createCell(8).setCellValue(ai.getMotivazioneAccesso());
							row.createCell(9).setCellValue(Utility.formatDateRegistro(ai.getDataAccessoInformazione(),
									ai.getOraAccessoInformazione()));

							if (ai.getDataCessazioneInformazione() != null) {
								row.createCell(10).setCellValue(Utility.formatDateRegistro(
										ai.getDataCessazioneInformazione(), ai.getOraCessazioneInformazione()));
							} else {
								row.createCell(10).setCellValue("");
							}

							row.createCell(11).setCellValue(Utility.formatDataNascita(s.getDataNascita().toString()));
							row.createCell(12).setCellValue(s.getIdentificativoNazionale());
							row.createCell(13).setCellValue(s.getTelefonoPrivato());

							if (s.getIndirizzo() != null && s.getLocalita() != null && s.getCap() != null
									&& s.getNazione() != null) {
								row.createCell(14).setCellValue(s.getIndirizzo() + ", " + s.getLocalita() + ", "
										+ s.getCap() + ", " + s.getNazione());
							} else {
								row.createCell(14).setCellValue("");
							}

							rowNumInfo = rowNum + rowNumInfo;
							rowNum = 1;

						}
					}

				}

			}

		}

		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		workbook.write(fileOut);
		workbook.close();

	}

}