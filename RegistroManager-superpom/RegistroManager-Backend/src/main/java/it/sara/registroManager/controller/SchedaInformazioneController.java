package it.sara.registroManager.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.bean.GenericMapper;
import it.sara.registroManager.bean.RicercaAccesso;
import it.sara.registroManager.dao.AccessoInformazioneDao;
import it.sara.registroManager.dao.SchedaInformazioneDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.SchedaInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.utility.SchedaPersonaCrudManager;
import it.sara.registroManager.utility.Utility;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class SchedaInformazioneController {

	private Logger logger = LoggerFactory.getLogger(SchedaInformazioneController.class);

	@Autowired
	SchedaInformazioneDao schedaInformazioneDao;

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	AccessoInformazioneDao accessoInformazioneDao;

	@Autowired
	SchedaPersonaCrudManager schedaPersonaCrudManager;

	@SuppressWarnings("rawtypes")
	@Autowired
	GenericMapper genericMapper;

	@GetMapping(value = "/caricaListaInformazioni", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<SchedaInformazione> caricaListaInformazioni() throws SQLException, ParseException {

		List<SchedaInformazione> listaSchedaInformazione = (List<SchedaInformazione>) schedaInformazioneDao.findAll();

		return listaSchedaInformazione;

	}

	@GetMapping(value = "/findSchedaInformazioneById/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public SchedaInformazione findSchedaInformazioneById(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {

		int idSchedaInformazione = Integer.parseInt(id);

		Optional<SchedaInformazione> schedaInformazione = schedaInformazioneDao.findById(idSchedaInformazione);

		if (schedaInformazione.isPresent())
			return schedaInformazione.get();
		else
			return new SchedaInformazione();

	}

	@GetMapping(value = "/findInfoByNome/{info}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public SchedaInformazione findInfoByNome(@PathVariable(name = "info") String info)
			throws SQLException, ParseException, NullPointerException {

		SchedaInformazione schedaInformazione = schedaInformazioneDao.findInfoByNome(info);

		if (schedaInformazione != null)
			return schedaInformazione;
		else
			return new SchedaInformazione();

	}

	@GetMapping(value = "/findAccessListByIdInfo/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Set<AccessoInformazione> findAccessListByIdInfo(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {

		Set<AccessoInformazione> accessList = accessoInformazioneDao.findAccessoInfoByIdInfo(Integer.parseInt(id));

		return accessList;

	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@GetMapping(value = "/findAccessListByRicercaAccesso/{ricercaAccesso}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public Set<AccessoInformazione> findAccessListByRicercaAccesso(
			@PathVariable(name = "ricercaAccesso") String ricercaAccesso)
			throws SQLException, ParseException, NullPointerException {

		RicercaAccesso ra = new RicercaAccesso();
		Set<AccessoInformazione> accessList = new HashSet<AccessoInformazione>();

		ra = (RicercaAccesso) genericMapper.jsonToJava(ricercaAccesso, ra);

		ra.getStartDate().setHours(0);
		ra.getStartDate().setMinutes(0);

		ra.getEndDate().setHours(23);
		ra.getEndDate().setMinutes(59);

		String startDate = Utility.formatDateRicercaAccesso(ra.getStartDate());
		String endDate = Utility.formatDateRicercaAccesso(ra.getEndDate());
		
		accessList = accessoInformazioneDao.getAccessBetweenDate(ra.getInformazionePrivilegiata(), startDate, endDate);
		
		return accessList;
	}

	// COLLEGA INFORMAZIONI

	@GetMapping(value = "/getInformazioniDisponibiliByIdPersona/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SchedaInformazione> getInformazioniDisponibiliByIdPersona(@PathVariable(name = "id") String id)
			throws ParseException, NullPointerException, SQLException {

		int idSchedaPersona = Integer.parseInt(id);

		Optional<SchedaPersona> schedaPersona = schedaPersonaDao.findById(idSchedaPersona);

		List<SchedaInformazione> listaInformazioniDisponibili = new ArrayList<SchedaInformazione>();

		if (schedaPersona.get().getListaAccessoInformazione().size() == 0) {

			// LISTA ACCESSI VUOTA
			listaInformazioniDisponibili = schedaInformazioneDao.loadEmptyAccessList();

		} else {

			// LISTA ACCESSI PIENA
			listaInformazioniDisponibili = schedaInformazioneDao.findInfoCollegabili(idSchedaPersona);

		}

		return listaInformazioniDisponibili;

	}

	// CESSA INFORMAZIONE

	@GetMapping(value = "/getInformazioniAssociateByIdPersona/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Set<AccessoInformazione> getInformazioniAssociateByIdPersona(@PathVariable(name = "id") String id)
			throws ParseException, NullPointerException, SQLException {

		int idSchedaPersona = Integer.parseInt(id);

		Optional<SchedaPersona> schedaPersona = schedaPersonaDao.findById(idSchedaPersona);

		Set<AccessoInformazione> listaInformazioniAssociate = new HashSet<AccessoInformazione>();

		for (AccessoInformazione a : schedaPersona.get().getListaAccessoInformazione()) {

			if (a.getDataAccessoInformazione() != null)
				listaInformazioniAssociate.add(a);

			if (a.getDataCessazioneInformazione() != null)
				listaInformazioniAssociate.remove(a);
		}

		return listaInformazioniAssociate;

	}

	@PostMapping(value = "/saveSchedaInformazione")
	public SchedaInformazione saveSchedaInformazione(@RequestBody SchedaInformazione schedaInformazione)
			throws NullPointerException, ParseException, SQLException {

		schedaInformazione.setDataUltimoAggiornamentoInfo(new Date());

		if (schedaInformazione.getDataCessazioneInformazione() != null) {

			List<SchedaPersona> listaSchedaPersona = schedaPersonaDao
					.findSpByInformazioneCess(schedaInformazione.getIdSchedaInformazione());

			if (listaSchedaPersona.size() > 0) {

				for (SchedaPersona s : listaSchedaPersona) {

					for (AccessoInformazione a : s.getListaAccessoInformazione()) {

						if (schedaInformazione.getIdSchedaInformazione() == a.getSchedaInformazione()
								.getIdSchedaInformazione() && a.getDataCessazioneInformazione() == null) {

							s.setDataUltimoAggiornamento(new Date());
							a.setDataCessazioneInformazione(schedaInformazione.getDataCessazioneInformazione());
							a.setOraCessazioneInformazione(schedaInformazione.getOraCessazioneInformazione());

						}

					}

					schedaPersonaDao.save(s);

				}

			}

		}

		schedaInformazione = schedaInformazioneDao.save(schedaInformazione);
		
		return schedaInformazione;
	}

	@GetMapping(value = "/getAccessoInformazioneById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Set<AccessoInformazione> getAccessoInformazioneById(@PathVariable(name = "id") String id)
			throws ParseException, NullPointerException, SQLException {

		int idSchedaPersona = Integer.parseInt(id);

		Set<AccessoInformazione> listaAccessoInformazione = accessoInformazioneDao
				.findAccessoInfoByIdPersona(idSchedaPersona);

		return listaAccessoInformazione;

	}

	@GetMapping(value = "/getAccessoPermanente", produces = MediaType.APPLICATION_JSON_VALUE)
	public SchedaInformazione getAccessoPermanente() throws ParseException, NullPointerException, SQLException {

		SchedaInformazione s = schedaInformazioneDao.findAccessoPermanente();

		if (s != null)
			return s;

		else
			return null;

	}

}
