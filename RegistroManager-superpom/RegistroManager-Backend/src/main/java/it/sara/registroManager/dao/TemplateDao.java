package it.sara.registroManager.dao;

import org.springframework.data.repository.CrudRepository;

import it.sara.registroManager.entity.Template;

public interface TemplateDao extends CrudRepository<Template, Integer> {
	
	
	
}
