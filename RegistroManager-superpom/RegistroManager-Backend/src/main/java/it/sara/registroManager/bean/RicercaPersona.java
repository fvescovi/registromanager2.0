package it.sara.registroManager.bean;

public class RicercaPersona {

	private String societa;
	private String nome;
	private String cognome;
	private String motivoIscrizione;
	
	public String getSocieta() {
		return societa;
	}

	public void setSocieta(String societa) {
		this.societa = societa;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getMotivoIscrizione() {
		return motivoIscrizione;
	}

	public void setMotivoIscrizione(String motivoIscrizione) {
		this.motivoIscrizione = motivoIscrizione;
	}

}
