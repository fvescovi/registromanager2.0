package it.sara.registroManager.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.registroManager.bean.RicercaPersona;
import it.sara.registroManager.dao.SchedaPersonaDao;

@Component("RicercaManager")
public class RicercaManager {

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	public String getQueryRicerca(String ambito, RicercaPersona rp) {

		StringBuilder query = new StringBuilder();

		String target = "\\'\\b";

		query.append("select sp from SchedaPersona sp where sp.tipoAmbito='" + ambito + "' and");

		boolean check = false;

		if (rp.getSocieta() != null && !rp.getSocieta().trim().equals("")) {

			if (check) {
				query.append(" and");
			}

			if (rp.getSocieta().contains("'")) {
				String societa = rp.getSocieta().replaceAll(target, "''");
				rp.setSocieta(societa);
			}

			query.append(" lower(sp.societa.nome)='" + rp.getSocieta().toLowerCase() + "'");

			check = true;
		}

		if (rp.getNome() != null && !rp.getNome().trim().equals("")) {

			if (check) {
				query.append(" and");
			}

			if (rp.getNome().contains("'")) {
				String nome = rp.getNome().replaceAll(target, "''");
				rp.setNome(nome);
			}

			query.append(" lower(sp.nome) like '%" + rp.getNome().toLowerCase() + "%'");

			check = true;
		}

		if (rp.getCognome() != null && !rp.getCognome().trim().equals("")) {

			if (check) {
				query.append(" and");
			}

			if (rp.getCognome().contains("'")) {
				String cognome = rp.getCognome().replaceAll(target, "''");
				rp.setCognome(cognome);
			}

			query.append(" lower(sp.cognome) like '%" + rp.getCognome().toLowerCase() + "%'");

			check = true;
		}

		if (rp.getMotivoIscrizione() != null && !rp.getMotivoIscrizione().trim().equals("")) {

			if (check) {
				query.append(" and");
			}

			if (rp.getMotivoIscrizione().contains("'")) {
				String motivoIscrizione = rp.getMotivoIscrizione().replaceAll(target, "''");
				rp.setMotivoIscrizione(motivoIscrizione);
			}

			query.append(" lower(sp.motivoIscrizione) like '%" + rp.getMotivoIscrizione().toLowerCase() + "%'");

			check = true;
		}

		return query.toString();
	}

}
