package it.sara.registroManager.utility;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.registroManager.bean.SchedaPersonaCmp;
import it.sara.registroManager.entity.SchedaPersona;

@Component("VersioningSchedaPersonaManager")
public class VersioningSchedaPersonaManager {

	@Autowired
	SchedaPersonaCmp schedaPersonaCmp;

	public String getListaCampiModificati(SchedaPersona newSchedaPersona, SchedaPersona schedaPersonaDB)
			throws ParseException {

		String datiModificati = null;

		schedaPersonaCmp.loadMaps(newSchedaPersona, schedaPersonaDB);

		datiModificati = schedaPersonaCmp.getDatiModificati();

		return datiModificati;
	}

}
