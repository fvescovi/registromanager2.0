package it.sara.registroManager.utility;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.registroManager.bean.GenericMapper;
import it.sara.registroManager.dao.SchedaInformazioneDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.dao.SocietaDao;
import it.sara.registroManager.dao.TemplateDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.Comunicazione;
import it.sara.registroManager.entity.SchedaInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.entity.Societa;
import it.sara.registroManager.entity.Template;
import it.sara.registroManager.entity.Versioning;
import it.sara.registroManager.pojo.AccessoInformazioneCsv;
import it.sara.registroManager.pojo.LetteraCsv;
import it.sara.registroManager.pojo.MailCsv;
import it.sara.registroManager.pojo.SchedaPersonaCsv;

@Component("CsvManager")
public class CsvManager {

	@Autowired
	SocietaDao societaDao;

	@Autowired
	TemplateDao templateDao;

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	SchedaInformazioneDao schedaInformazioneDao;

	@SuppressWarnings("rawtypes")
	@Autowired
	GenericMapper genericMapper;

	public AccessoInformazione setAccessoInformazioneToDb(AccessoInformazioneCsv a)
			throws ParseException, NullPointerException, SQLException {

		AccessoInformazione ai = new AccessoInformazione();

		ai.setDataAccessoInformazione(new SimpleDateFormat("dd/MM/yyyy").parse(a.getDataAccessoInformazione()));
		ai.setOraAccessoInformazione(a.getOraAccessoInformazione());
		ai.setStorico(Utility.STORICO_NO);

		if (a.getMotivazioneAccesso() != null)
			ai.setMotivazioneAccesso(a.getMotivazioneAccesso());

		SchedaInformazione s = schedaInformazioneDao.findAccessoPermanente();

		if (s != null)
			ai.setSchedaInformazione(s);

		String nome = a.getNome();
		String cognome = a.getCognome();

		// Controllare ambito nelle query 
		
		SchedaPersona sp = schedaPersonaDao.findPersonaByNomeAndCognome(nome, cognome);

		if (sp != null)
			ai.setSchedaPersona(sp);

		else {
			sp = schedaPersonaDao.findPersonaByNomeAndCognomeCancellato(nome, cognome);
			ai.setSchedaPersona(sp);
		}

		sp.getListaAccessoInformazione().add(ai);

		return ai;

	}

	public Comunicazione setFieldMailToDb(MailCsv m) throws ParseException, NullPointerException, SQLException {

		Comunicazione c = new Comunicazione();

		c.setTipo(Utility.TIPO_COMUNICAZIONE_MAIL);
		c.setMittente(m.getMittente());
		c.setOggetto(m.getOggetto());
		c.setDataInvio(new SimpleDateFormat("dd/MM/yyyy").parse(m.getDataInvio()));
		c.setTesto(m.getTesto());
		c.setTipoAmbito(Utility.AMBITO_INSIDER_LIST);

		Optional<Template> t = templateDao.findById(3);

		if (t != null) {

			Template tt = t.get();
			c.setTemplate(tt);

		} else
			c.setTemplate(null);

		SchedaPersona s = schedaPersonaDao.findPersonaByMail(m.getDestinatario());

		if (s != null) {
			c.getListaSchedaPersona().add(s);
			s.getListaComunicazione().add(c);
		}

		return c;

	}

	public Comunicazione setFieldLetteraToDb(LetteraCsv l, int counterTemplate)
			throws ParseException, NullPointerException, SQLException {

		Comunicazione c = new Comunicazione();

		c.setTipo(Utility.TIPO_COMUNICAZIONE_LETTERA);
		c.setOggetto("Registro delle persone che hanno accesso a informazioni privilegiate");
		c.setDataInvio(new SimpleDateFormat("dd/MM/yyyy").parse(l.getDataInvio()));
		c.setTesto(l.getTesto());
		c.setTipoAmbito(Utility.AMBITO_REGISTRO_MANAGER);

		Optional<Template> t = null;

		if (counterTemplate < 6) {

			t = templateDao.findById(1);

		} else if (counterTemplate > 6 && counterTemplate < 14) {
			t = templateDao.findById(3);
		} else {
			t = templateDao.findById(2);
		}

		if (t != null) {

			Template tt = t.get();
			c.setTemplate(tt);

		} else
			c.setTemplate(null);

		StringTokenizer str = new StringTokenizer(l.getDestinatario(), " ");

		String nome = null;
		String cognome = null;

		while (str.hasMoreElements()) {

			// Registro Manager
//			if (l.getDestinatario().equals("Tanya Louise Randall")) { 

			// Insider List
			if(l.getDestinatario().equals("Bruno Ivan Frigerio")) { 
				nome = str.nextToken() + " " + str.nextToken();
				cognome = str.nextToken();

			}

			// Registro Manager
//			else if (l.getDestinatario().equals("Massimo Arlotta Tarino")
//					|| l.getDestinatario().equals("Giorgio Leo Servidio")) { 

			// Insider List
			else if(l.getDestinatario().equals("Innocenzo De Sanctis")) { 

				nome = str.nextToken();
				cognome = str.nextToken() + " " + str.nextToken();

			}

			else {

				nome = str.nextToken();
				cognome = str.nextToken();

			}

		}

		// CONTROLLARE AMBITO NELLE QUERY 
		SchedaPersona s = schedaPersonaDao.findPersonaByNomeAndCognome(nome, cognome);

		if (s == null)
			s = schedaPersonaDao.findPersonaByNomeAndCognomeCancellato(nome, cognome);

		c.getListaSchedaPersona().add(s);
		s.getListaComunicazione().add(c);

		return c;

	}

	public void setFieldSchedaPersonaToDb(SchedaPersona s, SchedaPersonaCsv sp) throws ParseException {

		// s.setTipoAmbito(Utility.AMBITO_REGISTRO_MANAGER);
		s.setTipoAmbito(Utility.AMBITO_INSIDER_LIST);
		s.setTipoAccessoInformazione(Utility.TIPO_ACCESSO_PERMANENTE);

		s.setCodiceScheda(sp.getCodiceScheda());

		//s.setNome("Amir");
		s.setNome(sp.getNome());

		s.setCognome(sp.getCognome());

		if (sp.getCognomeNascita() != null)
			s.setCognomeNascita(sp.getCognomeNascita());

		s.setTipologiaSoggetto(sp.getTipologiaSoggetto());

		if (sp.getIdentificativoNazionale() != null)
			s.setIdentificativoNazionale(sp.getIdentificativoNazionale());

		if (sp.getRagioneSociale() != null)
			s.setRagioneSociale(sp.getRagioneSociale());

		s.setDataNascita(new SimpleDateFormat("dd/MM/yyyy").parse(sp.getDataNascita()));

		if (sp.getTelefonoProfessionale() != null)
			s.setTelefonoProfessionale(sp.getTelefonoProfessionale());

		if (sp.getTelefonoPrivato() != null)
			s.setTelefonoPrivato(sp.getTelefonoPrivato());

		if (sp.getIndirizzo() != null)
			s.setIndirizzo(sp.getIndirizzo());

		if (sp.getCap() != null)
			s.setCap(sp.getCap());

		if (sp.getLocalita() != null)
			s.setLocalita(sp.getLocalita());

		if (sp.getProvincia() != null)
			s.setProvincia(sp.getProvincia());

		if (sp.getNazione() != null)
			s.setNazione(sp.getNazione());

		s.setNomeIndirizzoImpresa("Sara Assicurazioni - Via Po, 20 – 00198 - Roma");

		try {

			Societa societaDb = societaDao.findSocietaByNome("Sara Assicurazioni");

			if (societaDb != null)
				s.setSocieta(societaDb);

		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

		s.setCodiceFiscale(sp.getCodiceFiscale());

		if (sp.getEmail() != null)
			s.setEmail(sp.getEmail());

		s.setRuolo(sp.getRuolo());

		s.setMotivoIscrizione(sp.getMotivoIscrizione());

		if (sp.getMotivoUltimaVariazione() != null)
			s.setMotivoUltimaVariazione(sp.getMotivoUltimaVariazione());

		s.setDescrizioneInformazione(sp.getDescrizioneInformazione());

		if (("Versioning Definitivo").equals(sp.getStato()))
			s.setStato(Utility.STATO_DEFINITIVO);

		else
			s.setStato(sp.getStato());

		s.setDataIscrizione(new SimpleDateFormat("dd/MM/yyyy").parse(sp.getDataIscrizione()));

		// s.setDataInserimento(new SimpleDateFormat("dd/MM/yyyy").parse(sp.getDataIscrizione()));

		if (sp.getDataCancellazione() != null)
			s.setDataCancellazione(new SimpleDateFormat("dd/MM/yyyy").parse(sp.getDataCancellazione()));

		if (sp.getDataUltimoAggiornamento() != null)
			s.setDataUltimoAggiornamento(new SimpleDateFormat("dd/MM/yyyy").parse(sp.getDataUltimoAggiornamento()));

		s.setAutore(sp.getAutore());

		if (sp.getManagerRiferimento() != null)
			s.setManagerRiferimento(sp.getManagerRiferimento());

		if (sp.getPartitaIva() != null)
			s.setPartitaIva(sp.getPartitaIva());

	}

	public void setFieldVersioning(SchedaPersona s, SchedaPersonaCsv sp, String datiModificati)
			throws ParseException {

		Versioning v = new Versioning();

//		if (sp.getDatiModificati() != null)
//			sb.append(sp.getDatiModificati());

		if (datiModificati != null) {

			v.setDatiModificati(datiModificati);

			v.setVersione(Integer.parseInt(String.valueOf(sp.getCodiceScheda().charAt(10))));

		} else {

			v.setDatiModificati("Prima Creazione");

			v.setVersione(1);

		}

		// REGISTRO MANAGER
		// v.setDataCreazione(new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(sp.getDataUltimoAggiornamento()));

		// INSIDER LIST
		v.setDataCreazione(new SimpleDateFormat("dd/MM/yyyy").parse(sp.getDataUltimoAggiornamento()));

		v.setSchedaJson(genericMapper.javaToJson(s));

		v.setSchedaPersona(s);

		s.getVersioningList().add(v);

	}

	public List<SchedaPersonaCsv> loadPersonList(List<SchedaPersonaCsv> listaSchedaPersonaCsv, int counter) {

		List<SchedaPersonaCsv> personListCsv = new ArrayList<SchedaPersonaCsv>();

		String codiceFiscale = null;

		for (int i = counter; i < listaSchedaPersonaCsv.size(); i++) {

			codiceFiscale = listaSchedaPersonaCsv.get(i).getCodiceFiscale();

			if ((i + 1) <= (listaSchedaPersonaCsv.size() - 1)) {

				if (listaSchedaPersonaCsv.get(i + 1).getCodiceFiscale().equals(codiceFiscale)) {

					personListCsv.add(listaSchedaPersonaCsv.get(i));

					System.out.println("Elemento inserito: " + listaSchedaPersonaCsv.get(i).getCodiceFiscale());

				} else {

					personListCsv.add(listaSchedaPersonaCsv.get(i));

					System.out.println("Elemento finale inserito: " + listaSchedaPersonaCsv.get(i).getCodiceFiscale());

					break;
				}

			} else {

				personListCsv.add(listaSchedaPersonaCsv.get(i));

				System.out.println("Elemento finale inserito: " + listaSchedaPersonaCsv.get(i).getCodiceFiscale());

				break;

			}

		}

		return personListCsv;

	}

	public boolean controlPersonRm(String codiceScheda) {

		boolean check = false;

		String[] arrayCode = { "A-000034-V1", "A-000035-V1", "A-000036-V1", "A-000037-V1", "A-000038-V1", "A-000039-V1",
				"A-000040-V1", "A-000041-V1", "A-000042-V1", "A-000043-V1", "A-000047-V1", "A-000048-V1", "A-000054-V1",
				"A-000056-V1", "A-000058-V1", "A-000060-V1", "A-000062-V1", "A-000066-V1", "A-000069-V1", "A-000073-V1",
				"A-000074-V1", "A-000077-V1", "A-000078-V1", "A-000080-V1", "A-000090-V1", "A-000091-V1", "A-000092-V1",
				"A-000093-V1", "A-000103-V1", "A-000104-V1", "A-000105-V1", "A-000106-V1", "A-000107-V1", "A-000108-V1",
				"A-000109-V1", "A-000110-V1", "A-000120-V1", "A-000123-V1", "A-000126-V1", "A-000129-V1", "A-000129-V2",
				"A-000132-V1", "A-000136-V1", "A-000138-V1", "A-000142-V1", "A-000143-V1", "S-000001-V1", "S-000004-V1",
				"S-000006-V1", "S-000008-V1", "S-000010-V1", "S-000011-V1", "S-000012-V1", "S-000013-V1", "S-000014-V1",
				"S-000016-V1", "S-000017-V1", "S-000018-V1", "S-000049-V1", "S-000052-V1", "S-000099-V1", "S-000128-V1",
				"S-000140-V1", "V-000024-V1", "V-000027-V1", "V-000028-V1", "V-000030-V1", "V-000046-V1", "V-000055-V1",
				"V-000057-V1", "V-000063-V1", "V-000072-V1", "V-000115-V1", "V-000121-V1", "V-000121-V2", "V-000130-V1",
				"V-000141-V1" };

		for (String c : arrayCode) {

			if (c.equals(codiceScheda)) {
				check = true;
				break;
			}

		}

		return check;

	}

}
