package it.sara.registroManager;

import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@ComponentScan("it.sara")
@EnableScheduling
public class RegistroManagerApplication extends SpringBootServletInitializer{

	public static void main(String[] args) throws SQLException {
		
		SpringApplication.run(RegistroManagerApplication.class, args);

	}
	
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
						
        return application.sources(RegistroManagerApplication.class);
    }

}