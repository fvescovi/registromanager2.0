package it.sara.registroManager.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.registroManager.entity.SchedaPersona;

public interface SchedaPersonaDao extends CrudRepository<SchedaPersona, Integer> {

	@Query("select c.listaSchedaPersona from Comunicazione c where c.id=:id")
	Set<SchedaPersona> findComunicazioneById(@Param(value = "id") Integer id)
			throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.stato='Definitivo' and sp.tipoAmbito='R' order by sp.cognome")
	List<SchedaPersona> findPersonaDefinitivaRM() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.stato='Cancellato' and sp.tipoAmbito='R' order by sp.cognome")
	List<SchedaPersona> findPersonaCancellataRM() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.stato='Bozza' and sp.tipoAmbito='R' order by sp.cognome")
	List<SchedaPersona> findPersonaBozzaRM() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.stato='Definitivo' and sp.tipoAmbito='I' order by sp.cognome")
	List<SchedaPersona> findPersonaDefinitivaIL() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.stato='Cancellato' and sp.tipoAmbito='I' order by sp.cognome")
	List<SchedaPersona> findPersonaCancellataIL() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.stato='Bozza' and sp.tipoAmbito='I' order by sp.cognome")
	List<SchedaPersona> findPersonaBozzaIL() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='R' order by sp.cognome")
	List<SchedaPersona> findPersonaRM() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='I' order by sp.cognome")
	List<SchedaPersona> findPersonaIL() throws SQLException, ParseException, NullPointerException;
	
	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='R' order by sp.cognome")
	List<SchedaPersona> findPersonaRMCognome() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='I' order by sp.cognome")
	List<SchedaPersona> findPersonaILCognome() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp inner join Societa so on sp.societa=so.id where so.nome=:nomeSocieta and sp.tipoAmbito=:ambito")
	List<SchedaPersona> findSchedaPersonaBySocieta(@Param("nomeSocieta") String nomeSocieta,
			@Param("ambito") String ambito) throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='R' order by sp.dataUltimoAggiornamento DESC")
	List<SchedaPersona> findPersonaByDataUltAggRM() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='I' order by sp.dataUltimoAggiornamento DESC")
	List<SchedaPersona> findPersonaByDataUltAggIL() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAccessoInformazione='Limitato' order by sp.cognome")
	List<SchedaPersona> findPersonaAccInfLim() throws SQLException, ParseException, NullPointerException;

	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='I' and (sp.tipoAccessoInformazione is null or sp.tipoAccessoInformazione='Permanente') order by sp.cognome")
	List<SchedaPersona> findPersonaAccInfPerm() throws SQLException, ParseException, NullPointerException;

	@Query("select ai.schedaPersona from AccessoInformazione ai where ai.dataCessazioneInformazione is null and ai.schedaInformazione.idSchedaInformazione=:id order by ai.schedaPersona.cognome")
	List<SchedaPersona> findSpByInformazione(@Param("id") Integer id)
			throws SQLException, ParseException, NullPointerException;

	@Query("select ai.schedaPersona from AccessoInformazione ai where ai.dataCessazioneInformazione is null and ai.schedaInformazione.idSchedaInformazione=:id")
	List<SchedaPersona> findSpByInformazioneCess(@Param("id") Integer id)
			throws SQLException, ParseException, NullPointerException;

	@Query("select ai.schedaPersona from AccessoInformazione ai where ai.dataCessazioneInformazione is null and ai.schedaInformazione.idSchedaInformazione=:id order by ai.schedaPersona.cognome")
	Page<SchedaPersona> getPageSchedaPersona(@Param("id") Integer id, Pageable p)
			throws SQLException, ParseException, NullPointerException;

	@Query("select count(*) from SchedaPersona sp where sp.tipoAmbito='I' and sp.stato<>'Bozza'")
	Integer countPersonaI() throws SQLException, NullPointerException;

	@Query("select count(*) from SchedaPersona sp where sp.tipoAmbito='R' and sp.stato<>'Bozza'")
	Integer countPersonaR() throws SQLException, NullPointerException;

	@Query("select count(*) from SchedaPersona sp where sp.stato<>'Bozza'")
	Integer countPersonDb() throws SQLException, NullPointerException;

	// QUERY PER CSV

	// IMPORT EMAIL
	@Query("select sp from SchedaPersona sp where sp.tipoAmbito='I' and sp.email=:email")
	SchedaPersona findPersonaByMail(@Param("email") String email) throws SQLException, NullPointerException;

	// IMPORT LETTERE (CAMBIARE AMBITO IN BASE ALL'IMPORT DA FARE)
	@Query("select sp from SchedaPersona sp where sp.nome=:nome and sp.cognome=:cognome and sp.tipoAmbito='I' and sp.stato='Definitivo'")
	SchedaPersona findPersonaByNomeAndCognome(@Param("nome") String nome, @Param("cognome") String cognome)
			throws SQLException, NullPointerException;

	// IMPORT LETTERE PERSONE CANCELLATE (CAMBIARE AMBITO IN BASE ALL'IMPORT DA
	// FARE)
	@Query("select sp from SchedaPersona sp where sp.nome=:nome and sp.cognome=:cognome and sp.tipoAmbito='I' and sp.stato='Cancellato'")
	SchedaPersona findPersonaByNomeAndCognomeCancellato(@Param("nome") String nome, @Param("cognome") String cognome)
			throws SQLException, NullPointerException;

}
