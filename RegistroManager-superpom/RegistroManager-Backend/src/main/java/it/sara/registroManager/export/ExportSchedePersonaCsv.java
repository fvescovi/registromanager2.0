package it.sara.registroManager.export;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.utility.Utility;

@Component("ExportSchedePersonaCsv")
public class ExportSchedePersonaCsv extends AbstractExportSchedaPersona {

	public static String[] columns = { "STATO", "NOME", "COGNOME", "CODICE SCHEDA", "SOCIETÁ", "TIPOLOGIA", "RUOLO",
			"DATA ISCRIZIONE", "" };

	@Override
	public void creazioneFoglioCsv(List<SchedaPersona> listaSchedaPersona, ServletOutputStream fileOut, String ambito)
			throws SQLException, ParseException, InvalidFormatException, IOException {

		if ((Utility.AMBITO_REGISTRO_MANAGER).equals(ambito))
			columns[7] = "DATA INSERIMENTO";

		boolean checkStato = false;

		for (SchedaPersona s : listaSchedaPersona) {
			if ((Utility.STATO_CANCELLATO).equals(s.getStato())) {
				checkStato = true;
				break;
			}
		}

		if (checkStato) {
			columns[8] = "DATA CANCELLAZIONE";
		}

		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("ElencoSchedePersone");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);

		if (checkStato) {
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(headerCellStyle);
			}
		} else {
			for (int i = 0; i <= 7; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(headerCellStyle);
			}
		}

		int rowNum = 1;

		for (SchedaPersona s : listaSchedaPersona) {

			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue(s.getStato());
			row.createCell(1).setCellValue(s.getNome());
			row.createCell(2).setCellValue(s.getCognome());

			if (s.getCodiceScheda() != null)
				row.createCell(3).setCellValue(s.getCodiceScheda());

			else
				row.createCell(3).setCellValue("");

			row.createCell(4).setCellValue(s.getSocieta().getNome());
			row.createCell(5).setCellValue(s.getTipologiaSoggetto());
			row.createCell(6).setCellValue(s.getRuolo());

			if ((Utility.AMBITO_REGISTRO_MANAGER).equals(ambito) && s.getDataInserimento() != null)
				row.createCell(7).setCellValue(Utility.formatDate(s.getDataInserimento()));

			else if ((Utility.AMBITO_INSIDER_LIST).equals(ambito) && s.getDataIscrizione() != null)
				row.createCell(7).setCellValue(Utility.formatDate(s.getDataIscrizione()));

			else
				row.createCell(7).setCellValue("");

			if (checkStato && (Utility.STATO_CANCELLATO).equals(s.getStato()))
				row.createCell(8).setCellValue(Utility.formatDate(s.getDataCancellazione()));

		}

		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		fileOut.flush();

		workbook.write(fileOut);
		workbook.close();

	}

}