package it.sara.registroManager.dao;

import java.sql.SQLException;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.registroManager.entity.ComunicazionePersona;
import it.sara.registroManager.entity.ComunicazionePersonaID;

public interface ComunicazionePersonaDao extends CrudRepository<ComunicazionePersona, ComunicazionePersonaID> {

	@Query("select cp from ComunicazionePersona cp where cp.pk.comunicazione.id=:id")
	ComunicazionePersona findComunicazionePersonaById(@Param(value = "id") Integer id)
			throws SQLException, NullPointerException;

}
