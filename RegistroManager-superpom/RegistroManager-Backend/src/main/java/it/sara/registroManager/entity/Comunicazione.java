package it.sara.registroManager.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "COMUNICAZIONE")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "listaSchedaPersona" })
public class Comunicazione implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6551704868814716442L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_COMUNICAZIONE")
	@SequenceGenerator(name = "SEQUENCE_COMUNICAZIONE", sequenceName = "SEQ_COMUNICAZIONE", allocationSize = 1)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "DATA_INVIO")
	private Date dataInvio;

	@Column(name = "TIPO")
	private String tipo;

	@Column(name = "TESTO")
	private String testo;

	@Column(name = "OGGETTO")
	private String oggetto;

	@Column(name = "MITTENTE")
	private String mittente;

	@Column(name = "TIPO_AMBITO")
	private String tipoAmbito;

	public String getTipoAmbito() {
		return tipoAmbito;
	}

	public void setTipoAmbito(String tipoAmbito) {
		this.tipoAmbito = tipoAmbito;
	}

	public String getMittente() {
		return mittente;
	}

	public void setMittente(String mittente) {
		this.mittente = mittente;
	}

	public String getOggetto() {
		return oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, targetEntity = Template.class)
	@JoinColumn(name = "ID_TEMPLATE")
	private Template template;

	@Embedded
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "COMUNICAZIONE_PERSONA", catalog = "REGISTRO", joinColumns = {
			@JoinColumn(name = "ID_COMUNICAZIONE", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "ID_SCHEDA_PERSONA", nullable = true, updatable = false) })
	private Set<SchedaPersona> listaSchedaPersona = new HashSet<SchedaPersona>();

	public Set<SchedaPersona> getListaSchedaPersona() {
		return listaSchedaPersona;
	}

	public void setListaSchedaPersona(Set<SchedaPersona> listaSchedaPersona) {
		this.listaSchedaPersona = listaSchedaPersona;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataInvio() {
		return dataInvio;
	}

	public void setDataInvio(Date dataInvio) {
		this.dataInvio = dataInvio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

}
