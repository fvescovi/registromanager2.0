package it.sara.registroManager.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import it.sara.registroManager.entity.Versioning;

public interface VersioningDao extends CrudRepository<Versioning, Integer> {

	@Query("select max(v.versione) from Versioning v where v.schedaPersona.idSchedaPersona=:id")
	Integer getMaxVersioneSchedaPersonaById(@Param("id") int id)
			throws SQLException, ParseException, NullPointerException;

	@Query("select v from Versioning v where v.schedaPersona.idSchedaPersona=:id and v.versione>=1 order by v.versione")
	Set<Versioning> getVersioniSchedaPersonaById(@Param("id") int id)
			throws SQLException, ParseException, NullPointerException;

	@Transactional
	@Modifying
	@Query("delete from Versioning v where v.schedaPersona.idSchedaPersona=:id")
	void deleteVersioniSchedaPersonaById(@Param("id") int id) throws SQLException, ParseException, NullPointerException;

}
