package it.sara.registroManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TEMPLATE")
public class Template implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4178229634189612284L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_TEMPLATE")
	@SequenceGenerator(name = "SEQUENCE_TEMPLATE", sequenceName = "SEQ_TEMPLATE", allocationSize = 1)
	@Column(name = "ID")
	private Integer id;

	@Column(name = "TESTO")
	private String testo;

	@Column(name = "NOME_TEMPLATE")
	private String nomeTemplate;

	@Column(name = "PERSONECC")
	private String personeCc;

	public String getPersoneCc() {
		return personeCc;
	}

	public void setPersoneCc(String personeCc) {
		this.personeCc = personeCc;
	}

	public String getNomeTemplate() {
		return nomeTemplate;
	}

	public void setNomeTemplate(String nomeTemplate) {
		this.nomeTemplate = nomeTemplate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

}
