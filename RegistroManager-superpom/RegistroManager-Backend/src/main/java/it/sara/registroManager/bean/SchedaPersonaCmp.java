package it.sara.registroManager.bean;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.utility.Utility;

@Component("SchedaPersonaCmp")
public class SchedaPersonaCmp {

	Map<String, String> oldSchedaPersonaMap = new HashMap<String, String>();
	Map<String, String> newSchedaPersonaMap = new HashMap<String, String>();

	Map<String, Date> oldSchedaPersonaMapDate = new HashMap<String, Date>();
	Map<String, Date> newSchedaPersonaMapDate = new HashMap<String, Date>();

	public void loadMaps(SchedaPersona newSchedaPersona, SchedaPersona schedaPersonaDb) {
		loadNewObjectSchedaPersonaMap(newSchedaPersona);
		loadSchedaPersonaDbMap(schedaPersonaDb);
		loadNewObjectSchedaPersonaMapDate(newSchedaPersona);
		loadSchedaPersonaDbMapDate(schedaPersonaDb);
	}

	public void loadSchedaPersonaDbMapDate(SchedaPersona schedaPersonaDb) {

		oldSchedaPersonaMapDate.put("Data di nascita", schedaPersonaDb.getDataNascita());

		if (schedaPersonaDb.getTipoAmbito().equals(Utility.AMBITO_INSIDER_LIST)
				&& schedaPersonaDb.getDataIscrizione() != null)
			oldSchedaPersonaMapDate.put("Data di iscrizione", schedaPersonaDb.getDataIscrizione());

		if (schedaPersonaDb.getTipoAmbito().equals(Utility.AMBITO_REGISTRO_MANAGER)
				&& schedaPersonaDb.getDataInserimento() != null)
			oldSchedaPersonaMapDate.put("Data di inserimento", schedaPersonaDb.getDataInserimento());

		if (schedaPersonaDb.getDataCancellazione() != null)
			oldSchedaPersonaMapDate.put("Data di cancellazione", schedaPersonaDb.getDataCancellazione());
	}

	public void loadNewObjectSchedaPersonaMapDate(SchedaPersona newSchedaPersona) {

		newSchedaPersonaMapDate.put("Data di nascita", newSchedaPersona.getDataNascita());

		if (newSchedaPersona.getTipoAmbito().equals(Utility.AMBITO_INSIDER_LIST)
				&& newSchedaPersona.getDataIscrizione() != null)
			newSchedaPersonaMapDate.put("Data di iscrizione", newSchedaPersona.getDataIscrizione());

		if (newSchedaPersona.getTipoAmbito().equals(Utility.AMBITO_REGISTRO_MANAGER)
				&& newSchedaPersona.getDataInserimento() != null)
			newSchedaPersonaMapDate.put("Data di inserimento", newSchedaPersona.getDataInserimento());

		if (newSchedaPersona.getDataCancellazione() != null)
			newSchedaPersonaMapDate.put("Data di cancellazione", newSchedaPersona.getDataCancellazione());
	}

	public void loadSchedaPersonaDbMap(SchedaPersona schedaPersonaDb) {
		oldSchedaPersonaMap.put("Codice Fiscale", schedaPersonaDb.getCodiceFiscale());
		oldSchedaPersonaMap.put("Nome", schedaPersonaDb.getNome());
		oldSchedaPersonaMap.put("Cognome", schedaPersonaDb.getCognome());
		oldSchedaPersonaMap.put("Cognome Nascita", schedaPersonaDb.getCognomeNascita());
		oldSchedaPersonaMap.put("Telefono Professionale", schedaPersonaDb.getTelefonoProfessionale());
		oldSchedaPersonaMap.put("Telefono Privato", schedaPersonaDb.getTelefonoPrivato());
		oldSchedaPersonaMap.put("Indirizzo", schedaPersonaDb.getIndirizzo());
		oldSchedaPersonaMap.put("Cap", schedaPersonaDb.getCap());
		oldSchedaPersonaMap.put("Localita", schedaPersonaDb.getLocalita());
		oldSchedaPersonaMap.put("Provincia", schedaPersonaDb.getProvincia());
		oldSchedaPersonaMap.put("Nazione", schedaPersonaDb.getNazione());
		oldSchedaPersonaMap.put("Nome Indirizzo Impresa", schedaPersonaDb.getNomeIndirizzoImpresa());
		oldSchedaPersonaMap.put("Identificativo Nazionale", schedaPersonaDb.getIdentificativoNazionale());
		oldSchedaPersonaMap.put("Email", schedaPersonaDb.getEmail());
		oldSchedaPersonaMap.put("Ruolo", schedaPersonaDb.getRuolo());
		oldSchedaPersonaMap.put("Ragione Sociale", schedaPersonaDb.getRagioneSociale());
		oldSchedaPersonaMap.put("Partita Iva", schedaPersonaDb.getPartitaIva());
		oldSchedaPersonaMap.put("Tipologia Soggetto", schedaPersonaDb.getTipologiaSoggetto());
		oldSchedaPersonaMap.put("Manager Riferimento", schedaPersonaDb.getManagerRiferimento());
		oldSchedaPersonaMap.put("Motivo Iscrizione", schedaPersonaDb.getMotivoIscrizione());
		oldSchedaPersonaMap.put("Societa", schedaPersonaDb.getSocieta().getNome());
		oldSchedaPersonaMap.put("Tipo Ambito", schedaPersonaDb.getTipoAmbito());
		oldSchedaPersonaMap.put("Tipo Accesso Informazione", schedaPersonaDb.getTipoAccessoInformazione());
	}

	public void loadNewObjectSchedaPersonaMap(SchedaPersona newObjectSchedaPersona) {
		newSchedaPersonaMap.put("Codice Fiscale", newObjectSchedaPersona.getCodiceFiscale());
		newSchedaPersonaMap.put("Nome", newObjectSchedaPersona.getNome());
		newSchedaPersonaMap.put("Cognome", newObjectSchedaPersona.getCognome());
		newSchedaPersonaMap.put("Cognome Nascita", newObjectSchedaPersona.getCognomeNascita());
		newSchedaPersonaMap.put("Telefono Professionale", newObjectSchedaPersona.getTelefonoProfessionale());
		newSchedaPersonaMap.put("Telefono Privato", newObjectSchedaPersona.getTelefonoPrivato());
		newSchedaPersonaMap.put("Indirizzo", newObjectSchedaPersona.getIndirizzo());
		newSchedaPersonaMap.put("Cap", newObjectSchedaPersona.getCap());
		newSchedaPersonaMap.put("Localita", newObjectSchedaPersona.getLocalita());
		newSchedaPersonaMap.put("Provincia", newObjectSchedaPersona.getProvincia());
		newSchedaPersonaMap.put("Nazione", newObjectSchedaPersona.getNazione());
		newSchedaPersonaMap.put("Nome Indirizzo Impresa", newObjectSchedaPersona.getNomeIndirizzoImpresa());
		newSchedaPersonaMap.put("Identificativo Nazionale", newObjectSchedaPersona.getIdentificativoNazionale());
		newSchedaPersonaMap.put("Email", newObjectSchedaPersona.getEmail());
		newSchedaPersonaMap.put("Ruolo", newObjectSchedaPersona.getRuolo());
		newSchedaPersonaMap.put("Ragione Sociale", newObjectSchedaPersona.getRagioneSociale());
		newSchedaPersonaMap.put("Partita Iva", newObjectSchedaPersona.getPartitaIva());
		newSchedaPersonaMap.put("Tipologia Soggetto", newObjectSchedaPersona.getTipologiaSoggetto());
		newSchedaPersonaMap.put("Manager Riferimento", newObjectSchedaPersona.getManagerRiferimento());
		newSchedaPersonaMap.put("Motivo Iscrizione", newObjectSchedaPersona.getMotivoIscrizione());
		newSchedaPersonaMap.put("Societa", newObjectSchedaPersona.getSocieta().getNome());
		newSchedaPersonaMap.put("Tipo Ambito", newObjectSchedaPersona.getTipoAmbito());
		newSchedaPersonaMap.put("Tipo Accesso Informazione", newObjectSchedaPersona.getTipoAccessoInformazione());
	}

	private boolean compare(String val1, String val2) {
		boolean ret = true;

		if (val1 == null && val2 == null)
			ret = true;
		else {
			if (val1 != null) {
				if (val1.equals(val2))
					ret = true;
				else
					ret = false;
			} else
				ret = false;

		}
		return ret;
	}

	public String getDatiModificati() throws ParseException {

		StringBuffer datiModificati = new StringBuffer();

		for (Entry<String, String> entry : oldSchedaPersonaMap.entrySet()) {
			for (Entry<String, String> entry2 : newSchedaPersonaMap.entrySet()) {
				if (entry.getKey().equals(entry2.getKey())) {
					if (!this.compare(entry.getValue(), entry2.getValue())) {
						System.out.println("### KEY: " + entry2.getKey());
						datiModificati.append(entry2.getKey() + " , ");
					}
				}
			}
		}

		for (Entry<String, Date> entry : oldSchedaPersonaMapDate.entrySet()) {
			for (Entry<String, Date> entry2 : newSchedaPersonaMapDate.entrySet()) {
				if (entry.getKey().equals(entry2.getKey())) {
					if (!Utility.formatDate(entry.getValue()).equals(Utility.formatDate(entry2.getValue()))) {
						System.out.println("### KEY: " + entry2.getKey());
						datiModificati.append(entry2.getKey() + " , ");
					}
				}
			}
		}

		if (datiModificati.length() != 0)
			return datiModificati.substring(0, datiModificati.length() - 3);

		else
			return datiModificati.substring(datiModificati.length());
	}

}