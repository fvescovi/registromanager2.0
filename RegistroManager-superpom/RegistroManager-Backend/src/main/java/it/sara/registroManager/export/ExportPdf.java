package it.sara.registroManager.export;

import java.text.ParseException;

import javax.servlet.ServletOutputStream;

import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.utility.Utility;

@Component("ExportPdf")
public class ExportPdf {

	public void creazioneLettera(String template, ServletOutputStream fileOut) throws ParseException {

		Document document = new Document();

		try {

			PdfWriter writer = PdfWriter.getInstance(document, fileOut);

			document.open();

			document.add(new Paragraph(template));

			document.close();

			writer.close();

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void stampaPersona(SchedaPersona schedaPersona, ServletOutputStream fileOut) throws ParseException {

		Document document = new Document();

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

		try {

			PdfWriter writer = PdfWriter.getInstance(document, fileOut);

			document.open();

			document.add(new Paragraph(schedaPersona.getNome() + " " + schedaPersona.getCognome(), boldFont));
			document.add(new Paragraph("\n"));

			if (schedaPersona.getTipoAmbito().equals(Utility.AMBITO_INSIDER_LIST)) {
				
				document.add(new Paragraph("Tipologia: " + schedaPersona.getStato()));
				document.add(new Paragraph("Registro di appartenenza: " + "Insider List"));
				document.add(new Paragraph("Nome: " + schedaPersona.getNome()));
				document.add(new Paragraph("Cognome: " + schedaPersona.getCognome()));
				document.add(new Paragraph("Codice Fiscale: " + schedaPersona.getCodiceFiscale().toUpperCase()));
				document.add(new Paragraph("Descrizione informazione: " + schedaPersona.getDescrizioneInformazione()));

				if (schedaPersona.getCognomeNascita() != null)
					document.add(new Paragraph("Cognome di nascita: " + schedaPersona.getCognomeNascita()));

				if (schedaPersona.getTelefonoProfessionale() != null)
					document.add(new Paragraph("Telefono professionale: " + schedaPersona.getTelefonoProfessionale()));

				if (schedaPersona.getTelefonoPrivato() != null)
					document.add(new Paragraph("Telefono privato: " + schedaPersona.getTelefonoPrivato()));

				if (schedaPersona.getIndirizzo() != null)
					document.add(new Paragraph("Indirizzo: " + schedaPersona.getIndirizzo()));

				if (schedaPersona.getCap() != null)
					document.add(new Paragraph("CAP: " + schedaPersona.getCap()));

				if (schedaPersona.getLocalita() != null)
					document.add(new Paragraph("Località: " + schedaPersona.getLocalita()));

				if (schedaPersona.getProvincia() != null)
					document.add(new Paragraph("Provincia: " + schedaPersona.getProvincia()));

				if (schedaPersona.getNazione() != null)
					document.add(new Paragraph("Nazione: " + schedaPersona.getNazione()));

				document.add(new Paragraph("Nome Indirizzo Impresa: " + schedaPersona.getNomeIndirizzoImpresa()));

				if (schedaPersona.getIdentificativoNazionale() != null)
					document.add(
							new Paragraph("Identificativo Nazionale: " + schedaPersona.getIdentificativoNazionale()));

				document.add(new Paragraph("Data di nascita: " + Utility.formatDate(schedaPersona.getDataNascita())));

				if (schedaPersona.getEmail() != null)
					document.add(new Paragraph("Email: " + schedaPersona.getEmail()));

				document.add(new Paragraph("Ruolo: " + schedaPersona.getRuolo()));

				if (schedaPersona.getRagioneSociale() != null)
					document.add(new Paragraph("Ragione Sociale: " + schedaPersona.getRagioneSociale()));

				if (schedaPersona.getPartitaIva() != null)
					document.add(new Paragraph("Partita IVA: " + schedaPersona.getPartitaIva()));

				document.add(new Paragraph("Tipoligia Soggetto: " + schedaPersona.getTipologiaSoggetto()));

				if (schedaPersona.getCodiceScheda() != null)
					document.add(new Paragraph("Codice scheda: " + schedaPersona.getCodiceScheda()));

				document.add(
						new Paragraph("Data iscrizione: " + Utility.formatDate(schedaPersona.getDataIscrizione())));

				document.add(new Paragraph("Motivo iscrizione: " + schedaPersona.getMotivoIscrizione()));

				if (!schedaPersona.getMotivoUltimaVariazione().equals(""))
					document.add(
							new Paragraph("Motivo ultima variazione: " + schedaPersona.getMotivoUltimaVariazione()));

				if(schedaPersona.getDataUltimoAggiornamento() != null)
				document.add(new Paragraph("Data ultimo aggiornamento: "
						+ Utility.formatDate(schedaPersona.getDataUltimoAggiornamento())));

				if (schedaPersona.getDataCancellazione() != null)
					document.add(new Paragraph(
							"Data cancellazione: " + Utility.formatDate(schedaPersona.getDataCancellazione())));

				document.add(new Paragraph("Autore: " + schedaPersona.getAutore()));

				if (schedaPersona.getTipoAccessoInformazione() != null)
					document.add(
							new Paragraph("Tipo Accesso Informazione: " + schedaPersona.getTipoAccessoInformazione()));

				document.add(new Paragraph("Società: " + schedaPersona.getSocieta().getNome()));
				
			} else {
				document.add(new Paragraph("Tipologia: " + schedaPersona.getStato()));
				document.add(new Paragraph("Registro di appartenenza: " + "Registro Manager"));
				document.add(new Paragraph("Nome: " + schedaPersona.getNome()));
				document.add(new Paragraph("Cognome: " + schedaPersona.getCognome()));
				document.add(new Paragraph("Codice Fiscale: " + schedaPersona.getCodiceFiscale().toUpperCase()));
				document.add(new Paragraph("Data di nascita: " + Utility.formatDate(schedaPersona.getDataNascita())));
				document.add(new Paragraph(
						"Data di inserimento: " + Utility.formatDate(schedaPersona.getDataInserimento())));

				if (schedaPersona.getEmail() != null)
					document.add(new Paragraph("Email: " + schedaPersona.getEmail()));

				document.add(new Paragraph("Ruolo: " + schedaPersona.getRuolo()));

				if (schedaPersona.getRagioneSociale() != null)
					document.add(new Paragraph("Ragione Sociale: " + schedaPersona.getRagioneSociale()));

				if (schedaPersona.getPartitaIva() != null)
					document.add(new Paragraph("Partita IVA: " + schedaPersona.getPartitaIva()));

				document.add(new Paragraph("Tipoligia Soggetto: " + schedaPersona.getTipologiaSoggetto()));

				if (!schedaPersona.getStato().equals("Bozza") && schedaPersona.getCodiceScheda() != null)
					document.add(new Paragraph("Codice scheda: " + schedaPersona.getCodiceScheda()));

				if (!schedaPersona.getMotivoUltimaVariazione().equals(""))
					document.add(
							new Paragraph("Motivo ultima variazione: " + schedaPersona.getMotivoUltimaVariazione()));

				document.add(new Paragraph("Data ultimo aggiornamento: "
						+ Utility.formatDate(schedaPersona.getDataUltimoAggiornamento())));

				if (schedaPersona.getManagerRiferimento() != null)
					document.add(new Paragraph("Manager di riferimento: " + schedaPersona.getManagerRiferimento()));

				if (schedaPersona.getDataCancellazione() != null)
					document.add(new Paragraph(
							"Data cancellazione: " + Utility.formatDate(schedaPersona.getDataCancellazione())));

				document.add(new Paragraph("Autore: " + schedaPersona.getAutore()));

				if (schedaPersona.getTipoAccessoInformazione() != null)
					document.add(
							new Paragraph("Tipo Accesso Informazione: " + schedaPersona.getTipoAccessoInformazione()));

				document.add(new Paragraph("Società: " + schedaPersona.getSocieta().getNome()));
			}

			document.close();

			writer.close();

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
