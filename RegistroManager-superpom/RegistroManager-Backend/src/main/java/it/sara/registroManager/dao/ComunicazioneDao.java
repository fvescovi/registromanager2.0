package it.sara.registroManager.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.registroManager.entity.Comunicazione;

public interface ComunicazioneDao extends CrudRepository<Comunicazione, Integer> {
	
	@Query("select c from Comunicazione c where c.tipo='M' and c.tipoAmbito=:ambito order by c.dataInvio")
	List<Comunicazione> findListaEmail(@Param(value = "ambito") String ambito) throws SQLException, NullPointerException;
	
	@Query("select c from Comunicazione c where c.tipo='L' and c.tipoAmbito=:ambito order by c.dataInvio")
	List<Comunicazione> findListaLettera(@Param(value = "ambito") String ambito) throws SQLException, NullPointerException;

}
