package it.sara.registroManager.business;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import it.sara.registroManager.security.IGestioneUtente;

@Component("gestioneUtente")
public class GestioneUtente implements IGestioneUtente {

	@Value("${Administrator}")
	public String administrator;

	@Value("${Responsabili}")
	public String responsabili;

	@Value("${Visualizzatori}")
	public String visualizzatori;

	@Value("${Linker}")
	public String linker;

	@Value("${Validatori}")
	public String validatori;

	@Value("${ValidatoriGenerali}")
	public String validatoriGenerali;


    public List<String> getRuoliFromGruppi(Collection<? extends GrantedAuthority> elencoGruppi) {
    	if (elencoGruppi==null)return null;
    	List<String> risultato = new LinkedList<String>();
    	for (GrantedAuthority ga : elencoGruppi) {
    		if (ga.getAuthority().equals("ROLE_"+administrator))risultato.add("Administrator");
    		if (ga.getAuthority().equals("ROLE_"+responsabili))risultato.add("Responsabili");
    		if (ga.getAuthority().equals("ROLE_"+visualizzatori))risultato.add("Visualizzatori");
    	}
    	return risultato;
    }

    public List<String> getRuoliEsterni(Collection<? extends GrantedAuthority> elencoGruppi) {
    	if (elencoGruppi==null)return null;
    	List<String> risultato = new LinkedList<String>();
    	for (GrantedAuthority ga : elencoGruppi) {
    		if (ga.getAuthority().indexOf("ROLE_")>=0)risultato.add(ga.getAuthority().substring(5));
    	}
    	return risultato;
    }

    
    public boolean hasRole(String nomeRuolo, Collection<? extends GrantedAuthority> elencoGruppi) {
    	if (elencoGruppi==null || nomeRuolo == null)return false;

    	for (GrantedAuthority ga : elencoGruppi) {
    		if (ga.getAuthority().equals("ROLE_"+administrator) && nomeRuolo.equals("Administrator")) return true;
    		if (ga.getAuthority().equals("ROLE_"+responsabili) && nomeRuolo.equals("Responsabili")) return true;
    		if (ga.getAuthority().equals("ROLE_"+visualizzatori) && nomeRuolo.equals("Visualizzatori")) return true;
    		if (ga.getAuthority().equals("ROLE_"+linker) && nomeRuolo.equals("Linker")) return true;
    		if (ga.getAuthority().equals("ROLE_"+validatori) && nomeRuolo.equals("Validatori")) return true;
    		if (ga.getAuthority().equals("ROLE_"+validatoriGenerali) && nomeRuolo.equals("ValidatoriGenerali")) return true;

    	}
    	return false;
    }
    
	
	@Override
	public String getAdministrator() {
		return administrator;
	}

}
