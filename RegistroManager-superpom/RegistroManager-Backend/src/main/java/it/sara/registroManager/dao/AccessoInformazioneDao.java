package it.sara.registroManager.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.registroManager.entity.AccessoInformazione;

public interface AccessoInformazioneDao extends CrudRepository<AccessoInformazione, Integer> {

	@Query("select sp.listaAccessoInformazione from SchedaPersona sp where sp.idSchedaPersona=:id")
	Set<AccessoInformazione> findAccessoInfoByIdPersona(@Param(value = "id") int id)
			throws SQLException, NullPointerException;

	@Query("select ai from AccessoInformazione ai where ai.schedaInformazione.idSchedaInformazione=:id order by ai.schedaPersona.cognome")
	Set<AccessoInformazione> findAccessoInfoByIdInfo(@Param(value = "id") int id)
			throws SQLException, NullPointerException;

	@Query("select s.listaAccessoInformazione from SchedaInformazione s where s.idSchedaInformazione > 1 and s.informazionePrivilegiata=:info")
	Set<AccessoInformazione> findAccessoInfoByNomeInfoLim(@Param(value = "info") String info)
			throws SQLException, NullPointerException;

	@Query("select ai from AccessoInformazione ai where ai.schedaPersona.tipoAccessoInformazione='Limitato' order by ai.schedaPersona.cognome")
	List<AccessoInformazione> findAccessoLimitato() throws SQLException, NullPointerException;

	@Query("select ai from AccessoInformazione ai where ai.schedaPersona.tipoAccessoInformazione='Permanente' order by ai.schedaPersona.cognome")
	List<AccessoInformazione> findAccessoPermanente() throws SQLException, NullPointerException;

	@Query("select ai from AccessoInformazione ai where ai.dataCessazioneInformazione is not null and ai.storico='S' and ai.schedaInformazione.idSchedaInformazione=1")
	List<AccessoInformazione> getAccessCessatoPerm() throws SQLException, NullPointerException;

	@Query("select ai from AccessoInformazione ai inner join SchedaPersona sp on ai.schedaPersona.idSchedaPersona=sp.idSchedaPersona where ai.schedaInformazione.idSchedaInformazione in (select s.idSchedaInformazione from SchedaInformazione s where s.informazionePrivilegiata=:info) and ai.schedaPersona.idSchedaPersona in (select sp.idSchedaPersona from SchedaPersona sp where sp.tipoAmbito='I') and ai.dataAccessoInformazione <= to_date(:endDate,'DD/MM/YYYY HH24:MI') and (ai.dataCessazioneInformazione is null or ai.dataCessazioneInformazione >= to_date(:startDate,'DD/MM/YYYY HH24:MI')) order by sp.cognome")
	Set<AccessoInformazione> getAccessBetweenDate(@Param(value = "info") String info,
			@Param(value = "startDate") String startDate, @Param(value = "endDate") String endDate)
			throws SQLException, NullPointerException;

}
