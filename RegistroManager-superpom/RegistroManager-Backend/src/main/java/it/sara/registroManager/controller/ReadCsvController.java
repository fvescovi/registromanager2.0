package it.sara.registroManager.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.bean.CsvToBeanBuilder;

import it.sara.registroManager.dao.ComunicazioneDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.dao.TemplateDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.Comunicazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.pojo.AccessoInformazioneCsv;
import it.sara.registroManager.pojo.LetteraCsv;
import it.sara.registroManager.pojo.MailCsv;
import it.sara.registroManager.pojo.SchedaPersonaCsv;
import it.sara.registroManager.utility.CsvManager;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class ReadCsvController {

	@Autowired
	ComunicazioneDao comunicazioneDao;

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	CsvManager csvManager;

	@Autowired
	TemplateDao templateDao;

	private static final String CSV_IMPORTNUOVAPERSONA_IL = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\import.csv";
	private static final String CSV_COMUNICAZIONE_LETTERA_RM = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\lettereRM.csv";
	private static final String CSV_SCHEDAPERSONA_RM = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\personeRM.csv";
	private static final String CSV_SCHEDAPERSONA_IL = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\personeIL.csv";
	private static final String CSV_COMUNICAZIONE_MAIL = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\mail.csv";
	private static final String CSV_COMUNICAZIONE_LETTERA_IL = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\lettere.csv";
	private static final String CSV_ACCESSOINFORMAZIONE = "C:\\Users\\a.magnante\\Desktop\\Documenti\\InsiderList\\informazioniIL.csv";

	@PostMapping(value = "/importSchedaPersona", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void importSchedaPersona() throws ParseException {
		
		System.out.println("Import scheda persona chiamato");

//		try {
//
//			List<SchedaPersonaCsv> listaSchedaPersonaCsv = new CsvToBeanBuilder<SchedaPersonaCsv>(
//					new FileReader(CSV_SCHEDAPERSONA_IL)).withType(SchedaPersonaCsv.class).withSkipLines(0).build()
//							.parse();
//
//			List<SchedaPersonaCsv> personListCsv = new ArrayList<SchedaPersonaCsv>();
//
//			int counter = 0;
//
//			// boolean check = false;
//
//			for (int i = counter; i < listaSchedaPersonaCsv.size(); i++) {
//
//				personListCsv = csvManager.loadPersonList(listaSchedaPersonaCsv, counter);
//
//				SchedaPersona s = new SchedaPersona();
//
//				// StringBuffer sb = new StringBuffer();
//
//				// int counterPersonList = 0;
//
//				for (int j = 0; j < personListCsv.size(); j++) {
//
//					// check = csvManager.controlPersonRm(personListCsv.get(j).getCodiceScheda());
//
//					// if (!check) {
//
//					csvManager.setFieldSchedaPersonaToDb(s, personListCsv.get(j));
//
//					schedaPersonaDao.save(s);
//
//					if (j == 0)
//						csvManager.setFieldVersioning(s, personListCsv.get(j), null);
//
//					// else if(counterPersonList <= personListCsv.size())
//					else
//						csvManager.setFieldVersioning(s, personListCsv.get(j),
//								personListCsv.get(j - 1).getDatiModificati());
//
//					schedaPersonaDao.save(s);
//
//				}
//
//				// }
//
//				// counterPersonList = counterPersonList + 1;
//
//				// sb = new StringBuffer();
//
//				counter = counter + personListCsv.size();
//
//				personListCsv = new ArrayList<SchedaPersonaCsv>();
//
//			}
//
//		} catch (NullPointerException | IOException e) {
//			e.printStackTrace();
//		}

	}

	@PostMapping(value = "/importMail", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void importMail()
			throws IllegalStateException, FileNotFoundException, ParseException, NullPointerException, SQLException {

		System.out.println("Import mail chiamato");

//		List<MailCsv> listaMailCsv = new CsvToBeanBuilder<MailCsv>(new FileReader(CSV_COMUNICAZIONE_MAIL))
//				.withType(MailCsv.class).build().parse();
//
//		for (MailCsv m : listaMailCsv) {
//
//			Comunicazione c = csvManager.setFieldMailToDb(m);
//
//			comunicazioneDao.save(c);
//
//			System.out.println("Mail inserita: " + c.getOggetto());
//
//		}

	}

	@PostMapping(value = "/importLettera", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void importLettera()
			throws IllegalStateException, FileNotFoundException, ParseException, NullPointerException, SQLException {

		System.out.println("Import lettera chiamato");
		
//		List<LetteraCsv> listaLetteraCsv = new CsvToBeanBuilder<LetteraCsv>(
//				new FileReader(CSV_COMUNICAZIONE_LETTERA_IL)).withType(LetteraCsv.class).withSkipLines(0).build()
//						.parse();
//
//		int counterTemplate = 1;
//
//		for (LetteraCsv l : listaLetteraCsv) {
//
//			Comunicazione c = csvManager.setFieldLetteraToDb(l, counterTemplate);
//
//			comunicazioneDao.save(c);
//
//			counterTemplate = counterTemplate + 1;
//
//			System.out.println("Lettera inserita: " + c.getOggetto());
//
//		}

	}

	@PostMapping(value = "/importAccessoInformazione", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void importAccessoInformazione()
			throws ParseException, IllegalStateException, FileNotFoundException, NullPointerException, SQLException {

		System.out.println("Import accesso informazione chiamato");

//		List<AccessoInformazioneCsv> listaAccessoInformazioneCsv = new CsvToBeanBuilder<AccessoInformazioneCsv>(
//				new FileReader(CSV_ACCESSOINFORMAZIONE)).withType(AccessoInformazioneCsv.class).build().parse();
//
//		for (AccessoInformazioneCsv a : listaAccessoInformazioneCsv) {
//
//			AccessoInformazione ai = csvManager.setAccessoInformazioneToDb(a);
//
//			schedaPersonaDao.save(ai.getSchedaPersona());
//
//			System.out.println("Accesso inserito con autore: " + a.getAutoreInserimento());
//
//		}

	}

}
