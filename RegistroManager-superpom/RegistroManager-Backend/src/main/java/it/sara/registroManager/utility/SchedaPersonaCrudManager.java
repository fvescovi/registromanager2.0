package it.sara.registroManager.utility;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.registroManager.bean.Riga;
import it.sara.registroManager.dao.AccessoInformazioneDao;
import it.sara.registroManager.dao.ContatoreSchedaDao;
import it.sara.registroManager.dao.SchedaInformazioneDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.dao.SocietaDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.ContatoreScheda;
import it.sara.registroManager.entity.SchedaInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.entity.Societa;

@Component("SchedaPersonaCrudManager")
public class SchedaPersonaCrudManager {

	@Autowired
	SocietaDao societaDao;

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	SchedaInformazioneDao schedaInformazioneDao;

	@Autowired
	AccessoInformazioneDao accessoInformazioneDao;

	@Autowired
	ContatoreSchedaDao contatoreSchedaDao;

	public SchedaPersona setFieldInSave(SchedaPersona schedaPersona, String username)
			throws ParseException, NullPointerException {

		schedaPersona.setAutore(username);
		schedaPersona.setDataUltimoAggiornamento(new Date());

		Optional<Societa> societa = societaDao.findById(schedaPersona.getSocieta().getId());

		schedaPersona.setSocieta(societa.get());

		return schedaPersona;

	}

	public SchedaPersona setFieldInDelete(SchedaPersona schedaPersona, String autore)
			throws ParseException, NullPointerException {

		schedaPersona.setAutore(autore);
		schedaPersona.setSocieta(null);

		return schedaPersona;

	}

	public void aggiungiInformazione(SchedaInformazione si, SchedaPersona sp, String ora, SimpleDateFormat sdf)
			throws ParseException {

		si.setOraAccessoInformazione(ora);

		si.setDataCessazioneInformazione(null);
		si.setOraCessazioneInformazione(null);

	}

	public void cessaInformazione(SchedaInformazione si, SchedaPersona sp, String ora) throws ParseException {

		si.setOraCessazioneInformazione(ora);

		if (sp.getTipoAccessoInformazione().equals(Utility.STATO_INFO_PERMANENTE)) {
			sp.setTipoAccessoInformazione(null);
		}

		schedaPersonaDao.save(sp);

	}

	public void addAccessInfo(SchedaPersona schedaPersona, SchedaInformazione schedaInformazione, String ora)
			throws ParseException, NullPointerException, SQLException {

		this.setAccessoInformazione(schedaPersona, schedaInformazione, ora);

		if (schedaInformazione.getStato().equals(Utility.STATO_INFO_PERMANENTE)) {

			schedaPersona.setTipoAccessoInformazione(Utility.TIPO_ACCESSO_PERMANENTE);

			SchedaInformazione s = new SchedaInformazione();

			s = schedaInformazioneDao.findAccessoPermanente();

			if (s == null) {

				Integer idSchedaInformazione = schedaInformazioneDao.findMaxIdSchedaInfo();

				idSchedaInformazione = idSchedaInformazione + 1;
				schedaInformazione.setIdSchedaInformazione(idSchedaInformazione);

			} else
				schedaInformazione.setIdSchedaInformazione(s.getIdSchedaInformazione());

		}

		schedaPersonaDao.save(schedaPersona);

	}

	@SuppressWarnings("deprecation")
	public AccessoInformazione setAccessoInformazione(SchedaPersona schedaPersona,
			SchedaInformazione schedaInformazione, String ora) {

		AccessoInformazione accessoInformazione = new AccessoInformazione();

		StringTokenizer str = new StringTokenizer(ora, ":");

		while (str.hasMoreElements()) {
			schedaInformazione.getDataAccessoInformazione().setHours(Integer.parseInt(str.nextToken()));
			schedaInformazione.getDataAccessoInformazione().setMinutes(Integer.parseInt(str.nextToken()));
		}

		if (schedaInformazione.getDataSezione() != null) {

			if (schedaInformazione.getDataSezione().before(schedaInformazione.getDataAccessoInformazione()))
				schedaInformazione.setDataSezione(schedaInformazione.getDataAccessoInformazione());

		} else
			schedaInformazione.setDataSezione(schedaInformazione.getDataAccessoInformazione());

		accessoInformazione.setSchedaPersona(schedaPersona);
		accessoInformazione.setSchedaInformazione(schedaInformazione);
		accessoInformazione.setMotivazioneAccesso(schedaInformazione.getMotivazioneAccesso());
		accessoInformazione.setDataCessazioneInformazione(null);
		accessoInformazione.setOraCessazioneInformazione(null);
		accessoInformazione.setOraAccessoInformazione(ora);
		accessoInformazione.setDataAccessoInformazione(schedaInformazione.getDataAccessoInformazione());
		accessoInformazione.setStorico(Utility.STORICO_NO);

		schedaPersona.setDataUltimoAggiornamento(new Date());
		schedaPersona.getListaAccessoInformazione().add(accessoInformazione);

		return accessoInformazione;
	}

	public void setCodiceSchedaDefinitiva(SchedaPersona schedaPersonaModified, int versione, String action) {

		int countPersonDb = 0;

		try {

			countPersonDb = schedaPersonaDao.countPersonDb();

		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

		int countPersona = this.loadCount(schedaPersonaModified.getTipoAmbito(), action, countPersonDb);

		if (action.equals("Conferma"))
			countPersona = countPersona + 1;

		schedaPersonaModified.setCodiceScheda(Utility
				.getCodiceSchedaPersona(schedaPersonaModified.getSocieta().getIniziale(), countPersona, versione));

		System.out.println("### CODICE SCHEDA: " + schedaPersonaModified.getCodiceScheda());
	}

	public String setCodiceSchedaModificata(String iniziale, String progressivo, int version) {

		String codice = iniziale + "-" + progressivo + "-V" + version;

		return codice;

	}

	public Set<AccessoInformazione> removeSameAccess(Set<AccessoInformazione> accessList, int id) {

		Set<AccessoInformazione> accessSet = new HashSet<AccessoInformazione>();

		for (AccessoInformazione ai : accessList) {
			if (ai.getSchedaInformazione().getIdSchedaInformazione() != id)
				accessSet.add(ai);
		}

		return accessSet;

	}

	public int loadCount(String ambito, String action, int countPersonDb) {

		int countPersona = 0;

		try {

			if (countPersonDb < 114) {

				if (ambito.equals(Utility.AMBITO_INSIDER_LIST))
					countPersona = schedaPersonaDao.countPersonaI();

				else if (ambito.equals(Utility.AMBITO_REGISTRO_MANAGER))
					countPersona = schedaPersonaDao.countPersonaR();

			} else {

				int contR = contatoreSchedaDao.findMaxValueR();
				int contI = contatoreSchedaDao.findMaxValueI();

				if (action.equals("Conferma")) {

					if (ambito.equals(Utility.AMBITO_INSIDER_LIST)) {
						countPersona = contI;
						contI = contI + 1;
					}

					else if (ambito.equals(Utility.AMBITO_REGISTRO_MANAGER)) {
						countPersona = contR;
						contR = contR + 1;
					}

				} else {

					if (ambito.equals(Utility.AMBITO_INSIDER_LIST)) {
						contI = contI + 1;
						countPersona = contI;
					}

					else if (ambito.equals(Utility.AMBITO_REGISTRO_MANAGER)) {
						contR = contR + 1;
						countPersona = contR;
					}

				}

				ContatoreScheda cs = new ContatoreScheda();

				cs.setContI(contI);
				cs.setContR(contR);

				contatoreSchedaDao.save(cs);

			}

		} catch (NullPointerException | SQLException e) {
			e.printStackTrace();
		}

		return countPersona;
	}

	public HashMap<String, LinkedList<SchedaPersona>> loadAccessMap(Set<SchedaPersona> listaSchedaPersona,
			String[] arrayInfo) throws NullPointerException, SQLException {

		HashMap<String, LinkedList<SchedaPersona>> accessMap = new HashMap<String, LinkedList<SchedaPersona>>();

		Comparator<SchedaPersona> comparator = new Comparator<SchedaPersona>() {

			@Override
			public int compare(SchedaPersona s1, SchedaPersona s2) {
				return s1.getCognome().compareTo(s2.getCognome());
			}

		};

		for (String s : arrayInfo) {

			LinkedList<SchedaPersona> singleAccessList = this.loadSingleAccessMap(s, listaSchedaPersona);

			Collections.sort(singleAccessList, comparator);

			accessMap.put(s, singleAccessList);

		}

		return accessMap;
	}

	public LinkedList<SchedaPersona> loadSingleAccessMap(String info, Set<SchedaPersona> listaSchedaPersona) {

		LinkedList<SchedaPersona> listAccessPersona = new LinkedList<SchedaPersona>();

		for (SchedaPersona s : listaSchedaPersona) {

			for (AccessoInformazione ai : s.getListaAccessoInformazione()) {

				if (ai.getSchedaInformazione().getInformazionePrivilegiata().equals(info)) {

					listAccessPersona.add(s);

					break;

				}

			}

		}

		return listAccessPersona;

	}

	public List<Riga> getRigheRegistroLim() throws NullPointerException, SQLException, ParseException {

		List<SchedaInformazione> listaSchedaInformazione = schedaInformazioneDao.findInfoListLim();

		Map<String, Set<AccessoInformazione>> accessMap = new HashMap<String, Set<AccessoInformazione>>();

		Set<AccessoInformazione> personList = new HashSet<AccessoInformazione>();

		List<Riga> righe = new ArrayList<Riga>();

		for (SchedaInformazione s : listaSchedaInformazione) {

			personList = accessoInformazioneDao.findAccessoInfoByIdInfo(s.getIdSchedaInformazione());
			accessMap.put(s.getInformazionePrivilegiata(), personList);
			personList = new HashSet<AccessoInformazione>();

		}

		for (Entry<String, Set<AccessoInformazione>> entry : accessMap.entrySet()) {

			int counter = 0;

			if (counter == 0) {

				Riga rv = new Riga();

				rv.setInfo(entry.getKey());
				rv.setStato("");
				rv.setNome("");
				rv.setCognome("");
				rv.setCodiceScheda("");
				rv.setTipologiaSoggetto("");
				rv.setSocieta("");
				rv.setRuolo("");
				rv.setDataAccessoInformazione("");
				rv.setDataCessazioneInformazione("");
				rv.setIdSchedaPersona(0);

				righe.add(rv);
			}

			for (AccessoInformazione a : entry.getValue()) {

				Riga rp = new Riga();

				rp.setInfo("");
				rp.setStato(a.getSchedaPersona().getStato());
				rp.setNome(a.getSchedaPersona().getNome());
				rp.setCognome(a.getSchedaPersona().getCognome());
				rp.setCodiceScheda(a.getSchedaPersona().getCodiceScheda());
				rp.setTipologiaSoggetto(a.getSchedaPersona().getTipologiaSoggetto());
				rp.setSocieta(a.getSchedaPersona().getSocieta().getNome());
				rp.setRuolo(a.getSchedaPersona().getRuolo());
				rp.setDataAccessoInformazione(Utility.formatDate(a.getDataAccessoInformazione()));
				rp.setIdSchedaPersona(a.getSchedaPersona().getIdSchedaPersona());

				if (a.getDataCessazioneInformazione() != null)
					rp.setDataCessazioneInformazione(Utility.formatDate(a.getDataCessazioneInformazione()));

				else
					rp.setDataCessazioneInformazione("");

				righe.add(rp);

				counter = 1;

			}

		}

		return righe;

	}

	public List<Riga> getRigheRegistroPerm() throws NullPointerException, SQLException, ParseException {

		List<AccessoInformazione> personList = accessoInformazioneDao.findAccessoPermanente();

		List<Riga> righe = new ArrayList<Riga>();

		for (AccessoInformazione a : personList) {

			Riga rp = new Riga();

			rp.setInfo("");
			rp.setStato(a.getSchedaPersona().getStato());
			rp.setNome(a.getSchedaPersona().getNome());
			rp.setCognome(a.getSchedaPersona().getCognome());
			rp.setCodiceScheda(a.getSchedaPersona().getCodiceScheda());
			rp.setTipologiaSoggetto(a.getSchedaPersona().getTipologiaSoggetto());
			rp.setSocieta(a.getSchedaPersona().getSocieta().getNome());
			rp.setRuolo(a.getSchedaPersona().getRuolo());
			rp.setDataAccessoInformazione(Utility.formatDate(a.getDataAccessoInformazione()));
			rp.setIdSchedaPersona(a.getSchedaPersona().getIdSchedaPersona());

			if (a.getDataCessazioneInformazione() != null)
				rp.setDataCessazioneInformazione(Utility.formatDate(a.getDataCessazioneInformazione()));

			else
				rp.setDataCessazioneInformazione("");

			righe.add(rp);

		}

		return righe;

	}

}
