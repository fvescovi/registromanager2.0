package it.sara.registroManager.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "SCHEDA_PERSONA")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "versioningList", "listaComunicazione",
		"listaAccessoInformazione" })
public class SchedaPersona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3791158797848388632L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_SCHEDA_PERSONA")
	@SequenceGenerator(name = "SEQUENCE_SCHEDA_PERSONA", sequenceName = "SEQ_SCHEDA_PERSONA", allocationSize = 1)
	@Column(name = "ID_SCHEDA_PERSONA")
	private Integer idSchedaPersona;

	@Column(name = "CODICE_FISCALE")
	private String codiceFiscale;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "COGNOME")
	private String cognome;

	@Column(name = "COGNOME_NASCITA")
	private String cognomeNascita;

	@Column(name = "TELEFONO_PROFESSIONALE")
	private String telefonoProfessionale;

	@Column(name = "TELEFONO_PRIVATO")
	private String telefonoPrivato;

	@Column(name = "INDIRIZZO")
	private String indirizzo;

	@Column(name = "CAP")
	private String cap;

	@Column(name = "LOCALITA")
	private String localita;

	@Column(name = "PROVINCIA")
	private String provincia;

	@Column(name = "NAZIONE")
	private String nazione;

	@Column(name = "NOME_INDIRIZZO_IMPRESA")
	private String nomeIndirizzoImpresa;

	@Column(name = "IDENTIFICATIVO_NAZIONALE")
	private String identificativoNazionale;

	@Column(name = "DATA_NASCITA")
	private Date dataNascita;

	@Column(name = "DATA_INSERIMENTO")
	private Date dataInserimento;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "RUOLO")
	private String ruolo;

	@Column(name = "RAGIONE_SOCIALE")
	private String ragioneSociale;

	@Column(name = "PARTITA_IVA")
	private String partitaIva;

	@Column(name = "TIPO_ACCESSO_INFORMAZIONE")
	private String tipoAccessoInformazione;

	@Column(name = "TIPOLOGIA_SOGGETTO")
	private String tipologiaSoggetto;

	@Column(name = "MANAGER_RIFERIMENTO")
	private String managerRiferimento;

	@Column(name = "CODICE_SCHEDA")
	private String codiceScheda;

	@Column(name = "STATO")
	private String stato;

	@Column(name = "MOTIVO_ULTIMA_VARIAZIONE")
	private String motivoUltimaVariazione;

	@Column(name = "MOTIVO_ISCRIZIONE")
	private String motivoIscrizione;

	@Column(name = "AUTORE")
	private String autore;

	@Column(name = "DATA_ISCRIZIONE")
	private Date dataIscrizione;

	@Column(name = "DATA_ULTIMO_AGGIORNAMENTO")
	private Date dataUltimoAggiornamento;

	@Column(name = "DATA_CANCELLAZIONE")
	private Date dataCancellazione;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, targetEntity = Societa.class)
	@JoinColumn(name = "ID_SOCIETA")
	private Societa societa;

	@Column(name = "TIPO_AMBITO")
	private String tipoAmbito;

	@Column(name = "DESCRIZIONE_INFORMAZIONE")
	private String descrizioneInformazione;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "schedaPersona", cascade = {
			CascadeType.ALL }, targetEntity = AccessoInformazione.class)
	private Set<AccessoInformazione> listaAccessoInformazione = new HashSet<AccessoInformazione>();

	@Embedded
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "listaSchedaPersona", cascade = { CascadeType.ALL })
	private Set<Comunicazione> listaComunicazione = new HashSet<Comunicazione>();

	@OneToMany(fetch = FetchType.LAZY, cascade = {
			CascadeType.ALL }, mappedBy = "schedaPersona", targetEntity = Versioning.class)
	private Set<Versioning> versioningList = new HashSet<Versioning>();

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Set<Versioning> getVersioningList() {
		return versioningList;
	}

	public void setVersioningList(Set<Versioning> versioningList) {
		this.versioningList = versioningList;
	}

	public Set<Comunicazione> getListaComunicazione() {
		return listaComunicazione;
	}

	public void setListaComunicazione(Set<Comunicazione> listaComunicazione) {
		this.listaComunicazione = listaComunicazione;
	}

	public Integer getIdSchedaPersona() {
		return idSchedaPersona;
	}

	public void setIdSchedaPersona(Integer idSchedaPersona) {
		this.idSchedaPersona = idSchedaPersona;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCognomeNascita() {
		return cognomeNascita;
	}

	public void setCognomeNascita(String cognomeNascita) {
		this.cognomeNascita = cognomeNascita;
	}

	public String getTelefonoProfessionale() {
		return telefonoProfessionale;
	}

	public void setTelefonoProfessionale(String telefonoProfessionale) {
		this.telefonoProfessionale = telefonoProfessionale;
	}

	public String getTelefonoPrivato() {
		return telefonoPrivato;
	}

	public void setTelefonoPrivato(String telefonoPrivato) {
		this.telefonoPrivato = telefonoPrivato;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getLocalita() {
		return localita;
	}

	public void setLocalita(String localita) {
		this.localita = localita;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getNazione() {
		return nazione;
	}

	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

	public String getNomeIndirizzoImpresa() {
		return nomeIndirizzoImpresa;
	}

	public void setNomeIndirizzoImpresa(String nomeIndirizzoImpresa) {
		this.nomeIndirizzoImpresa = nomeIndirizzoImpresa;
	}

	public String getIdentificativoNazionale() {
		return identificativoNazionale;
	}

	public void setIdentificativoNazionale(String identificativoNazionale) {
		this.identificativoNazionale = identificativoNazionale;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}

	public String getTipoAccessoInformazione() {
		return tipoAccessoInformazione;
	}

	public void setTipoAccessoInformazione(String tipoAccessoInformazione) {
		this.tipoAccessoInformazione = tipoAccessoInformazione;
	}

	public String getTipologiaSoggetto() {
		return tipologiaSoggetto;
	}

	public void setTipologiaSoggetto(String tipologiaSoggetto) {
		this.tipologiaSoggetto = tipologiaSoggetto;
	}

	public String getManagerRiferimento() {
		return managerRiferimento;
	}

	public void setManagerRiferimento(String managerRiferimento) {
		this.managerRiferimento = managerRiferimento;
	}

	public String getCodiceScheda() {
		return codiceScheda;
	}

	public void setCodiceScheda(String codiceScheda) {
		this.codiceScheda = codiceScheda;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getMotivoUltimaVariazione() {
		return motivoUltimaVariazione;
	}

	public void setMotivoUltimaVariazione(String motivoUltimaVariazione) {
		this.motivoUltimaVariazione = motivoUltimaVariazione;
	}

	public String getMotivoIscrizione() {
		return motivoIscrizione;
	}

	public void setMotivoIscrizione(String motivoIscrizione) {
		this.motivoIscrizione = motivoIscrizione;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public Date getDataIscrizione() {
		return dataIscrizione;
	}

	public void setDataIscrizione(Date dataIscrizione) {
		this.dataIscrizione = dataIscrizione;
	}

	public Date getDataUltimoAggiornamento() {
		return dataUltimoAggiornamento;
	}

	public void setDataUltimoAggiornamento(Date dataUltimoAggiornamento) {
		this.dataUltimoAggiornamento = dataUltimoAggiornamento;
	}

	public Date getDataCancellazione() {
		return dataCancellazione;
	}

	public void setDataCancellazione(Date dataCancellazione) {
		this.dataCancellazione = dataCancellazione;
	}

	public Societa getSocieta() {
		return societa;
	}

	public void setSocieta(Societa societa) {
		this.societa = societa;
	}

	public String getTipoAmbito() {
		return tipoAmbito;
	}

	public void setTipoAmbito(String tipoAmbito) {
		this.tipoAmbito = tipoAmbito;
	}

	public String getDescrizioneInformazione() {
		return descrizioneInformazione;
	}

	public void setDescrizioneInformazione(String descrizioneInformazione) {
		this.descrizioneInformazione = descrizioneInformazione;
	}

	public Set<AccessoInformazione> getListaAccessoInformazione() {
		return listaAccessoInformazione;
	}

	public void setListaAccessoInformazione(Set<AccessoInformazione> listaAccessoInformazione) {
		this.listaAccessoInformazione = listaAccessoInformazione;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autore == null) ? 0 : autore.hashCode());
		result = prime * result + ((cap == null) ? 0 : cap.hashCode());
		result = prime * result + ((codiceFiscale == null) ? 0 : codiceFiscale.hashCode());
		result = prime * result + ((codiceScheda == null) ? 0 : codiceScheda.hashCode());
		result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
		result = prime * result + ((cognomeNascita == null) ? 0 : cognomeNascita.hashCode());
		result = prime * result + ((dataCancellazione == null) ? 0 : dataCancellazione.hashCode());
		result = prime * result + ((dataIscrizione == null) ? 0 : dataIscrizione.hashCode());
		result = prime * result + ((dataNascita == null) ? 0 : dataNascita.hashCode());
		result = prime * result + ((dataUltimoAggiornamento == null) ? 0 : dataUltimoAggiornamento.hashCode());
		result = prime * result + ((descrizioneInformazione == null) ? 0 : descrizioneInformazione.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((idSchedaPersona == null) ? 0 : idSchedaPersona.hashCode());
		result = prime * result + ((identificativoNazionale == null) ? 0 : identificativoNazionale.hashCode());
		result = prime * result + ((indirizzo == null) ? 0 : indirizzo.hashCode());
		result = prime * result + ((listaAccessoInformazione == null) ? 0 : listaAccessoInformazione.hashCode());
		result = prime * result + ((listaComunicazione == null) ? 0 : listaComunicazione.hashCode());
		result = prime * result + ((localita == null) ? 0 : localita.hashCode());
		result = prime * result + ((managerRiferimento == null) ? 0 : managerRiferimento.hashCode());
		result = prime * result + ((motivoIscrizione == null) ? 0 : motivoIscrizione.hashCode());
		result = prime * result + ((motivoUltimaVariazione == null) ? 0 : motivoUltimaVariazione.hashCode());
		result = prime * result + ((nazione == null) ? 0 : nazione.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((nomeIndirizzoImpresa == null) ? 0 : nomeIndirizzoImpresa.hashCode());
		result = prime * result + ((partitaIva == null) ? 0 : partitaIva.hashCode());
		result = prime * result + ((provincia == null) ? 0 : provincia.hashCode());
		result = prime * result + ((ragioneSociale == null) ? 0 : ragioneSociale.hashCode());
		result = prime * result + ((ruolo == null) ? 0 : ruolo.hashCode());
		result = prime * result + ((societa == null) ? 0 : societa.hashCode());
		result = prime * result + ((stato == null) ? 0 : stato.hashCode());
		result = prime * result + ((telefonoPrivato == null) ? 0 : telefonoPrivato.hashCode());
		result = prime * result + ((telefonoProfessionale == null) ? 0 : telefonoProfessionale.hashCode());
		result = prime * result + ((tipoAccessoInformazione == null) ? 0 : tipoAccessoInformazione.hashCode());
		result = prime * result + ((tipoAmbito == null) ? 0 : tipoAmbito.hashCode());
		result = prime * result + ((tipologiaSoggetto == null) ? 0 : tipologiaSoggetto.hashCode());
		result = prime * result + ((versioningList == null) ? 0 : versioningList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SchedaPersona))
			return false;
		SchedaPersona other = (SchedaPersona) obj;
		if (autore == null) {
			if (other.autore != null)
				return false;
		} else if (!autore.equals(other.autore))
			return false;
		if (cap == null) {
			if (other.cap != null)
				return false;
		} else if (!cap.equals(other.cap))
			return false;
		if (codiceFiscale == null) {
			if (other.codiceFiscale != null)
				return false;
		} else if (!codiceFiscale.equals(other.codiceFiscale))
			return false;
		if (codiceScheda == null) {
			if (other.codiceScheda != null)
				return false;
		} else if (!codiceScheda.equals(other.codiceScheda))
			return false;
		if (cognome == null) {
			if (other.cognome != null)
				return false;
		} else if (!cognome.equals(other.cognome))
			return false;
		if (cognomeNascita == null) {
			if (other.cognomeNascita != null)
				return false;
		} else if (!cognomeNascita.equals(other.cognomeNascita))
			return false;
		if (dataCancellazione == null) {
			if (other.dataCancellazione != null)
				return false;
		} else if (!dataCancellazione.equals(other.dataCancellazione))
			return false;
		if (dataIscrizione == null) {
			if (other.dataIscrizione != null)
				return false;
		} else if (!dataIscrizione.equals(other.dataIscrizione))
			return false;
		if (dataNascita == null) {
			if (other.dataNascita != null)
				return false;
		} else if (!dataNascita.equals(other.dataNascita))
			return false;
		if (dataUltimoAggiornamento == null) {
			if (other.dataUltimoAggiornamento != null)
				return false;
		} else if (!dataUltimoAggiornamento.equals(other.dataUltimoAggiornamento))
			return false;
		if (descrizioneInformazione == null) {
			if (other.descrizioneInformazione != null)
				return false;
		} else if (!descrizioneInformazione.equals(other.descrizioneInformazione))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idSchedaPersona == null) {
			if (other.idSchedaPersona != null)
				return false;
		} else if (!idSchedaPersona.equals(other.idSchedaPersona))
			return false;
		if (identificativoNazionale == null) {
			if (other.identificativoNazionale != null)
				return false;
		} else if (!identificativoNazionale.equals(other.identificativoNazionale))
			return false;
		if (indirizzo == null) {
			if (other.indirizzo != null)
				return false;
		} else if (!indirizzo.equals(other.indirizzo))
			return false;
		if (listaAccessoInformazione == null) {
			if (other.listaAccessoInformazione != null)
				return false;
		} else if (!listaAccessoInformazione.equals(other.listaAccessoInformazione))
			return false;
		if (listaComunicazione == null) {
			if (other.listaComunicazione != null)
				return false;
		} else if (!listaComunicazione.equals(other.listaComunicazione))
			return false;
		if (localita == null) {
			if (other.localita != null)
				return false;
		} else if (!localita.equals(other.localita))
			return false;
		if (managerRiferimento == null) {
			if (other.managerRiferimento != null)
				return false;
		} else if (!managerRiferimento.equals(other.managerRiferimento))
			return false;
		if (motivoIscrizione == null) {
			if (other.motivoIscrizione != null)
				return false;
		} else if (!motivoIscrizione.equals(other.motivoIscrizione))
			return false;
		if (motivoUltimaVariazione == null) {
			if (other.motivoUltimaVariazione != null)
				return false;
		} else if (!motivoUltimaVariazione.equals(other.motivoUltimaVariazione))
			return false;
		if (nazione == null) {
			if (other.nazione != null)
				return false;
		} else if (!nazione.equals(other.nazione))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nomeIndirizzoImpresa == null) {
			if (other.nomeIndirizzoImpresa != null)
				return false;
		} else if (!nomeIndirizzoImpresa.equals(other.nomeIndirizzoImpresa))
			return false;
		if (partitaIva == null) {
			if (other.partitaIva != null)
				return false;
		} else if (!partitaIva.equals(other.partitaIva))
			return false;
		if (provincia == null) {
			if (other.provincia != null)
				return false;
		} else if (!provincia.equals(other.provincia))
			return false;
		if (ragioneSociale == null) {
			if (other.ragioneSociale != null)
				return false;
		} else if (!ragioneSociale.equals(other.ragioneSociale))
			return false;
		if (ruolo == null) {
			if (other.ruolo != null)
				return false;
		} else if (!ruolo.equals(other.ruolo))
			return false;
		if (societa == null) {
			if (other.societa != null)
				return false;
		} else if (!societa.equals(other.societa))
			return false;
		if (stato == null) {
			if (other.stato != null)
				return false;
		} else if (!stato.equals(other.stato))
			return false;
		if (telefonoPrivato == null) {
			if (other.telefonoPrivato != null)
				return false;
		} else if (!telefonoPrivato.equals(other.telefonoPrivato))
			return false;
		if (telefonoProfessionale == null) {
			if (other.telefonoProfessionale != null)
				return false;
		} else if (!telefonoProfessionale.equals(other.telefonoProfessionale))
			return false;
		if (tipoAccessoInformazione == null) {
			if (other.tipoAccessoInformazione != null)
				return false;
		} else if (!tipoAccessoInformazione.equals(other.tipoAccessoInformazione))
			return false;
		if (tipoAmbito == null) {
			if (other.tipoAmbito != null)
				return false;
		} else if (!tipoAmbito.equals(other.tipoAmbito))
			return false;
		if (tipologiaSoggetto == null) {
			if (other.tipologiaSoggetto != null)
				return false;
		} else if (!tipologiaSoggetto.equals(other.tipologiaSoggetto))
			return false;
		if (versioningList == null) {
			if (other.versioningList != null)
				return false;
		} else if (!versioningList.equals(other.versioningList))
			return false;
		return true;
	}

}