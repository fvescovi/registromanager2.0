package it.sara.registroManager.pojo;

import com.opencsv.bean.CsvBindByName;

public class AccessoInformazioneCsv {

	@CsvBindByName(column = "nome")
	private String nomeInformazione; // Accessi permanenti

	@CsvBindByName(column = "Nome")
	private String nome;

	@CsvBindByName(column = "Cognome")
	private String cognome;

	@CsvBindByName(column = "data_accesso")
	private String dataAccessoInformazione;

	@CsvBindByName(column = "ora_accesso")
	private String oraAccessoInformazione;

	@CsvBindByName(column = "inserimento")
	private String dataInserimento;

	@CsvBindByName(column = "autore")
	private String autoreInserimento;

	// @CsvBindByName(column = "")
	private String dataCessazioneInformazione;

	// @CsvBindByName(column = "")
	private String oraCessazioneInformazione;

	@CsvBindByName(column = "Motivo accesso")
	private String motivazioneAccesso;

	public String getNomeInformazione() {
		return nomeInformazione;
	}

	public void setNomeInformazione(String nomeInformazione) {
		this.nomeInformazione = nomeInformazione;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getDataAccessoInformazione() {
		return dataAccessoInformazione;
	}

	public void setDataAccessoInformazione(String dataAccessoInformazione) {
		this.dataAccessoInformazione = dataAccessoInformazione;
	}

	public String getOraAccessoInformazione() {
		return oraAccessoInformazione;
	}

	public void setOraAccessoInformazione(String oraAccessoInformazione) {
		this.oraAccessoInformazione = oraAccessoInformazione;
	}

	public String getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(String dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public String getAutoreInserimento() {
		return autoreInserimento;
	}

	public void setAutoreInserimento(String autoreInserimento) {
		this.autoreInserimento = autoreInserimento;
	}

	public String getDataCessazioneInformazione() {
		return dataCessazioneInformazione;
	}

	public void setDataCessazioneInformazione(String dataCessazioneInformazione) {
		this.dataCessazioneInformazione = dataCessazioneInformazione;
	}

	public String getOraCessazioneInformazione() {
		return oraCessazioneInformazione;
	}

	public void setOraCessazioneInformazione(String oraCessazioneInformazione) {
		this.oraCessazioneInformazione = oraCessazioneInformazione;
	}

	public String getMotivazioneAccesso() {
		return motivazioneAccesso;
	}

	public void setMotivazioneAccesso(String motivazioneAccesso) {
		this.motivazioneAccesso = motivazioneAccesso;
	}

}
