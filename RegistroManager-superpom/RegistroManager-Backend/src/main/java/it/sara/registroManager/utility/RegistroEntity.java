/**
 * 
 */
package it.sara.registroManager.utility;

import java.util.ArrayList;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

/**
 * @author f.vescovi
 * @param <T>
 *
 */
public class RegistroEntity<T> {
	
	private T body;
	private  HttpHeaders headers;
	private String messageType;
	private String messageDescription;
	private HttpStatus status;
	
	/**
	 * @param body
	 * @param headers
	 * @param messageType
	 * @param messageDescription
	 * @param status
	 */
	public RegistroEntity(T body, HttpHeaders headers, String messageType, String messageDescription, HttpStatus status) {
		super();
		this.body = body;
		this.headers = headers;
		this.messageType = messageType;
		this.messageDescription = messageDescription;
		this.status = status;
	}
	/**
	 * @return the body
	 */
	public T getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(T body) {
		this.body = body;
	}
	/**
	 * @return the headers
	 */
	public HttpHeaders getHeaders() {
		return headers;
	}
	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}
	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return messageType;
	}
	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * @return the messageDescription
	 */
	public String getMessageDescription() {
		return messageDescription;
	}
	/**
	 * @param messageDescription the messageDescription to set
	 */
	public void setMessageDescription(String messageDescription) {
		this.messageDescription = messageDescription;
	}
	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	
	
	

}
