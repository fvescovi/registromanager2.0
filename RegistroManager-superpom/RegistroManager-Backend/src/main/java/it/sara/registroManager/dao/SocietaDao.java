package it.sara.registroManager.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.registroManager.entity.Societa;

public interface SocietaDao extends CrudRepository<Societa, Integer> {

	@Query("select s from Societa s where s.stato='Definitivo'")
	List<Societa> findSocietaByStato() throws SQLException, NullPointerException;

	@Query("select s from Societa s where s.nome=:nome")
	Societa findSocietaByNome(@Param("nome") String nome) throws SQLException, NullPointerException;

}