package it.sara.registroManager.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "SCHEDA_INFORMAZIONE")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "listaAccessoInformazione" })
public class SchedaInformazione implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2475106064970573468L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_SCHEDA_INFORMAZIONE")
	@SequenceGenerator(name = "SEQUENCE_SCHEDA_INFORMAZIONE", sequenceName = "SEQ_SCHEDA_INFORMAZIONE", allocationSize = 1)
	@Column(name = "ID_SCHEDA_INFORMAZIONE")
	private Integer idSchedaInformazione;

	@Column(name = "INFORMAZIONE_PRIVILEGIATA")
	private String informazionePrivilegiata;

	@Column(name = "DATA_IDENTIFICAZIONE")
	private Date dataIdentificazione;

	@Column(name = "ORA_IDENTIFICAZIONE")
	private String oraIdentificazione;

	@Column(name = "AUTORE_INFORMAZIONE")
	private String autoreInformazione;

	@Column(name = "DATA_ULTIMO_AGGIORNAMENTO_INFO")
	private Date dataUltimoAggiornamentoInfo;

	@Column(name = "DATA_ACCESSO_INFORMAZIONE")
	private Date dataAccessoInformazione;

	@Column(name = "ORA_ACCESSO_INFORMAZIONE")
	private String oraAccessoInformazione;

	@Column(name = "MOTIVAZIONE_ACCESSO")
	private String motivazioneAccesso;

	@Column(name = "DATA_CESSAZIONE_INFORMAZIONE")
	private Date dataCessazioneInformazione;

	@Column(name = "ORA_CESSAZIONE_INFORMAZIONE")
	private String oraCessazioneInformazione;

	@Column(name = "STATO")
	private String stato;

	@Column(name = "DATA_SEZIONE")
	private Date dataSezione;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "schedaInformazione", cascade = CascadeType.ALL, targetEntity = AccessoInformazione.class)
	private Set<AccessoInformazione> listaAccessoInformazione = new HashSet<AccessoInformazione>();

	public Set<AccessoInformazione> getListaAccessoInformazione() {
		return listaAccessoInformazione;
	}

	public void setListaAccessoInformazione(Set<AccessoInformazione> listaAccessoInformazione) {
		this.listaAccessoInformazione = listaAccessoInformazione;
	}

	public Date getDataSezione() {
		return dataSezione;
	}

	public void setDataSezione(Date dataSezione) {
		this.dataSezione = dataSezione;
	}

	public Integer getIdSchedaInformazione() {
		return idSchedaInformazione;
	}

	public void setIdSchedaInformazione(Integer idSchedaInformazione) {
		this.idSchedaInformazione = idSchedaInformazione;
	}

	public String getInformazionePrivilegiata() {
		return informazionePrivilegiata;
	}

	public void setInformazionePrivilegiata(String informazionePrivilegiata) {
		this.informazionePrivilegiata = informazionePrivilegiata;
	}

	public Date getDataIdentificazione() {
		return dataIdentificazione;
	}

	public void setDataIdentificazione(Date dataIdentificazione) {
		this.dataIdentificazione = dataIdentificazione;
	}

	public String getOraIdentificazione() {
		return oraIdentificazione;
	}

	public void setOraIdentificazione(String oraIdentificazione) {
		this.oraIdentificazione = oraIdentificazione;
	}

	public String getOraAccessoInformazione() {
		return oraAccessoInformazione;
	}

	public void setOraAccessoInformazione(String oraAccessoInformazione) {
		this.oraAccessoInformazione = oraAccessoInformazione;
	}

	public String getAutoreInformazione() {
		return autoreInformazione;
	}

	public void setAutoreInformazione(String autoreInformazione) {
		this.autoreInformazione = autoreInformazione;
	}

	public Date getDataUltimoAggiornamentoInfo() {
		return dataUltimoAggiornamentoInfo;
	}

	public void setDataUltimoAggiornamentoInfo(Date dataUltimoAggiornamentoInfo) {
		this.dataUltimoAggiornamentoInfo = dataUltimoAggiornamentoInfo;
	}

	public Date getDataAccessoInformazione() {
		return dataAccessoInformazione;
	}

	public void setDataAccessoInformazione(Date dataAccessoInformazione) {
		this.dataAccessoInformazione = dataAccessoInformazione;
	}

	public String getMotivazioneAccesso() {
		return motivazioneAccesso;
	}

	public void setMotivazioneAccesso(String motivazioneAccesso) {
		this.motivazioneAccesso = motivazioneAccesso;
	}

	public Date getDataCessazioneInformazione() {
		return dataCessazioneInformazione;
	}

	public void setDataCessazioneInformazione(Date dataCessazioneInformazione) {
		this.dataCessazioneInformazione = dataCessazioneInformazione;
	}

	public String getOraCessazioneInformazione() {
		return oraCessazioneInformazione;
	}

	public void setOraCessazioneInformazione(String oraCessazioneInformazione) {
		this.oraCessazioneInformazione = oraCessazioneInformazione;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

}
