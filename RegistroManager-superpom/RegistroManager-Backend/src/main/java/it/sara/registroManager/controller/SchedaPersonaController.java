package it.sara.registroManager.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sara.registroManager.bean.GenericMapper;
import it.sara.registroManager.bean.RicercaPersona;
import it.sara.registroManager.dao.AccessoInformazioneDao;
import it.sara.registroManager.dao.SchedaInformazioneDao;
import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.dao.VersioningDao;
import it.sara.registroManager.entity.AccessoInformazione;
import it.sara.registroManager.entity.SchedaInformazione;
import it.sara.registroManager.entity.SchedaPersona;
import it.sara.registroManager.entity.Versioning;
import it.sara.registroManager.utility.RicercaManager;
import it.sara.registroManager.utility.SchedaPersonaCrudManager;
import it.sara.registroManager.utility.SchedaPersonaStatoManager;
import it.sara.registroManager.utility.Utility;
import it.sara.registroManager.utility.VersioningSchedaPersonaManager;

@RestController
@RequestMapping("/rest")
@CrossOrigin(origins = "*")
public class SchedaPersonaController {

	private Logger logger = LoggerFactory.getLogger(SchedaPersonaController.class);

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	@Autowired
	SchedaInformazioneDao schedaInformazioneDao;

	@Autowired
	VersioningDao versioningDao;

	@Autowired
	AccessoInformazioneDao accessoInformazioneDao;

	@Autowired
	SchedaPersonaStatoManager schedaPersonaStatoManager;

	@Autowired
	SchedaPersonaCrudManager schedaPersonaCrudManager;

	@Autowired
	VersioningSchedaPersonaManager versioningSchedaPersonaManager;

	@SuppressWarnings("rawtypes")
	@Autowired
	GenericMapper genericMapper;

	@Autowired
	RicercaManager ricercaManager;

	@Autowired
	EntityManager entityManager;

	@GetMapping(value = "/caricaListaSchedaPersona/{sceltaAmbito}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<SchedaPersona> caricaListaSchedaPersona(@PathVariable(name = "sceltaAmbito") String sceltaAmbito)
			throws SQLException, NullPointerException, ParseException {

		List<SchedaPersona> listaSchedaPersona = new ArrayList<SchedaPersona>();

		if (sceltaAmbito.equals(Utility.AMBITO_REGISTRO_MANAGER))
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaRM();

		else
			listaSchedaPersona = (List<SchedaPersona>) schedaPersonaDao.findPersonaIL();

		return listaSchedaPersona;

	}

	@GetMapping(value = "/caricaListaSchedaPersonaStato/{ambito}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<List<SchedaPersona>> caricaListaSchedaPersonaStato(@PathVariable(name = "ambito") String ambito)
			throws SQLException, ParseException, NullPointerException {

		List<List<SchedaPersona>> listaSchedaPersonaStato = new ArrayList<List<SchedaPersona>>();

		if (ambito.equals(Utility.AMBITO_REGISTRO_MANAGER))
			listaSchedaPersonaStato = schedaPersonaStatoManager.getListCompleteRM();

		else
			listaSchedaPersonaStato = schedaPersonaStatoManager.getListCompleteIL();

		return listaSchedaPersonaStato;

	}

	@GetMapping(value = "/caricaListaSchedaPersonaSocieta/{nomeSocieta}/{ambito}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public List<SchedaPersona> caricaListaSchedaPersonaSocieta(@PathVariable(name = "nomeSocieta") String nomeSocieta,
			@PathVariable(name = "ambito") String ambito) throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaSchedaPersonaSocieta = schedaPersonaDao.findSchedaPersonaBySocieta(nomeSocieta,
				ambito);

		return listaSchedaPersonaSocieta;

	}

	@GetMapping(value = "/caricaListaSchedaPersonaDataUltimoAggiornamento/{ambito}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public List<SchedaPersona> caricaListaSchedaPersonaDataUltimoAggiornamento(
			@PathVariable(name = "ambito") String ambito) throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaSchedaPersonaDataUltimoAggiornamento = new ArrayList<SchedaPersona>();

		if (ambito.equals(Utility.AMBITO_REGISTRO_MANAGER))
			listaSchedaPersonaDataUltimoAggiornamento = schedaPersonaDao.findPersonaByDataUltAggRM();

		else
			listaSchedaPersonaDataUltimoAggiornamento = schedaPersonaDao.findPersonaByDataUltAggIL();

		return listaSchedaPersonaDataUltimoAggiornamento;

	}

	@GetMapping(value = "/findSchedaPersonaById/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public SchedaPersona findSchedaPersonaById(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {

		int idSchedaPersona = Integer.parseInt(id);

		Optional<SchedaPersona> schedaPersona = schedaPersonaDao.findById(idSchedaPersona);

		return schedaPersona.get();

	}

	@PostMapping(value = "/saveSchedaPersona/{username}")
	public SchedaPersona saveSchedaPersona(@RequestBody SchedaPersona schedaPersona,
			@PathVariable(name = "username") String username)
			throws SQLException, NullPointerException, ParseException {

		int version = 1;

		schedaPersona.setDataNascita(schedaPersona.getDataNascita());

		Versioning versioning = new Versioning();

		Optional<SchedaPersona> schedaPersonaDB = null;

		SchedaPersona schedaPersonaModified = null;

		schedaPersonaCrudManager.setFieldInSave(schedaPersona, username);

		if (schedaPersona.getIdSchedaPersona() != null) {

			schedaPersonaDB = schedaPersonaDao.findById(schedaPersona.getIdSchedaPersona());
			schedaPersonaModified = schedaPersona;

		} else {
			schedaPersonaModified = schedaPersonaDao.save(schedaPersona);
		}

		if (!schedaPersona.getStato().equals(Utility.STATO_CANCELLATO)) {
			Set<Versioning> versionList = versioningDao
					.getVersioniSchedaPersonaById(schedaPersonaModified.getIdSchedaPersona());

			if (versionList.isEmpty()) {

				if (schedaPersona.getStato().equals(Utility.STATO_BOZZA)) {

					// INSERIMENTO SCHEDA BOZZA
					schedaPersona.setCodiceScheda(null);
					versioning.setVersione(0);

				} else {

					// INSERIMENTO SCHEDA DEFINITIVA
					if (schedaPersonaDB == null)
						schedaPersonaCrudManager.setCodiceSchedaDefinitiva(schedaPersonaModified, 1, "");

					// CONFERMA DA BOZZA
					else
						schedaPersonaCrudManager.setCodiceSchedaDefinitiva(schedaPersonaModified, 1, "Conferma");

					versioning.setVersione(1);
				}

			} else {

				version = versioningDao.getMaxVersioneSchedaPersonaById(schedaPersonaModified.getIdSchedaPersona());
				version = version + 1;

				// MODIFICA SCHEDA PERSONA
				String codice = schedaPersonaCrudManager.setCodiceSchedaModificata(
						schedaPersonaModified.getSocieta().getIniziale(),
						schedaPersonaModified.getCodiceScheda().substring(2, 8), version);

				schedaPersonaModified.setCodiceScheda(codice);
				versioning.setVersione(version);

				schedaPersonaModified.setDataUltimoAggiornamento(new Date());

			}

			versioning.setDataCreazione(new Date());

			versioning.setSchedaJson(genericMapper.javaToJson(schedaPersonaModified));

			if (schedaPersonaDB == null)
				versioning.setDatiModificati("Prima creazione");

			else if (schedaPersonaModified.getStato().equals(Utility.STATO_DEFINITIVO))
				if (schedaPersonaDB.get().getStato().equals(Utility.STATO_BOZZA)) {
					versioning.setDatiModificati("Prima creazione");
				} else {
					versioning.setDatiModificati(versioningSchedaPersonaManager
							.getListaCampiModificati(schedaPersonaModified, schedaPersonaDB.get()));
				}

			// CONFERMA
			else if (schedaPersonaModified.getStato().equals(Utility.STATO_BOZZA)) {

				if (schedaPersonaDB.get().getStato().equals(Utility.STATO_BOZZA)) {

					schedaPersonaModified = schedaPersonaDao.save(schedaPersonaModified);

					return schedaPersonaModified;

				}

				schedaPersonaModified.setStato(Utility.STATO_DEFINITIVO);
				versioning.setDatiModificati("Prima creazione");
				versioning.setVersione(1);
			}

			versioning.setSchedaPersona(schedaPersonaModified);

			versionList.add(versioning);

			schedaPersonaModified.setVersioningList(versionList);
		}

		// CESSAZIONE ACCESSI IN CASO DI ELIMINAZIONE SCHEDA PERSONA

		if (schedaPersona.getStato().equals(Utility.STATO_CANCELLATO)) {

			Set<AccessoInformazione> accessList = accessoInformazioneDao
					.findAccessoInfoByIdPersona(schedaPersona.getIdSchedaPersona());

			if (accessList.size() > 0) {

				for (AccessoInformazione a : accessList) {

					a.setDataCessazioneInformazione(new SimpleDateFormat("dd/MM/yyyy")
							.parse(Utility.formatDate(schedaPersona.getDataCancellazione())));
					a.setOraCessazioneInformazione(Utility.formatHour(schedaPersona.getDataCancellazione()));

					if (a.getSchedaInformazione().getDataSezione().before(schedaPersona.getDataCancellazione()))
						a.getSchedaInformazione().setDataSezione(schedaPersona.getDataCancellazione());

					accessoInformazioneDao.save(a);

				}
			}

		}

		schedaPersonaModified = schedaPersonaDao.save(schedaPersonaModified);

		return schedaPersonaModified;

	}

	@DeleteMapping(value = "/deleteSchedaPersonaById/{id}/{autore}")
	public void deleteSchedaPersonaById(@PathVariable(name = "id") String id,
			@PathVariable(name = "autore") String autore) throws SQLException, ParseException, NullPointerException {

		int idSchedaPersona = Integer.parseInt(id);

		Optional<SchedaPersona> schedaPersona = schedaPersonaDao.findById(idSchedaPersona);

		schedaPersonaCrudManager.setFieldInDelete(schedaPersona.get(), autore);

		versioningDao.deleteVersioniSchedaPersonaById(schedaPersona.get().getIdSchedaPersona());

		schedaPersonaDao.deleteById(schedaPersona.get().getIdSchedaPersona());

	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/ricercaPersona/{ambito}/{ricercaPersona}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<SchedaPersona> ricercaPersona(@PathVariable(name = "ambito") String ambito,
			@PathVariable(name = "ricercaPersona") String ricercaPersona)
			throws SQLException, ParseException, NullPointerException, HibernateException {

		RicercaPersona rp = new RicercaPersona();

		rp = (RicercaPersona) genericMapper.jsonToJava(ricercaPersona, rp);

		String sql = ricercaManager.getQueryRicerca(ambito, rp);

		Query query = entityManager.createQuery(sql);

		List<SchedaPersona> listaSchedaPersona = query.getResultList();

		return listaSchedaPersona;

	}

	@GetMapping(value = "/getPersonaByIdInfo/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<SchedaPersona> getPersonaByIdInfo(@PathVariable(name = "id") String id)
			throws SQLException, ParseException, NullPointerException {

		int idSchedaInformazione = Integer.parseInt(id);

		List<SchedaPersona> listaSchedaPersona = schedaPersonaDao.findSpByInformazione(idSchedaInformazione);

		return listaSchedaPersona;

	}

	@PostMapping(value = "/associaInformazionePersona/{id}/{ora}/{tipoAccessoInformazione}")
	public Set<AccessoInformazione> associaInformazionePersona(@PathVariable(name = "id") String id,
			@PathVariable(name = "ora") String ora,
			@PathVariable(name = "tipoAccessoInformazione") String tipoAccessoInformazione,
			@RequestBody SchedaInformazione schedaInformazione)
			throws NullPointerException, ParseException, SQLException {

		int idSchedaPersona = Integer.parseInt(id);

		Optional<SchedaPersona> schedaPersonaDb = schedaPersonaDao.findById(idSchedaPersona);

		SchedaPersona schedaPersona = schedaPersonaDb.get();

		if (schedaPersona.getTipoAccessoInformazione() == null)
			schedaPersona.setTipoAccessoInformazione(tipoAccessoInformazione);

		if (schedaInformazione.getStato() == null)
			schedaInformazione.setStato(Utility.STATO_INFO_ATTIVA);

		boolean found = false;

		if (schedaPersona.getListaAccessoInformazione().size() != 0) {

			for (AccessoInformazione ai : schedaPersona.getListaAccessoInformazione()) {

				if (ai.getSchedaInformazione().getIdSchedaInformazione() == schedaInformazione
						.getIdSchedaInformazione()) {

					found = true;

					schedaPersona.setListaAccessoInformazione(
							schedaPersonaCrudManager.removeSameAccess(schedaPersona.getListaAccessoInformazione(),
									ai.getSchedaInformazione().getIdSchedaInformazione()));

					schedaPersonaCrudManager.setAccessoInformazione(schedaPersona, schedaInformazione, ora);

					schedaPersonaDao.save(schedaPersona);

					break;

				}

			}

		}

		if (!found)
			schedaPersonaCrudManager.addAccessInfo(schedaPersona, schedaInformazione, ora);

		return schedaPersona.getListaAccessoInformazione();

	}

	@SuppressWarnings("deprecation")
	@PostMapping(value = "/cessaInformazionePersona/{id}")
	public Set<AccessoInformazione> cessaInformazionePersona(@PathVariable(name = "id") Integer id,
			@RequestBody AccessoInformazione accessoInformazione)
			throws NullPointerException, ParseException, SQLException {

		Optional<SchedaPersona> schedaPersona = schedaPersonaDao.findById(id);

		for (AccessoInformazione ai : schedaPersona.get().getListaAccessoInformazione()) {

			if (ai.getId() == accessoInformazione.getId()) {

				ai.setDataCessazioneInformazione(accessoInformazione.getDataCessazioneInformazione());
				ai.setOraCessazioneInformazione(accessoInformazione.getOraCessazioneInformazione());
				ai.setStorico(Utility.STORICO_SI);
				ai.getSchedaPersona().setDataUltimoAggiornamento(new Date());

				StringTokenizer str = new StringTokenizer(accessoInformazione.getOraCessazioneInformazione(), ":");

				while (str.hasMoreElements()) {
					accessoInformazione.getDataCessazioneInformazione().setHours(Integer.parseInt(str.nextToken()));
					accessoInformazione.getDataCessazioneInformazione().setMinutes(Integer.parseInt(str.nextToken()));
				}

				if (ai.getSchedaInformazione().getDataSezione()
						.before(accessoInformazione.getDataCessazioneInformazione()))
					ai.getSchedaInformazione().setDataSezione(accessoInformazione.getDataCessazioneInformazione());

				if (ai.getSchedaInformazione().getInformazionePrivilegiata().equals(Utility.ACCESSO_PERMANENTE))
					schedaPersona.get().setTipoAccessoInformazione(null);

				break;
			}

		}

		schedaPersonaDao.save(schedaPersona.get());

		return schedaPersona.get().getListaAccessoInformazione();

	}

}
