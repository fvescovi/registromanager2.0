package it.sara.registroManager.bean;

import java.io.Serializable;
import java.util.List;

import it.sara.registroManager.entity.SchedaPersona;

public class PaginaSchedaPersona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2330018352524948597L;

	List<SchedaPersona> listaSchedaPersona;
	private Integer totalePagina;
	private int paginaCorrente;
	private Long totaleRecord;
	private Integer recordPerPagina;

	public List<SchedaPersona> getListaSchedaPersona() {
		return listaSchedaPersona;
	}

	public void setListaSchedaPersona(List<SchedaPersona> listaSchedaPersona) {
		this.listaSchedaPersona = listaSchedaPersona;
	}

	public Integer getTotalePagina() {
		return totalePagina;
	}

	public void setTotalePagina(Integer totalePagina) {
		this.totalePagina = totalePagina;
	}

	public int getPaginaCorrente() {
		return paginaCorrente;
	}

	public void setPaginaCorrente(int paginaCorrente) {
		this.paginaCorrente = paginaCorrente;
	}

	public Long getTotaleRecord() {
		return totaleRecord;
	}

	public void setTotaleRecord(Long totaleRecord) {
		this.totaleRecord = totaleRecord;
	}

	public Integer getRecordPerPagina() {
		return recordPerPagina;
	}

	public void setRecordPerPagina(Integer recordPerPagina) {
		this.recordPerPagina = recordPerPagina;
	}

}
