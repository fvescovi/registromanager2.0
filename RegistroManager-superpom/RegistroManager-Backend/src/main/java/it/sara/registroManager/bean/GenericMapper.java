package it.sara.registroManager.bean;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.sara.registroManager.entity.SchedaPersona;

@Component("GenericMapper")
public class GenericMapper<T> {

	@Autowired
	ObjectMapper objectMapper;

	@SuppressWarnings("unchecked")
	public T jsonToJava(String jsonString, T entity) {

		try {

			entity = (T) objectMapper.readValue(jsonString, entity.getClass());

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return entity;

	}

	public String javaToJson(SchedaPersona schedaPersona) {

		String jsonString = "{\"idSchedaPersona\": " + schedaPersona.getIdSchedaPersona() + ", \"codiceFiscale\": "
				+ schedaPersona.getCodiceFiscale().toUpperCase() + ", \"nome\": " + schedaPersona.getNome() + ", \"cognome\": "
				+ schedaPersona.getCognome() + ", \"cognomeNascita\": " + schedaPersona.getCognomeNascita()
				+ ", \"telefonoProfessionale\": " + schedaPersona.getTelefonoProfessionale() + ", \"telefonoPrivato\": "
				+ schedaPersona.getTelefonoPrivato() + ", \"indirizzo\": " + schedaPersona.getIndirizzo()
				+ ", \"cap\": " + schedaPersona.getCap() + ", \"localita\": " + schedaPersona.getLocalita()
				+ ", \"provincia\": " + schedaPersona.getProvincia() + ", \"nazione\": " + schedaPersona.getNazione()
				+ ", \"nomeIndirizzoImpresa\": " + schedaPersona.getNomeIndirizzoImpresa()
				+ ", \"identificativoNazionale\": " + schedaPersona.getIdentificativoNazionale() + ", \"dataNascita\": "
				+ schedaPersona.getDataNascita() + ", \"email\": " + schedaPersona.getEmail() + ", \"ruolo\": "
				+ schedaPersona.getRuolo() + ", \"ragioneSociale\": " + schedaPersona.getRagioneSociale()
				+ ", \"partitaIva\": " + schedaPersona.getPartitaIva() + ", \"tipoAccessoInformazione\": "
				+ schedaPersona.getTipoAccessoInformazione() + ", \"tipologiaSoggetto\": "
				+ schedaPersona.getTipologiaSoggetto() + ", \"managerRiferimento\": "
				+ schedaPersona.getManagerRiferimento() + ", \"codiceScheda\": " + schedaPersona.getCodiceScheda()
				+ ", \"stato\": " + schedaPersona.getStato() + ", \"motivoUltimaVariazione\": "
				+ schedaPersona.getMotivoUltimaVariazione() + ", \"motivoIscrizione\": "
				+ schedaPersona.getMotivoIscrizione() + ", \"autore\": " + schedaPersona.getAutore()
				+ ", \"dataIscrizione\": " + schedaPersona.getDataIscrizione() + ", \"dataUltimoAggiornamento\": "
				+ schedaPersona.getDataUltimoAggiornamento() + ", \"dataCancellazione\": "
				+ schedaPersona.getDataCancellazione() + ", \"societa\": " + schedaPersona.getSocieta();

		try {

			jsonString = objectMapper.writeValueAsString(schedaPersona);

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonString;

	}

}
