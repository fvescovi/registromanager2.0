package it.sara.registroManager.export;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.sara.registroManager.dao.SchedaInformazioneDao;
import it.sara.registroManager.entity.SchedaPersona;

@Component("AbstractExportSchedaPersona")
public abstract class AbstractExportSchedaPersona {

	@Autowired
	SchedaInformazioneDao schedaInformazioneDao;

	public void creazioneFoglioXls(Set<SchedaPersona> listaSchedaPersona, String[] arrayInfo,
			ServletOutputStream fileOut) throws SQLException, ParseException, InvalidFormatException, IOException {

	}

	public void creazioneFoglioXlsPerm(List<SchedaPersona> listaSchedaPersona, ServletOutputStream fileOut)
			throws SQLException, ParseException, InvalidFormatException, IOException {

	}

	public void creazioneFoglioSelectedInfo(Map<String, List<SchedaPersona>> accessMap, ServletOutputStream fileOut)
			throws SQLException, ParseException, InvalidFormatException, IOException {

	}

	public void creazioneFoglioCsv(List<SchedaPersona> listaSchedaPersona, ServletOutputStream fileOut, String ambito)
			throws SQLException, ParseException, InvalidFormatException, IOException {

	}

}
