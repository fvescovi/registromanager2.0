package it.sara.registroManager.bean;

public class Riga {

	private int idSchedaPersona;
	private String info;
	private String stato;
	private String nome;
	private String cognome;
	private String codiceScheda;
	private String tipologiaSoggetto;
	private String societa;
	private String ruolo;
	private String dataAccessoInformazione;
	private String dataCessazioneInformazione;

	public int getIdSchedaPersona() {
		return idSchedaPersona;
	}

	public void setIdSchedaPersona(int idSchedaPersona) {
		this.idSchedaPersona = idSchedaPersona;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodiceScheda() {
		return codiceScheda;
	}

	public void setCodiceScheda(String codiceScheda) {
		this.codiceScheda = codiceScheda;
	}

	public String getTipologiaSoggetto() {
		return tipologiaSoggetto;
	}

	public void setTipologiaSoggetto(String tipologiaSoggetto) {
		this.tipologiaSoggetto = tipologiaSoggetto;
	}

	public String getSocieta() {
		return societa;
	}

	public void setSocieta(String societa) {
		this.societa = societa;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

	public String getDataAccessoInformazione() {
		return dataAccessoInformazione;
	}

	public void setDataAccessoInformazione(String dataAccessoInformazione) {
		this.dataAccessoInformazione = dataAccessoInformazione;
	}

	public String getDataCessazioneInformazione() {
		return dataCessazioneInformazione;
	}

	public void setDataCessazioneInformazione(String dataCessazioneInformazione) {
		this.dataCessazioneInformazione = dataCessazioneInformazione;
	}

}
