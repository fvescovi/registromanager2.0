package it.sara.registroManager.utility;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.registroManager.dao.SchedaPersonaDao;
import it.sara.registroManager.entity.SchedaPersona;

@Component("SchedaPersonaStatoManager")
public class SchedaPersonaStatoManager {

	@Autowired
	SchedaPersonaDao schedaPersonaDao;

	public List<List<SchedaPersona>> getListCompleteRM()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoDefinitivo = getListaPersonaDefRM();
		List<SchedaPersona> listaPersonaStatoCancellato = getListaPersonaCancRM();
		List<SchedaPersona> listaPersonaStatoBozza = getListaPersonaBozzaRM();

		List<List<SchedaPersona>> listaCompleta = new ArrayList<List<SchedaPersona>>();

		listaCompleta.add(listaPersonaStatoDefinitivo);
		listaCompleta.add(listaPersonaStatoBozza);
		listaCompleta.add(listaPersonaStatoCancellato);

		return listaCompleta;

	}

	public List<SchedaPersona> getListaPersonaDefRM()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoDefinitivo = schedaPersonaDao
				.findPersonaDefinitivaRM();
		return listaPersonaStatoDefinitivo;

	}

	public List<SchedaPersona> getListaPersonaCancRM()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoCancellato = schedaPersonaDao
				.findPersonaCancellataRM();
		return listaPersonaStatoCancellato;

	}

	public List<SchedaPersona> getListaPersonaBozzaRM()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoBozza = schedaPersonaDao.findPersonaBozzaRM();
		return listaPersonaStatoBozza;

	}

	public List<List<SchedaPersona>> getListCompleteIL()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoDefinitivo = getListaPersonaDefIL();
		List<SchedaPersona> listaPersonaStatoCancellato = getListaPersonaCancIL();
		List<SchedaPersona> listaPersonaStatoBozza = getListaPersonaBozzaIL();

		List<List<SchedaPersona>> listaCompleta = new ArrayList<List<SchedaPersona>>();

		listaCompleta.add(listaPersonaStatoDefinitivo);
		listaCompleta.add(listaPersonaStatoBozza);
		listaCompleta.add(listaPersonaStatoCancellato);

		return listaCompleta;

	}

	public List<SchedaPersona> getListaPersonaDefIL()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoDefinitivo = schedaPersonaDao
				.findPersonaDefinitivaIL();
		return listaPersonaStatoDefinitivo;

	}

	public List<SchedaPersona> getListaPersonaCancIL()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoCancellato = schedaPersonaDao
				.findPersonaCancellataIL();
		return listaPersonaStatoCancellato;

	}

	public List<SchedaPersona> getListaPersonaBozzaIL()
			throws SQLException, ParseException, NullPointerException {

		List<SchedaPersona> listaPersonaStatoBozza = schedaPersonaDao.findPersonaBozzaIL();
		return listaPersonaStatoBozza;

	}

}
