--------------------------------------------------------
--  File created - Tuesday-August-27-2019   
--------------------------------------------------------
DROP TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" cascade constraints;
DROP TABLE "REGISTRO"."COMUNICAZIONE" cascade constraints;
DROP TABLE "REGISTRO"."COMUNICAZIONE_PERSONA" cascade constraints;
DROP TABLE "REGISTRO"."CONTATORE_SCHEDA" cascade constraints;
DROP TABLE "REGISTRO"."SCHEDA_INFORMAZIONE" cascade constraints;
DROP TABLE "REGISTRO"."SCHEDA_PERSONA" cascade constraints;
DROP TABLE "REGISTRO"."SOCIETA" cascade constraints;
DROP TABLE "REGISTRO"."TEMPLATE" cascade constraints;
DROP TABLE "REGISTRO"."VERSIONING" cascade constraints;
DROP SEQUENCE "REGISTRO"."SEQ_ACCESSO_INFORMAZIONE";
DROP SEQUENCE "REGISTRO"."SEQ_COMUNICAZIONE";
DROP SEQUENCE "REGISTRO"."SEQ_COMUNICAZIONE_PERSONA";
DROP SEQUENCE "REGISTRO"."SEQ_CONTATORE_SCHEDA";
DROP SEQUENCE "REGISTRO"."SEQ_SCHEDA_INFORMAZIONE";
DROP SEQUENCE "REGISTRO"."SEQ_SCHEDA_PERSONA";
DROP SEQUENCE "REGISTRO"."SEQ_SOCIETA";
DROP SEQUENCE "REGISTRO"."SEQ_TEMPLATE";
DROP SEQUENCE "REGISTRO"."SEQ_VERSIONING";
--------------------------------------------------------
--  DDL for Sequence SEQ_ACCESSO_INFORMAZIONE
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_ACCESSO_INFORMAZIONE"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_COMUNICAZIONE
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_COMUNICAZIONE"  MINVALUE 0 MAXVALUE 999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_COMUNICAZIONE_PERSONA
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_COMUNICAZIONE_PERSONA"  MINVALUE 0 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_CONTATORE_SCHEDA
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_CONTATORE_SCHEDA"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_SCHEDA_INFORMAZIONE
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_SCHEDA_INFORMAZIONE"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_SCHEDA_PERSONA
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_SCHEDA_PERSONA"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_SOCIETA
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_SOCIETA"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_TEMPLATE
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_TEMPLATE"  MINVALUE 0 MAXVALUE 999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Sequence SEQ_VERSIONING
--------------------------------------------------------

   CREATE SEQUENCE  "REGISTRO"."SEQ_VERSIONING"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOPARTITION ;
--------------------------------------------------------
--  DDL for Table ACCESSO_INFORMAZIONE
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" 
   (	"ID" NUMBER, 
	"ID_SCHEDA_INFORMAZIONE" NUMBER, 
	"ID_SCHEDA_PERSONA" NUMBER, 
	"DATA_ACCESSO_INFORMAZIONE" DATE, 
	"ORA_ACCESSO_INFORMAZIONE" VARCHAR2(50 BYTE), 
	"DATA_CESSAZIONE_INFORMAZIONE" DATE, 
	"ORA_CESSAZIONE_INFORMAZIONE" VARCHAR2(50 BYTE), 
	"MOTIVAZIONE_ACCESSO" VARCHAR2(200 BYTE), 
	"STORICO" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "REGISTRO"."ACCESSO_INFORMAZIONE"."STORICO" IS 'S=SI, N=NO';
--------------------------------------------------------
--  DDL for Table COMUNICAZIONE
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."COMUNICAZIONE" 
   (	"ID" NUMBER, 
	"DATA_INVIO" DATE, 
	"TIPO" VARCHAR2(1 BYTE), 
	"ID_TEMPLATE" NUMBER, 
	"TESTO" VARCHAR2(4000 BYTE), 
	"MITTENTE" VARCHAR2(200 BYTE), 
	"OGGETTO" VARCHAR2(200 BYTE), 
	"TIPO_AMBITO" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "REGISTRO"."COMUNICAZIONE"."TIPO" IS 'M=MAIL, L=LETTERA';
   COMMENT ON COLUMN "REGISTRO"."COMUNICAZIONE"."TIPO_AMBITO" IS 'I=INSIDER LIST, R=REGISTRO MANAGER';
--------------------------------------------------------
--  DDL for Table COMUNICAZIONE_PERSONA
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."COMUNICAZIONE_PERSONA" 
   (	"ID_COMUNICAZIONE" NUMBER, 
	"ID_SCHEDA_PERSONA" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table CONTATORE_SCHEDA
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."CONTATORE_SCHEDA" 
   (	"ID" NUMBER, 
	"CONT_I" NUMBER, 
	"CONT_R" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SCHEDA_INFORMAZIONE
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."SCHEDA_INFORMAZIONE" 
   (	"ID_SCHEDA_INFORMAZIONE" NUMBER, 
	"INFORMAZIONE_PRIVILEGIATA" VARCHAR2(300 BYTE), 
	"DATA_IDENTIFICAZIONE" DATE, 
	"ORA_IDENTIFICAZIONE" VARCHAR2(20 BYTE), 
	"AUTORE_INFORMAZIONE" VARCHAR2(20 BYTE), 
	"DATA_ULTIMO_AGGIORNAMENTO_INFO" DATE, 
	"DATA_ACCESSO_INFORMAZIONE" DATE, 
	"ORA_ACCESSO_INFORMAZIONE" VARCHAR2(20 BYTE), 
	"MOTIVAZIONE_ACCESSO" VARCHAR2(300 BYTE), 
	"DATA_CESSAZIONE_INFORMAZIONE" DATE, 
	"ORA_CESSAZIONE_INFORMAZIONE" VARCHAR2(20 BYTE), 
	"STATO" VARCHAR2(50 BYTE), 
	"DATA_SEZIONE" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SCHEDA_PERSONA
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."SCHEDA_PERSONA" 
   (	"CODICE_FISCALE" VARCHAR2(20 BYTE), 
	"NOME" VARCHAR2(45 BYTE), 
	"COGNOME" VARCHAR2(45 BYTE), 
	"COGNOME_NASCITA" VARCHAR2(45 BYTE), 
	"TELEFONO_PROFESSIONALE" VARCHAR2(200 BYTE), 
	"TELEFONO_PRIVATO" VARCHAR2(200 BYTE), 
	"INDIRIZZO" VARCHAR2(100 BYTE), 
	"CAP" VARCHAR2(20 BYTE), 
	"LOCALITA" VARCHAR2(45 BYTE), 
	"PROVINCIA" VARCHAR2(20 BYTE), 
	"NAZIONE" VARCHAR2(20 BYTE), 
	"NOME_INDIRIZZO_IMPRESA" VARCHAR2(200 BYTE), 
	"IDENTIFICATIVO_NAZIONALE" VARCHAR2(45 BYTE), 
	"DATA_NASCITA" DATE, 
	"DATA_INSERIMENTO" DATE, 
	"EMAIL" VARCHAR2(45 BYTE), 
	"RUOLO" VARCHAR2(80 BYTE), 
	"RAGIONE_SOCIALE" VARCHAR2(80 BYTE), 
	"PARTITA_IVA" VARCHAR2(11 BYTE), 
	"TIPO_ACCESSO_INFORMAZIONE" VARCHAR2(20 BYTE), 
	"TIPOLOGIA_SOGGETTO" VARCHAR2(20 BYTE), 
	"MANAGER_RIFERIMENTO" VARCHAR2(45 BYTE), 
	"CODICE_SCHEDA" VARCHAR2(45 BYTE), 
	"STATO" VARCHAR2(45 BYTE), 
	"MOTIVO_ULTIMA_VARIAZIONE" VARCHAR2(300 BYTE), 
	"MOTIVO_ISCRIZIONE" VARCHAR2(300 BYTE), 
	"AUTORE" VARCHAR2(45 BYTE), 
	"DATA_ISCRIZIONE" DATE, 
	"DATA_ULTIMO_AGGIORNAMENTO" DATE, 
	"DATA_CANCELLAZIONE" DATE, 
	"ID_SCHEDA_PERSONA" NUMBER, 
	"ID_SOCIETA" NUMBER, 
	"TIPO_AMBITO" VARCHAR2(1 BYTE), 
	"DESCRIZIONE_INFORMAZIONE" VARCHAR2(200 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "REGISTRO"."SCHEDA_PERSONA"."TIPO_AMBITO" IS 'R=RegistroManager, I=InsiderList';
--------------------------------------------------------
--  DDL for Table SOCIETA
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."SOCIETA" 
   (	"ID_SOCIETA" NUMBER, 
	"NOME" VARCHAR2(45 BYTE), 
	"INIZIALE" VARCHAR2(1 BYTE), 
	"STATO" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table TEMPLATE
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."TEMPLATE" 
   (	"ID" NUMBER, 
	"NOME_TEMPLATE" VARCHAR2(100 BYTE), 
	"TESTO" VARCHAR2(4000 BYTE), 
	"PERSONECC" VARCHAR2(1000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table VERSIONING
--------------------------------------------------------

  CREATE TABLE "REGISTRO"."VERSIONING" 
   (	"ID_VERSIONING" NUMBER, 
	"VERSIONE" NUMBER, 
	"DATI_MODIFICATI" VARCHAR2(800 BYTE), 
	"DATA_CREAZIONE" DATE, 
	"ID_SCHEDA_PERSONA" NUMBER, 
	"SCHEDA_JSON" VARCHAR2(4000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into REGISTRO.ACCESSO_INFORMAZIONE
SET DEFINE OFF;
REM INSERTING into REGISTRO.COMUNICAZIONE
SET DEFINE OFF;
REM INSERTING into REGISTRO.COMUNICAZIONE_PERSONA
SET DEFINE OFF;
REM INSERTING into REGISTRO.SCHEDA_INFORMAZIONE
SET DEFINE OFF;
REM INSERTING into REGISTRO.SCHEDA_PERSONA
SET DEFINE OFF;
REM INSERTING into REGISTRO.SOCIETA
SET DEFINE OFF;
REM INSERTING into REGISTRO.VERSIONING
SET DEFINE OFF;
REM INSERTING into REGISTRO.CONTATORE_SCHEDA
SET DEFINE OFF;
Insert into REGISTRO.SOCIETA (ID_SOCIETA,NOME,INIZIALE,STATO) values (1,'Sara Assicurazioni','S','Definitivo');
REM INSERTING into REGISTRO.TEMPLATE
SET DEFINE OFF;
Insert into REGISTRO.TEMPLATE (ID,NOME_TEMPLATE,TESTO,PERSONECC) values (1,'Inserimento al registro','Con riferimento al Registro delle persone che hanno accesso ad informazioni privilegiate concernenti Sara Assicurazioni S.p.A e/o il Gruppo Sara Assicurazioni(il "Registro") istituito da ${Societa}
sensi dell''articolo 115-bis del Decreto Legislativo 58/98(il "TUF").
Come attuato dagli articoli 152-bis del Regolamento Emittenti, approvato da CONSOB con delibera n.11971 del 14 Maggio 1999,
la informiamo con la presente che i suoi dati, con le specifiche di seguito elencate, sono stati iscritti con la seguente motivazione: ${MotivoIscrizione}

Data ultimo aggiornamento: ${DataUltimoAggiornamento}
Cognome e Nome: ${Cognome} ${Nome}
Data di nascita: ${DataNascita}
Codice Fiscale: ${CodiceFiscale}
Carica/funzione/qualifica: ${Ruolo}
Data di iscrizione: ${DataIscrizione}

Si ricorda che, ai sensi di legge, nessuna informazione privilegiata può essere comunicata a terzi senza giustificato motivo di lavoro o professionale e la invitiamo a segnalare alla Società scrivente senza
indugio l''eventuale giustificato motivo per la conseguente iscrizione del terzo nel Registro.
Si ricorda inoltre che la normativa sanzione l''abuso di informazioni privilegiate, e in particolare:


-l''acquisto,vendita o altre operazioni effetuate, direttamente o indirettamente, per conto proprio o di terzi, su strumenti finanziari utilizzando informazioni privilegiate;
-la comunicazione a terzi di informazioni privilegiate al di fuori del normale esercizio di attività lavorativa professionale;
-la raccomandazione ad altri, sulla base di informazioni privilegiate, di compiere operazioni su strumenti finanziari.


Le sanzioni penali ed amministrative a carico dei soggetti che fanno abuso di informazioni privilegiate sono stabilite agli articoli 184 e 187-bis del TUF.',null);
Insert into REGISTRO.TEMPLATE (ID,NOME_TEMPLATE,TESTO,PERSONECC) values (2,'Modifica al registro','Con riferimento al Registro delle persone che hanno accesso ad informazioni privilegiate concernenti Sara Assicurazioni S.p.A e/o il Gruppo Sara Assicurazioni(il "Registro") istituito da ${Societa}
sensi dell''articolo 115-bis del Decreto Legislativo 58/98(il "TUF").
Come attuato dagli articoli 152-bis del Regolamento Emittenti, approvato da CONSOB con delibera n.11971 del 14 Maggio 1999,
la informiamo con la presente che i suoi dati, con le specifiche di seguito elencate, sono stati modificati con la seguente motivazione: ${MotivoVariazione}

Data ultimo aggiornamento: ${DataUltimoAggiornamento}
Cognome e Nome: ${Cognome} ${Nome}
Data di nascita: ${DataNascita}
Codice Fiscale: ${CodiceFiscale}
Carica/funzione/qualifica: ${Ruolo}
Data di iscrizione: ${DataIscrizione}

Si ricorda che, ai sensi di legge, nessuna informazione privilegiata può essere comunicata a terzi senza giustificato motivo di lavoro o professionale e la invitiamo a segnalare alla Società scrivente senza
indugio l''eventuale giustificato motivo per la conseguente iscrizione del terzo nel Registro.
Si ricorda inoltre che la normativa sanzione l''abuso di informazioni privilegiate, e in particolare:


-l''acquisto,vendita o altre operazioni effetuate, direttamente o indirettamente, per conto proprio o di terzi, su strumenti finanziari utilizzando informazioni privilegiate;
-la comunicazione a terzi di informazioni privilegiate al di fuori del normale esercizio di attività lavorativa professionale;
-la raccomandazione ad altri, sulla base di informazioni privilegiate, di compiere operazioni su strumenti finanziari.


Le sanzioni penali ed amministrative a carico dei soggetti che fanno abuso di informazioni privilegiate sono stabilite agli articoli 184 e 187-bis del TUF.',null);
Insert into REGISTRO.TEMPLATE (ID,NOME_TEMPLATE,TESTO,PERSONECC) values (3,'Cancellazione al registro','Con riferimento al Registro delle persone che hanno accesso ad informazioni privilegiate concernenti Sara Assicurazioni S.p.A e/o il Gruppo Sara Assicurazioni(il "Registro") istituito da ${Societa}
sensi dell''articolo 115-bis del Decreto Legislativo 58/98(il "TUF").
Come attuato dagli articoli 152-bis del Regolamento Emittenti, approvato da CONSOB con delibera n.11971 del 14 Maggio 1999,
la informiamo con la presente che i suoi dati, con le specifiche di seguito elencate, sono stati cancellati con la seguente motivazione: ${MotivoVariazione}

Data ultimo aggiornamento: ${DataUltimoAggiornamento}
Cognome e Nome: ${Cognome} ${Nome}
Data di nascita: ${DataNascita}
Codice Fiscale: ${CodiceFiscale}
Carica/funzione/qualifica: ${Ruolo}
Data di iscrizione: ${DataIscrizione}

Si ricorda che, ai sensi di legge, nessuna informazione privilegiata può essere comunicata a terzi senza giustificato motivo di lavoro o professionale e la invitiamo a segnalare alla Società scrivente senza
indugio l''eventuale giustificato motivo per la conseguente iscrizione del terzo nel Registro.
Si ricorda inoltre che la normativa sanzione l''abuso di informazioni privilegiate, e in particolare:


-l''acquisto,vendita o altre operazioni effetuate, direttamente o indirettamente, per conto proprio o di terzi, su strumenti finanziari utilizzando informazioni privilegiate;
-la comunicazione a terzi di informazioni privilegiate al di fuori del normale esercizio di attività lavorativa professionale;
-la raccomandazione ad altri, sulla base di informazioni privilegiate, di compiere operazioni su strumenti finanziari.


Le sanzioni penali ed amministrative a carico dei soggetti che fanno abuso di informazioni privilegiate sono stabilite agli articoli 184 e 187-bis del TUF.',null);
--------------------------------------------------------
--  DDL for Index ACCESSO_INFORMAZIONE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."ACCESSO_INFORMAZIONE_PK" ON "REGISTRO"."ACCESSO_INFORMAZIONE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index COMUNICAZIONE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."COMUNICAZIONE_PK" ON "REGISTRO"."COMUNICAZIONE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index CONTATORE_SCHEDA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."CONTATORE_SCHEDA_PK" ON "REGISTRO"."CONTATORE_SCHEDA" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SCHEDA_INFORMAZIONE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."SCHEDA_INFORMAZIONE_PK" ON "REGISTRO"."SCHEDA_INFORMAZIONE" ("ID_SCHEDA_INFORMAZIONE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SCHEDA_PERSONA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."SCHEDA_PERSONA_PK" ON "REGISTRO"."SCHEDA_PERSONA" ("ID_SCHEDA_PERSONA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SOCIETA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."SOCIETA_PK" ON "REGISTRO"."SOCIETA" ("ID_SOCIETA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TEMPLATE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."TEMPLATE_PK" ON "REGISTRO"."TEMPLATE" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index VERSIONING_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "REGISTRO"."VERSIONING_PK" ON "REGISTRO"."VERSIONING" ("ID_VERSIONING") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ACCESSO_INFORMAZIONE
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" MODIFY ("ID_SCHEDA_INFORMAZIONE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" MODIFY ("ID_SCHEDA_PERSONA" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" ADD CONSTRAINT "ACCESSO_INFORMAZIONE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMUNICAZIONE
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."COMUNICAZIONE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."COMUNICAZIONE" ADD CONSTRAINT "COMUNICAZIONE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMUNICAZIONE_PERSONA
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."COMUNICAZIONE_PERSONA" MODIFY ("ID_COMUNICAZIONE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."COMUNICAZIONE_PERSONA" MODIFY ("ID_SCHEDA_PERSONA" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CONTATORE_SCHEDA
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."CONTATORE_SCHEDA" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."CONTATORE_SCHEDA" ADD CONSTRAINT "CONTATORE_SCHEDA_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table SCHEDA_INFORMAZIONE
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."SCHEDA_INFORMAZIONE" MODIFY ("ID_SCHEDA_INFORMAZIONE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_INFORMAZIONE" ADD CONSTRAINT "SCHEDA_INFORMAZIONE_PK" PRIMARY KEY ("ID_SCHEDA_INFORMAZIONE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table SCHEDA_PERSONA
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("CODICE_FISCALE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("NOME" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("COGNOME" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("DATA_NASCITA" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("RUOLO" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("TIPOLOGIA_SOGGETTO" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("STATO" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("AUTORE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" MODIFY ("ID_SCHEDA_PERSONA" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" ADD CONSTRAINT "SCHEDA_PERSONA_PK" PRIMARY KEY ("ID_SCHEDA_PERSONA")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table SOCIETA
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."SOCIETA" MODIFY ("ID_SOCIETA" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SOCIETA" MODIFY ("NOME" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SOCIETA" MODIFY ("INIZIALE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."SOCIETA" ADD CONSTRAINT "SOCIETA_PK" PRIMARY KEY ("ID_SOCIETA")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table TEMPLATE
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."TEMPLATE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."TEMPLATE" ADD CONSTRAINT "TEMPLATE_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table VERSIONING
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."VERSIONING" MODIFY ("ID_VERSIONING" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."VERSIONING" MODIFY ("VERSIONE" NOT NULL ENABLE);
  ALTER TABLE "REGISTRO"."VERSIONING" ADD CONSTRAINT "VERSIONING_PK" PRIMARY KEY ("ID_VERSIONING")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ACCESSO_INFORMAZIONE
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" ADD CONSTRAINT "ACCESSO_INFORMAZIONE_FK1" FOREIGN KEY ("ID_SCHEDA_PERSONA")
	  REFERENCES "REGISTRO"."SCHEDA_PERSONA" ("ID_SCHEDA_PERSONA") ENABLE;
  ALTER TABLE "REGISTRO"."ACCESSO_INFORMAZIONE" ADD CONSTRAINT "ACCESSO_INFORMAZIONE_FK2" FOREIGN KEY ("ID_SCHEDA_INFORMAZIONE")
	  REFERENCES "REGISTRO"."SCHEDA_INFORMAZIONE" ("ID_SCHEDA_INFORMAZIONE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMUNICAZIONE
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."COMUNICAZIONE" ADD CONSTRAINT "COM_TEMPLATE_FK1" FOREIGN KEY ("ID_TEMPLATE")
	  REFERENCES "REGISTRO"."TEMPLATE" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMUNICAZIONE_PERSONA
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."COMUNICAZIONE_PERSONA" ADD CONSTRAINT "COMUNICAZIONE_PERSONA_FK1" FOREIGN KEY ("ID_COMUNICAZIONE")
	  REFERENCES "REGISTRO"."COMUNICAZIONE" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "REGISTRO"."COMUNICAZIONE_PERSONA" ADD CONSTRAINT "COMUNICAZIONE_PERSONA_FK2" FOREIGN KEY ("ID_SCHEDA_PERSONA")
	  REFERENCES "REGISTRO"."SCHEDA_PERSONA" ("ID_SCHEDA_PERSONA") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SCHEDA_PERSONA
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."SCHEDA_PERSONA" ADD CONSTRAINT "SCHEDA_PERSONA_FK1" FOREIGN KEY ("ID_SOCIETA")
	  REFERENCES "REGISTRO"."SOCIETA" ("ID_SOCIETA") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table VERSIONING
--------------------------------------------------------

  ALTER TABLE "REGISTRO"."VERSIONING" ADD CONSTRAINT "VERSIONING_FK1" FOREIGN KEY ("ID_SCHEDA_PERSONA")
	  REFERENCES "REGISTRO"."SCHEDA_PERSONA" ("ID_SCHEDA_PERSONA") ENABLE;
